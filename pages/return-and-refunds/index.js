
import MetaTagHeader from "../../components/MetaTagHeader";
import Breadcrumb from "../../components/Breadcrumb";
const breadcrumb = [
    {
        title: "Return and Refunds",
    },
];
const returnandrefund = () => {
    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="mb-12 text-center">
                    <h1>RETURN AND REFUNDS</h1>
                </div>
                <p >
                    The following general returns policy is applicable to all items sold on www.toptenelectronics.in
                </p>
                <p>
                    A product purchased on www.toptenelectronics.in is eligible for return within the return period mentioned in the table below if it fulfils any of the following conditions:
                </p>
                <ul>
                    <li>
                        Product delivered is different from what was ordered
                    </li>
                    <li>
                        Product was received in a physically damaged condition or was found defective when package was opened.
                    </li>
                    <li>
                        The product or parts of the product or accessories that were described on the website as “In the Box” was missing.

                    </li>
                </ul>
                <p>
                    Please ensure that you do not accept packages where the seal is tampered. Acceptance of a tampered or a damaged box will automatically disqualify any return claims for physically damaged/defective products, incorrect product or missing accessories.
                </p>
                <p>
                    The below product categories are marked as “non-returnable” unless the package received is verified as Defective or Damaged.
                </p>
                <ol>
                    <li>
                        Mobile Phones and Laptops
                    </li>
                    <li>
                        Mobile Phone accessories like screen guards, screen protectors and tempered glass.
                    </li>
                    <li>
                        All console games and PC games that are labelled as non-returnable on their product detail pages.
                    </li>
                </ol>
                <p>
                    All items to be returned must be in their original condition along with the following.
                </p>
                <ol>
                    <li>
                        Original Order Invoice
                    </li>
                    <li>
                        Original packaging with labels, user manuals and warranty card.
                    </li>
                    <li>
                        Serial Number/IMEI of the product being returned should match our system

                    </li>
                    <li>
                        Any accessories/components, combo or bundle products or freebies you received with the purchase

                    </li>
                    <li>
                        For Wrong product/colour, incorrect description and missing accessories, the product should be unused.
                    </li>
                    <li>
                        For “Defective on Arrival” of installable products, returns will be accepted only if the product was installed by our engineers
                    </li>
                    <li>
                        For DOA of non-installable products, replacements would be processed basis the following
                        <li>
                            All Apple products (smartphones, MacBook’s, airpods and accessories) must include a DOA or defective certificate from a Brand Authorized Service Centre
                        </li>
                        <li>
                            Products like Smartphones and Laptops must include a DOA or defective certificate from a Brand Authorized Service Centre
                        </li>
                        <li>
                            Final QA of the product and certification of defect by the engineers when the product is received back as a return
                        </li>
                    </li>
                    <p>
                        Please ensure you have erased any/all personal informed from an electronic device that stores any personal information prior to returning. Top Ten Electronic Shoppe shall not be liable for any misuse or usage of such information.
                    </p>

                </ol>
            </div>
        </>
    )
}

export default returnandrefund
