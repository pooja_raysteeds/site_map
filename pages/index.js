import React, { useEffect, useState } from "react";
import NProgress from "nprogress";
import dynamic from 'next/dynamic'
import { getErrorMessage } from "../services/errorHandle"
import { getProductsByIds, getProductsByCategoryIds, getProductsByParentIds } from "../api/products"
import { getSpecialOffers, getPromoGalleries } from "../api/promo"
import { getLocalStorage } from "../services/helper";
const Carousel = dynamic(() => import('../components/Carousel'))
const Deals = dynamic(() => import('../components/Deals'))
const SpecialOffers = dynamic(() => import('../components/SpecialOffers'))
const FeaturedOnsaleTopratedTab = dynamic(() => import('../components/F-O-T-Tab'))
const ShopByCategory = dynamic(() => import('../components/ShopByCategory'))
const FullWidthBanner = dynamic(() => import('../components/FullWidthBanner'))
const HorizontalSlider = dynamic(() => import('../components/HorizontalSlider'))
const BestSellers = dynamic(() => import('../components/BestSellers'))
const Brands = dynamic(() => import('../components/Brands'))
const MetaTagHeader = dynamic(() => import('../components/MetaTagHeader'))
import {
  PROMO_TYPE_1,
  PROMO_TYPE_2,
  PROMO_TYPE_3,
  PROMO_TYPE_4,
  PROMO_TYPE_5,
  PROMO_TYPE_6,
  PROMO_TYPE_7,
  PROMO_TYPE_8,
  PROMO_TYPE_11,
  PROMO_TYPE_12,
  CATEGORY_TYPE_1,
  CATEGORY_TYPE_2,
  CATEGORY_TYPE_3,
  CATEGORY_TYPE_4,
  CATEGORY_TYPE_5,
  CATEGORY_TYPE_6,
  CATEGORY_TYPE_7,
  CATEGORY_TYPE_8
} from "../services/constant";

const index = () => {
  const [loading, setLoading] = useState(true);
  const [recentProducts, setRecentProducts] = useState([])
  const [sliderProducts, setSliderProducts] = useState([])
  const [sliderParentProducts, setSliderParentProducts] = useState([])
  const [specialoffers, setSpecialoffers] = useState([])
  const [promoGalleries, setPromoGalleries] = useState([])
  const recentProductIds = [...new Set(getLocalStorage("rcntP"))]
  const categoryIds = {
    category_ids: [
      CATEGORY_TYPE_1,
      CATEGORY_TYPE_2,
      CATEGORY_TYPE_3,
      CATEGORY_TYPE_4,
      CATEGORY_TYPE_5,
      CATEGORY_TYPE_6,
      CATEGORY_TYPE_7,
      CATEGORY_TYPE_8
    ]
  }
  const parentIds = {
    parent_ids: [CATEGORY_TYPE_5]
  }

  useEffect(() => {
    getProductsByCategoryIds(categoryIds).then(
      res => {
        setSliderProducts(res)
        setLoading(false)
        NProgress.done()
      }).catch(
        err => getErrorMessage(err)
      )

    getProductsByParentIds(parentIds).then(
      res => setSliderParentProducts(res)
    ).catch(
      err => getErrorMessage(err)
    )

    getSpecialOffers().then(
      res => setSpecialoffers(res)
    ).catch(
      err => getErrorMessage(err)
    )

    getPromoGalleries().then(
      res => setPromoGalleries(res)
    ).catch(
      err => getErrorMessage(err)
    )

    getProductsByIds(recentProductIds).then(
      res => setRecentProducts(res)
    ).catch(
      err => getErrorMessage(err)
    )

    if (loading) {
      NProgress.start()
    }
  }, []);

  const sliders = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_1)
  const catOneBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_2)
  const catTwoBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_3)
  const catThreeBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_4)
  const catFourBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_5)
  const catFiveBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_6)
  const catSixBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_7)
  const catSevenBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_11)
  const catEightBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_12)
  const AdvBanner = promoGalleries.filter(p => p.promoType.type === PROMO_TYPE_8)

  const catOneProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_1)
  const catTwoProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_2)
  const catThreeProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_3)
  const catFourProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_4)
  const catFiveProducts = sliderParentProducts.filter(p => p.parent_id === CATEGORY_TYPE_5)
  const catSixProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_6)
  const catSevenProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_7)
  const catEightProducts = sliderProducts.filter(p => p.category_id === CATEGORY_TYPE_8)

  return (
    <>
      <MetaTagHeader />
      <Carousel data={sliders} />
      <Deals />
      <SpecialOffers data={specialoffers} />
      <FeaturedOnsaleTopratedTab />
      <ShopByCategory />
      <FullWidthBanner data={catOneBanner} />
      <HorizontalSlider data={catOneProducts} />
      <FullWidthBanner data={catTwoBanner} />
      <HorizontalSlider data={catTwoProducts} />
      <FullWidthBanner data={catThreeBanner} />
      <HorizontalSlider data={catThreeProducts} />
      <FullWidthBanner data={catFourBanner} />
      <HorizontalSlider data={catFourProducts} />
      <FullWidthBanner data={catFiveBanner} />
      <HorizontalSlider data={catFiveProducts} title="KITCHEN APPLIANCES" />
      <FullWidthBanner data={catSixBanner} />
      <HorizontalSlider data={catSixProducts} />
      <FullWidthBanner data={catSevenBanner} />
      <HorizontalSlider data={catSevenProducts} />
      <FullWidthBanner data={catEightBanner} />
      <HorizontalSlider data={catEightProducts} />
      <BestSellers />
      <FullWidthBanner data={AdvBanner} />
      <HorizontalSlider data={recentProducts} title="RECENTLY VIEWED" />
      <Brands />
    </>
  )
}

export default index;