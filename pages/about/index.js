import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";
const breadcrumb = [
    {
        title: "About",
    },
];

const AboutUs = () => {

    return (
        <>
            <MetaTagHeader />
            <div className="container">
                <Breadcrumb data={breadcrumb} />
                <div
                    className="bg-img-hero mb-14"
                    style={{ backgroundImage: "url(../../assets/img/aboutus.jpg)" }}
                >
                    <div className="container">
                        <div className="flex-content-center max-width-620-lg flex-column mx-auto text-center min-height-564">
                            <h1 className="h1 font-weight-bold">About Us</h1>
                            <p className="text-gray-39 font-size-18 text-lh-default">
                                Founded in 1996 with its first ever store in Vashi, Top Ten Electronics is
                                growing stronger by the day in Navi Mumbai. With 10 already functional
                                stores and many more in tow in future, Top Ten Electronics is a well-
                                known brand among customers when it comes to flawless customer service
                                and ease of user experience.
                            </p>
                        </div>
                    </div>
                </div>

                <div className="container mb-8 mb-lg-0 ">
                    <div className="text-center">
                        <h1 className="h1 font-weight-bold">Values</h1>
                        <p className="text-gray-39 font-size-18 text-lh-default">
                            Top Ten Electronics Function on 6 core values based on its name.
                        </p>
                    </div>
                    <div className="row mb-8">
                        <div className="col-lg-2" />
                        <div className="col-lg-8">
                            <div className="row">
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">T</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            rustworthy
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        We aim to offer not just a service but to build trust
                                        among our customers when it comes to product quality
                                        and service.
                                    </p>
                                </div>
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">T</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            imely
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        We aim to offer not just a service but to build
                                        trust among our customers when it comes to product
                                        quality and service.
                                    </p>
                                </div>
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">O</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            peness
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        We believe in transparency and honesty. We continually strive to
                                        provide best rates in the market for all products.
                                    </p>
                                </div>
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">E</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            ase
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        We strongly believe that the easier the shopping experience is
                                        the happier our customer will be.
                                    </p>
                                </div>
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">P</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            ersonalised
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        For us, Consumer is King! We always try to provide unparalleled
                                        personalised user experience - both online and offline.
                                    </p>
                                </div>
                                <div className="col-lg-6 mb-5 mb-lg-8">
                                    <div className="d-flex-row">
                                        <h2 className=" font-weight-semi-bold text-gray-39 mb-4 text-center">N</h2>
                                        <h3 className="font-size-18 font-weight-semi-bold text-gray-39 mb-4 lh-3dot5">
                                            ewness
                                        </h3>
                                    </div>
                                    <p className="text-gray-90">
                                        We always try to stock the latest technologies and gadgets in
                                        our store so that our customers have a one-stop-shop for all things new.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-2" />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AboutUs;
