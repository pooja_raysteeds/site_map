import React, { useEffect, useState } from "react";
import { useRouter } from 'next/router'
import HorizontalSlider from "../../../components/HorizontalSlider";
import ProductInfo from "../../../components/ProductInfo";
import ProductTabs from "../../../components/ProductTabs";
import Brands from "../../../components/Brands";
import { getProductByUrl, getRelatedProducts } from "../../../api/products";
import { getErrorMessage } from "../../../services/errorHandle";
import { getLocalStorage, setLocalStorage } from "../../../services/helper";
import Breadcrumb from "../../../components/Breadcrumb";
import MetaTagHeader from "../../../components/MetaTagHeader";

const ProductDetails = () => {
    const { query, isReady } = useRouter()
    const { slug_url } = query
    const [relatedProducts, setRelatedProducts] = useState([])
    const [product, setProduct] = useState({})

    useEffect(() => {
        if (isReady) {
            getProductByUrl(slug_url).then(
                res => setProduct(res)
            ).catch(
                err => getErrorMessage(err)
            )

            getRelatedProducts(slug_url).then(
                res => setRelatedProducts(res)
            ).catch(
                err => getErrorMessage(err)
            )
        }
    }, [isReady, slug_url]);

    if (product && Object.keys(product).length === 0 || !product) return null;
    let recentProducts = getLocalStorage("rcntP")
    recentProducts.push(product.id);
    setLocalStorage("rcntP", recentProducts)
    const breadcrumb = [
        {
            link: `/shop?parent=${product.parent.slug_url}`,
            title: product.parent.title,
        },
        {
            link: `/shop?category=${product.category.slug_url}`,
            title: product.category.title,
        },
        {
            title: product.title,
        },
    ];

    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="mb-14">
                    {/* Product Information */}
                    <ProductInfo data={product} />
                </div>
            </div>

            <div className="pt-6 pb-3 mb-6">
                {/* Product Tab - Description, Specifications, Reviews */}
                <ProductTabs data={product} />
            </div>


            <div className="container">
                <div className="mb-6">
                    {/* Related Product */}
                    <HorizontalSlider data={relatedProducts} title="Related Products" />
                </div>

                <div className="mb-8">
                    {/* Brand Carousel */}
                    <Brands />
                </div>
            </div>
        </>
    );
}

export default ProductDetails;