import React, { useEffect, useState, useContext } from "react"
import { useRouter } from 'next/router'
import InputRange from 'react-input-range';
import dynamic from 'next/dynamic'
const BrowseCategory = dynamic(() => import('../../components/Browsecategory'))
const ProductList = dynamic(() => import('../../components/ProductList'))
const VerticalSlider = dynamic(() => import('../../components/VerticalSlider'))
import { getErrorMessage } from "../../services/errorHandle"
import { LATEST_PRODUCTS_LIMIT, F_O_T_PRODUCTS_LIMIT } from "../../services/constant";
import { getBrandsByCategory, getFilters } from "../../api/products"
import { getCategoryTitle } from "../../api/category"
import Brands from "../../components/Brands";
import AppContext from "../../context/AppContext"
import "react-input-range/lib/css/index.css"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";

const Shop = () => {
    const { query, isReady, push } = useRouter()
    const { search, parent, category, subcategory, brand, filter, availability, minprice, maxprice } = query
    const [brands, setBrands] = useState([])
    const [params, setParams] = useState({})
    const [categoryName, setCategoryName] = useState({})
    const [productIds, setProductIds] = useState([]);
    const [checkFilters, setCheckFilters] = useState({});
    const [checkBrands, setCheckBrands] = useState([]);
    const [checkAvailability, setCheckAvailability] = useState(null)
    const [checkIndex, setCheckIndex] = useState(-1)
    const [filters, setFilters] = useState([])
    const [toggleMobFilter, setToggleMobFiler] = useState(false)
    const [initialPrice, setInitialPrice] = useState(null);
    const [filterFlag, setFilterFlag] = useState(false);
    const { isMobileView } = useContext(AppContext)
    const [priceRange, setPriceRange] = useState({
        max: 0,
        min: 0,
        value: {
            max: 0,
            min: 0,
        }
    });
    const availabilityOption = [
        { key: "all", value: "All" },
        { key: "in_stock", value: "In Stock" }
    ]
    const [showFiters, setShowFilters] = useState();
    const breadcrumb = [
        {
            title: "Shop",
        },
        {
            title: categoryName.title || "",
        },
    ];
    useEffect(() => {
        let selectedParams = {}
        if (subcategory) selectedParams.sub_category = subcategory
        if (parent) selectedParams.parent_url = parent
        if (category) selectedParams.category_url = category
        if (search) selectedParams.search = search
        if (brand) {
            selectedParams.brand_url = brand
            const updatedBrand = JSON.parse(brand);
            setCheckBrands([...new Set([...checkBrands, ...updatedBrand])]);
            updatedBrand.forEach((item) => {
                const tag = document.getElementById(item.toUpperCase());
                if (tag) tag.checked = true;
            })
        }
        if (filter) {
            selectedParams.filter_option = filter
            const updatedFilter = JSON.parse(filter);
            setCheckFilters({ ...checkFilters, ...updatedFilter });
            for (const key in updatedFilter) {
                updatedFilter[key].forEach((item) => {
                    const tag = document.getElementById(item);
                    if (tag) tag.checked = true;
                })
            }
        }
        if (availability) {
            selectedParams.availability = availability
            availability == "all" ? setCheckIndex(0) : setCheckIndex(1)
        }
        if (minprice && maxprice) {
            selectedParams.min_price = minprice
            selectedParams.max_price = maxprice
        }
        setParams({ ...selectedParams })
        const category_url = category || parent || subcategory;
        if (productIds && category_url) {
            const params = { slug_url: category_url, ids: productIds }
            getCategoryTitle(category_url).then(
                res => setCategoryName(res),
            ).catch(
                err => getErrorMessage(err)
            )

            getFilters({ params }).then(
                res => setFilters(res)
            ).catch(
                err => getErrorMessage(err)
            )

            getBrandsByCategory(category_url).then(
                res => setBrands(res),
            ).catch(
                err => getErrorMessage(err)
            )
        }
    }, [isReady, query, productIds, (category || parent)]);

    const handleBrandChange = (e) => {
        const { value, checked } = e.target
        if (checked) {
            checkBrands.push(value)
        } else {
            const index = checkBrands.indexOf(value)
            if (index !== -1) checkBrands.splice(index, 1)
        }
        setCheckBrands(checkBrands);
    }

    const handleAvailabilityChange = (e, i) => {
        const { value, checked } = e.target
        if (checked) {
            setCheckAvailability(value);
            setCheckIndex(i)
        } else {
            setCheckAvailability(null);
            setCheckIndex(-1)
        }
    }

    const handleFilterChange = (e, typeId) => {
        const { value, checked } = e.target
        const filterType = checkFilters[typeId] || []
        if (checked) {
            filterType.push(parseInt(value))
        } else {
            const index = filterType.indexOf(parseInt(value))
            if (index !== -1) filterType.splice(index, 1)
        }
        filterType.length > 0 ? checkFilters[typeId] = filterType : delete checkFilters[typeId]
        setCheckFilters(checkFilters);
    }

    const handlePriceRange = (range) => {
        if (priceRange.max === range.max) return;
        setInitialPrice(range);
        if (priceRange.min !== range.min || priceRange.max !== range.max) {
            let updatedPriceRange = priceRange;
            updatedPriceRange.value.min = range.min;
            updatedPriceRange.value.max = range.max;
            setPriceRange({ ...updatedPriceRange, ...range })
        }
    };

    const handleProductIds = (value) => {
        setProductIds(value)
    }

    const submitFilters = (e) => {
        e.preventDefault()
        window.scrollTo(0, 0)
        let redirectUrl = ""
        const { value } = priceRange
        if (parent) redirectUrl += `parent=${parent}&`
        if (category) redirectUrl += `category=${category}&`
        if (subcategory) redirectUrl += `subcategory=${subcategory}&`
        if (checkBrands.length > 0) redirectUrl += `brand=${JSON.stringify(checkBrands)}&`
        if (Object.keys(checkFilters).length > 0) redirectUrl += `filter=${JSON.stringify(checkFilters)}&`
        if (checkAvailability) redirectUrl += `availability=${checkAvailability}&`
        if (value.min !== minprice || value.max !== maxprice) redirectUrl += `minprice=${value.min}&maxprice=${value.max}&`
        if (isMobileView) return setToggleMobFiler((prevState) => !prevState)
        setFilterFlag((prevState) => !prevState)
        redirectUrl = redirectUrl.substring(0, redirectUrl.length - 1);
        push(`/shop?${redirectUrl}`)
    }

    const clearFilters = (e) => {
        e.preventDefault()
        window.scrollTo(0, 0)
        // Uncheck brands list
        checkBrands.map((id) => {
            const tag = document.getElementById(id.toUpperCase());
            if (tag) tag.checked = false
        })
        // Uncheck filters type list
        for (const key in checkFilters) {
            checkFilters[key].forEach((item) => {
                const tag = document.getElementById(item);
                if (tag) tag.checked = false;
            })
        }
        let redirectUrl = ""
        setCheckBrands([])
        setCheckFilters({})
        setCheckAvailability(null)
        setCheckIndex(-1)
        delete params?.min_price
        delete params?.max_price
        delete params?.brand_url
        delete params?.filter_option
        delete params?.availability
        setParams({ ...params })
        if (parent) redirectUrl = `parent=${parent}`
        if (category) redirectUrl = `category=${category}`
        if (subcategory) redirectUrl = `subcategory=${subcategory}`
        push(`/shop?${redirectUrl}`)
        if (initialPrice) {
            let updatedPriceRange = priceRange;
            updatedPriceRange.value.min = initialPrice.min;
            updatedPriceRange.value.max = initialPrice.max;
            setPriceRange({ ...updatedPriceRange, ...initialPrice })
        }
        setShowFilters("collapse");
        setTimeout(() => {
            setShowFilters()
        }, 1000);
    }

    const renderMobFiler = () => {
        return (
            <>
                {
                    isMobileView && toggleMobFilter && (
                        <div className="u-sidebar__body" style={{ maxWidth: "22rem" }}>
                            <div className="u-sidebar__content u-header-sidebar__content px-4">
                                <div className="mb-6 border border-width-2 border-color-3 borders-radius-6">
                                    <div className="z-index-2 pt-2 pr-2">
                                        <button type="button" className="close ml-auto mb-2" onClick={() => setToggleMobFiler((prevState) => !prevState)}>
                                            <span aria-hidden="true"><i className="ec ec-close-remove text-gray-90 font-size-20"></i></span>
                                        </button>
                                    </div>

                                    <BrowseCategory isMobileView={isMobileView} setToggleMobFiler={setToggleMobFiler} />

                                    <div className="mb-6 ml-3" id="mobviewfilters">
                                        <div className="border-bottom border-color-1 mb-5">
                                            <h3 className="section-title section-title__sm mb-0 pb-2 font-size-18">
                                                Filters
                                            </h3>
                                        </div>
                                        <div className="border-bottom pb-4 mb-4">
                                            {/* Brands */}
                                            <h4 className="font-size-14 mb-3 font-weight-bold" data-toggle="collapse"
                                                aria-expanded="false"
                                                aria-controls="filterMobCollapse1"
                                                data-target="#filterMobCollapse1" >BRANDS</h4>
                                            {
                                                brands.map((b, index) => (
                                                    <div id="filterMobCollapse1"
                                                        className="collapse"
                                                        data-parent="#mobviewfilters"
                                                        key={index}
                                                    >
                                                        <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                            <div className="custom-control custom-checkbox">
                                                                <input
                                                                    type="checkbox"
                                                                    className="custom-control-input"
                                                                    name={b.name}
                                                                    value={b.slug_url}
                                                                    onChange={handleBrandChange}
                                                                    id={b.name}
                                                                    checked={checkBrands.find(c => c === b.slug_url)}
                                                                />
                                                                <label className="custom-control-label" htmlFor={b.name}>
                                                                    {b.name}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                            }
                                        </div>
                                        {
                                            filters.map((f, index) => (
                                                <div key={f.id}>
                                                    {
                                                        f.filter !== null && (
                                                            <div className="border-bottom pb-4 mb-4">
                                                                <h4 className="font-size-14 mb-3 font-weight-bold" role="button"
                                                                    data-toggle="collapse"
                                                                    aria-expanded="false"
                                                                    aria-controls={`filterMob${index}Collapse`}
                                                                    data-target={`#filterMob${index}Collapse`}>{f.filter.title}</h4>
                                                                {
                                                                    f.filter.options.map(o => (
                                                                        <div id={`filterMob${index}Collapse`}
                                                                            className="collapse"
                                                                            data-parent="#mobviewfilters"
                                                                            key={o.id}
                                                                        >
                                                                            <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                                                <div className="custom-control custom-checkbox">
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        className="custom-control-input"
                                                                                        onChange={(e) => handleFilterChange(e, f.filter_id)}
                                                                                        value={o.id}
                                                                                        checked={checkFilters[f.filter_id] && checkFilters[f.filter_id].includes(o.id)}
                                                                                        id={o.id}
                                                                                    />
                                                                                    <label className="custom-control-label" htmlFor={o.id}>
                                                                                        {o.title}
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    ))
                                                                }
                                                            </div>
                                                        )
                                                    }
                                                </div>
                                            ))
                                        }

                                        <div className="border-bottom pb-4 mb-4">
                                            {/* Availability Filter - Mobile View */}
                                            <h4 className="font-size-14 mb-3 font-weight-bold dropdown-toggle dropdown-toggle-collapse"
                                                data-toggle="collapse"
                                                role="button"
                                                aria-expanded="false"
                                                aria-controls="collapseAvailability"
                                                data-target="#collapseAvailability"
                                            >
                                                AVAILABILITY
                                            </h4>
                                            {
                                                availabilityOption.map((b, index) => (
                                                    <div className="collapse" id="collapseAvailability" key={index}>
                                                        <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                            <div className="custom-control custom-checkbox">
                                                                <input
                                                                    type="checkbox"
                                                                    className="custom-control-input"
                                                                    name={b.key}
                                                                    value={b.key}
                                                                    onChange={(e) => handleAvailabilityChange(e, index)}
                                                                    id={b.key}
                                                                    checked={checkIndex === index}
                                                                />
                                                                <label className="custom-control-label" htmlFor={b.key}>
                                                                    {b.value}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                            }
                                        </div>

                                        <div className="range-slider">
                                            <h4 className="font-size-14 mb-3 font-weight-bold">Price</h4>
                                            <InputRange
                                                step={2}
                                                maxValue={priceRange.max}
                                                minValue={priceRange.min}
                                                value={priceRange.value}
                                                formatLabel={value => `₹${value}`}
                                                onChange={value => setPriceRange({ ...priceRange, value })}
                                            />
                                            <div className="mt-1 text-gray-111 d-flex mb-4 mt-4">
                                                <span className="mr-0dot5">Price: </span><span>{`₹${priceRange.value.min}`}</span>
                                                <span id="rangeSliderExample3MinResult" />
                                                <span className="mx-0dot5"> — </span><span>{`₹${priceRange.value.max}`}</span>
                                                <span id="rangeSliderExample3MaxResult" />
                                            </div>
                                            <button type="button" onClick={submitFilters} className="btn px-4 btn-primary-dark-w py-2 rounded-lg">
                                                Filter
                                            </button>
                                            {
                                                filter || brand || availability || priceRange.min != priceRange.value.min || priceRange.max != priceRange.value.max ? (
                                                    <button type="button" onClick={clearFilters} className="btn px-4 btn-primary-dark-w py-2 ml-2 rounded-lg">
                                                        Clear
                                                    </button>
                                                ) : <></>
                                            }
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
            </>

        )
    }

    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />

            {/* Render Mobile View Filters */}
            {renderMobFiler()}

            <div className="container">
                <div className="row mb-8">
                    <div className="d-none d-xl-block col-xl-3 col-wd-2gdot5">
                        {/* Browse by Category */}
                        <div className="mb-6 border border-width-2 border-color-3 borders-radius-6">
                            <BrowseCategory isMobileView={isMobileView} setToggleMobFiler={setToggleMobFiler} />
                        </div>

                        {/* Product Filters */}
                        <div className="mb-6" id="filters">
                            <div className="border-bottom border-color-1 mb-5">
                                <h3 className="section-title section-title__sm mb-0 pb-2 font-size-18">
                                    Filters
                                </h3>
                            </div>
                            <div className="border-bottom pb-4 mb-4">
                                {/* Brands */}
                                <h4 className="font-size-14 mb-3 font-weight-bold dropdown-toggle dropdown-toggle-collapse"
                                    data-toggle="collapse"
                                    role="button"
                                    aria-expanded="false"
                                    aria-controls="collapseExample"
                                    data-target="#collapseExample"
                                >
                                    BRANDS
                                </h4>
                                {
                                    brands.map((b, index) => (
                                        <div className="collapse" id="collapseExample" key={index}>
                                            <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                <div className="custom-control custom-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        className="custom-control-input"
                                                        name={b.name}
                                                        value={b.slug_url}
                                                        onChange={handleBrandChange}
                                                        id={b.name}
                                                    //checked={checkBrands.find(c => c === b.slug_url)}
                                                    />
                                                    <label className="custom-control-label" htmlFor={b.name}>
                                                        {b.name}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>

                            {/* Filters */}
                            {
                                filters.map((f, index) => (
                                    <div key={f.id}>
                                        {
                                            f.filter !== null && (
                                                <div className="border-bottom pb-4 mb-4">
                                                    <h4 className="font-size-14 mb-3 font-weight-bold dropdown-toggle dropdown-toggle-collapse"
                                                        role="button"
                                                        data-toggle="collapse"
                                                        aria-expanded="false"
                                                        aria-controls={`filterNav${index}Collapse`}
                                                        data-target={`#filterNav${index}Collapse`}
                                                    >
                                                        {f.filter.title}
                                                    </h4>
                                                    {
                                                        f.filter.options.map(o => (
                                                            <div id={`filterNav${index}Collapse`}
                                                                className={`collapse ${showFiters}`}
                                                                data-parent="#filters"
                                                                key={o.id}
                                                            >
                                                                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                                    <div className="custom-control custom-checkbox">
                                                                        <input
                                                                            type="checkbox"
                                                                            className="custom-control-input"
                                                                            onChange={(e) => handleFilterChange(e, f.filter_id)}
                                                                            value={o.id}
                                                                            checked={checkFilters[f.filter_id] && checkFilters[f.filter_id].includes(o.id)}
                                                                            id={o.id}
                                                                        />
                                                                        <label className="custom-control-label" htmlFor={o.id}>
                                                                            {o.title}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        ))
                                                    }
                                                </div>
                                            )
                                        }
                                    </div>
                                ))
                            }

                            <div className="border-bottom pb-4 mb-4">
                                {/* Availability Filter */}
                                <h4 className="font-size-14 mb-3 font-weight-bold dropdown-toggle dropdown-toggle-collapse"
                                    data-toggle="collapse"
                                    role="button"
                                    aria-expanded="false"
                                    aria-controls="collapseAvailability"
                                    data-target="#collapseAvailability"
                                >
                                    AVAILABILITY
                                </h4>
                                {
                                    availabilityOption.map((b, index) => (
                                        <div className="collapse" id="collapseAvailability" key={index}>
                                            <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                                                <div className="custom-control custom-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        className="custom-control-input"
                                                        name={b.key}
                                                        value={b.key}
                                                        onChange={(e) => handleAvailabilityChange(e, index)}
                                                        id={b.key}
                                                        checked={checkIndex === index}
                                                    />
                                                    <label className="custom-control-label" htmlFor={b.key}>
                                                        {b.value}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>

                            <div className="range-slider">
                                <h4 className="font-size-14 mb-3 font-weight-bold">Price</h4>
                                <InputRange
                                    step={2}
                                    maxValue={priceRange.max}
                                    minValue={priceRange.min}
                                    value={priceRange.value}
                                    formatLabel={value => `₹${value}`}
                                    onChange={value => setPriceRange({ ...priceRange, value })}
                                />
                                <div className="mt-1 text-gray-111 d-flex mb-4 mt-4">
                                    <span className="mr-0dot5">Price: </span><span>{`₹${priceRange.value.min}`}</span>
                                    <span id="rangeSliderExample3MinResult" />
                                    <span className="mx-0dot5"> — </span><span>{`₹${priceRange.value.max}`}</span>
                                    <span id="rangeSliderExample3MaxResult" />
                                </div>
                                <button type="button" onClick={submitFilters} className="btn px-4 btn-primary-dark-w py-2 rounded-lg">
                                    Apply
                                </button>
                                {
                                    filter || brand || availability || priceRange.min != priceRange.value.min || priceRange.max != priceRange.value.max ? (
                                        <button type="button" onClick={clearFilters} className="btn px-4 btn-primary-dark-w py-2 ml-2 rounded-lg">
                                            Clear
                                        </button>
                                    ) : <></>
                                }
                            </div>

                        </div>

                        {/* Latest Product List*/}
                        <div className="mb-8">
                            <VerticalSlider
                                title="Latest Products"
                                params={{ sort: '-id', limit: LATEST_PRODUCTS_LIMIT }}
                            />
                        </div>
                    </div>

                    {/* Product List for Shopping*/}
                    <div className=" d-xl-block col-xl-9 col-wd-9gdot5">
                        <ProductList
                            value={params}
                            categoryTitle={categoryName.title || ""}
                            filterFlag={filterFlag}
                            priceFilter={handlePriceRange}
                            productIds={handleProductIds}
                            setToggleMobFiler={setToggleMobFiler}
                        />
                    </div>
                </div>
            </div>

            {/* Render Brand Filter */}
            <Brands />

            <div className="container d-lg-block mb-3 mt-8">
                {/* Featured OnSale TopRated Products */}
                <div className="row">
                    <div className="col-12  featured">
                        <div className="col-wd-4 col-lg-4">
                            <VerticalSlider
                                title="Featured Products"
                                params={{ is_featured: true, limit: F_O_T_PRODUCTS_LIMIT }}
                            />
                        </div>
                        <div className="col-wd-4 col-lg-4">
                            <VerticalSlider
                                title="Onsale Products"
                                params={{ on_sale: true, limit: F_O_T_PRODUCTS_LIMIT }}
                            />
                        </div>
                        <div className="col-wd-4 col-lg-4">
                            <VerticalSlider
                                title="Top Rated Products"
                                params={{ top_rated: true, limit: F_O_T_PRODUCTS_LIMIT }}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Shop
