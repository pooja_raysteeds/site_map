import React, { useState } from "react";
import OrderInfo from "../../components/OrderInfo";
import { getOrderInfoByOrderNo } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";
const breadcrumb = [
    {
        title: "Track order",
    },
];

const TrackOrder = () => {

    const [show, setShow] = useState(false);
    const [orderId, setOrderId] = useState(null);
    const [orderInfo, setOrderInfo] = useState(null);
    const [error, setError] = useState(false);

    const handleTrack = async () => {
        try {
            if (orderId) {
                const res = await getOrderInfoByOrderNo(orderId);
                if (res) {
                    setOrderInfo(res);
                    setShow(true);
                    return;
                }
            }
            setError(true);
            setTimeout(() => {
                setError(false);
            }, 2000);
            setShow(false);
        } catch (err) {
            getErrorMessage(err);
        }
    }

    return (
        <>
            <MetaTagHeader />
            <div className="container">
                <Breadcrumb data={breadcrumb} />
                <div className="mx-xl-10">
                    <div className="mb-6 text-center">
                        <h1 className="mb-6 pt-3">Track your Order</h1>
                        <p className="text-gray-90 px-xl-10">
                            To track your order please enter your Order Number in
                            the box below and press the "Track" button.
                        </p>
                    </div>
                    <div>
                        <form className="js-validate" noValidate="novalidate">
                            <div className="row center-trackorder ">
                                <div className="col-md-6">
                                    {/* Form Group */}
                                    <div className="js-form-message form-group">
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="text"
                                            id="orderid"
                                            onChange={(e) => setOrderId(e.target.value)}
                                            placeholder="Order Number"
                                            aria-label="Order Number"
                                        />
                                    </div>
                                    {/* End Form Group */}
                                </div>
                                {/* Button */}
                                <div className="col">
                                    <button
                                        type="button"
                                        onClick={handleTrack}
                                        className="btn btn-primary-dark-w px-5"
                                    >
                                        Track
                                    </button>
                                </div>
                                {/* End Button */}
                            </div>
                            {
                                error && (
                                    <p className="text-red" >Please enter valid order ID</p>
                                )
                            }
                        </form>
                    </div>
                </div>
                {
                    show && (
                        <OrderInfo order={orderInfo} />
                    )
                }
            </div>
        </>
    );
};

export default TrackOrder;
