import React, { useEffect, useState, useContext } from "react"
import Link from "next/link"
import { getUserWishList } from "../../api/user"
import { getErrorMessage } from "../../services/errorHandle";
import { getLocalStorage, getS3Url } from "../../services/helper"
import AppContext from "../../context/AppContext"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";

const Wishlist = () => {
    const [wishList, setWishList] = useState(null);
    const user_id = getLocalStorage("usrid");
    const { setUserWishlist, addProductToCart, removeProductToWishlist } = useContext(AppContext);
    const breadcrumb = [
        {
            title: "Wishlist",
        },
    ];

    useEffect(() => {
        getWishlistProduct()
    }, []);

    const getWishlistProduct = () => {
        getUserWishList(user_id).then(
            res => {
                setWishList(res)
                const wishlist = res.map(r => ({ product_id: r.product_id }))
                setUserWishlist(wishlist)
            }
        ).catch(
            err => getErrorMessage(err)
        )
    }

    const handleCart = (product_id) => {
        const params = { product_id, quantity: 1 }
        addProductToCart(params)
    }

    const removeWishlistProduct = (e, product_id) => {
        e.preventDefault();
        removeProductToWishlist(product_id)
        getWishlistProduct()
    }
    return (
        <>
            <MetaTagHeader />
            <div className="container">
                <Breadcrumb data={breadcrumb} />
                <div className="my-1">
                    <h1 className="text-center">My Wishlist</h1>
                </div>
                {wishList && wishList.length > 0 ? (
                    <div className="mb-16 wishlist-table">
                        <form className="mb-4" action="#" method="post">
                            <div className="table-responsive">
                                <table className="table" cellSpacing={0}>
                                    <thead>
                                        <tr>
                                            <th className="product-remove">&nbsp;</th>
                                            <th className="product-thumbnail">
                                                &nbsp;
                                            </th>
                                            <th className="product-name">Product</th>
                                            <th className="product-price">
                                                Price
                                            </th>
                                            <th className="product-Stock w-lg-15">
                                                Stock Status
                                            </th>
                                            <th className="product-subtotal min-width-200-md-lg">
                                                &nbsp;
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {wishList ? (
                                            wishList.map(item => (
                                                <tr key={item.id} >
                                                    <td className="text-center largeviewprodetails">
                                                        <Link href="/wishlist">
                                                            <a
                                                                onClick={(e) => removeWishlistProduct(e, item.product_id)}
                                                                className="text-gray-32 font-size-26"
                                                            >
                                                                <i className="ec ec-close-remove mr-1 font-size-15" />
                                                            </a>
                                                        </Link>
                                                    </td>
                                                    <td className="d-none d-md-table-cell">
                                                        <Link href={`/shop/${item.product.slug_url}`}>
                                                            <a>
                                                                <img
                                                                    rel="preload"
                                                                    className="img-fluid max-width-100 p-1 border border-color-1"
                                                                    src={getS3Url(item.product.galleries[0].image_url)}
                                                                    alt="Image Description"
                                                                />
                                                            </a>
                                                        </Link>
                                                    </td>
                                                    <div className=" col-md-5 col-lg-3 col-xl-3 mt-2 mobileviewprodetails">
                                                        <div className="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                                                            <Link href={`/shop/${item.product.slug_url}`}>
                                                                <a>
                                                                    <img
                                                                        rel="preload"
                                                                        className="img-fluid max-width-100 p-1 border border-color-1"
                                                                        src={getS3Url(item.product.galleries[0].image_url)}
                                                                        alt="Image Description"
                                                                    />
                                                                </a>
                                                            </Link>
                                                            <a
                                                                onClick={(e) => removeWishlistProduct(e, item.product_id)}
                                                                className="text-gray-32 font-size-26 cursor-pointer-on ml-19 wishlistimg"
                                                            >
                                                                <i className="ec ec-close-remove mr-1 font-size-15" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <td data-title="Product">
                                                        <Link href={`/shop/${item.product.slug_url}`}>
                                                            <a className="text-gray-90">
                                                                {item.product.title}
                                                            </a>
                                                        </Link>
                                                    </td>
                                                    <td data-title="Unit Price">
                                                        <span className>
                                                            ₹{item.product.selling_price}
                                                        </span>
                                                    </td>
                                                    <td data-title="Stock Status">
                                                        {
                                                            item.product.stock_quantity > 0 ? (
                                                                <span className="text-green">In stock</span>
                                                            ) : (
                                                                <span className="text-red">Out of stock</span>
                                                            )
                                                        }

                                                    </td>
                                                    <td>
                                                        {
                                                            getLocalStorage("crt").filter(c => c.product_id === item.product_id).length > 0 ? (
                                                                <Link href="/cart">
                                                                    <button
                                                                        type="button"
                                                                        className="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto"
                                                                    >
                                                                        Go to Cart
                                                                    </button>
                                                                </Link>
                                                            ) : (
                                                                <button
                                                                    type="button"
                                                                    onClick={() => handleCart(item.product_id)}
                                                                    className="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-4 px-lg-5 w-100 w-md-auto"
                                                                >
                                                                    Add to Cart
                                                                </button>
                                                            )
                                                        }
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                            <>Empty </>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>) : (
                    <>
                        <h3 className="d-flex justify-content-center">
                            <img
                                rel="preload"
                                className="img-fluid empty-cart-width"
                                src="../../assets/img/empty-wishlist.png"
                                alt="wishlist-cart"
                            />
                        </h3>
                        <h4 className="d-flex justify-content-center">Your Wishlist is empty</h4>
                        <div className="mb-8 pb-0dot5 d-flex justify-content-center">
                            <Link href="/">
                                <a className="btn btn-block btn-primary-dark continue-shopping">
                                    Continue Shopping
                                </a>
                            </Link>
                        </div>
                    </>
                )}
            </div>
        </>
    );
};

export default Wishlist;
