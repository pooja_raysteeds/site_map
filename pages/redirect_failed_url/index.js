import React from 'react'
import Link from "next/link"
import MetaTagHeader from "../../components/MetaTagHeader";

const paymentFailed = () => {
    return (
        <>
            <MetaTagHeader />
            <div className="bg-gray-13 bg-md-transparent">
                <div className="container">
                    <div className="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li className="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                                    <Link href="/"><a>Home</a></Link>
                                </li>
                                <li
                                    className="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                                    aria-current="page"
                                >
                                    Payment Failure
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div className="container mr-18">
                <div className="mb-12 text-center d-flex align-items-center justify-content-center flex-column">
                    <div className="width-50 height-50 bg-red d-flex align-items-center justify-content-center rounded-circle font-weight-bold font-size-12 text-whites">
                        <i className="fa fa-times text-white lead" />
                    </div>
                    <h2 className="text-red">Transaction Failed</h2>
                    <h5>Unfortunately !! There is some issue with your transaction. Please try again.</h5>
                    <div className="d-flex mt-8 mr-6 justify-content-between align-item-center ml-5" style={{ width: "30rem" }}>
                        <Link href="/">
                            <a className="btn btn-block btn-pill font-size-16 height-50 mt-2 mr-3  border-width-2 border-color-5">
                                Continue Shopping
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </>

    )
}

export default paymentFailed
