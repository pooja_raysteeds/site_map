import React, { useState, useRef, useContext, useEffect } from "react";
import router from "next/router";
import dynamic from 'next/dynamic'
import Breadcrumb from "../../components/Breadcrumb";
const OrderDetails = dynamic(() => import('../../components/OrderDetails'))
import {
    placeOrder,
    removeAllFromCart,
    userAddresses,
    markDefaultAddress,
    addUserAddress,
} from "../../api/user";
import {
    encryptdata,
    changeOrderStatus,
    getPinCodes,
} from "../../api/checkout";
import { getErrorMessage } from "../../services/errorHandle";
import {
    getLocalStorage,
    getRandomNumber,
    setLocalStorage,
} from "../../services/helper";
import {
    API_BASE_URL_CCAVENUE,
    ccAvenueEndPoint,
    MERCHANT_ID,
    orderedId,
} from "../../services/constant";
import AppContext from "../../context/AppContext";
import AddressForm from "../../components/AddressForm";
import { isEmpty } from "lodash"
import MetaTagHeader from "../../components/MetaTagHeader";

const Checkout = () => {
    const billingAddress = {
        billing_first_name: "",
        billing_last_name: "",
        billing_company_name: "",
        billing_country: "India",
        billing_street_address: "",
        billing_apt: "",
        billing_city: "",
        billing_postal_code: 0,
        billing_state: "",
        billing_email: "",
        billing_contact_number: 0,
        gstin: "",
    };

    const [savedAddresses, setSavedAddresses] = useState([]);
    const [dispalyAddress, setAddress] = useState("Address1");
    const [currentAddress, setCurrentAddress] = useState({});
    const [pinCodes, setPinCodes] = useState(null);
    const [show, setShow] = useState({ status: "red", message: "", valid: 1 });
    const [loading, setLoading] = useState(false);
    const [campaignData, setCampaignData] = useState(null);
    const breadcrumb = [
        {
            title: "Checkout",
        },
    ];
    useEffect(() => {
        getCampaignData();
        getUserAddresses(userId);
        initializePinCodes();
    }, [loading]);

    const getCampaignData = () => {
        const res = sessionStorage.getItem("campaignData");
        const data = JSON.parse(res);

        if (!isEmpty(data)) {
            const payload = {
                campaign: data.utm_campaign,
                source: data.utm_source,
                adgroup: data.utm_adgroup,
                content: data.utm_content,
                creative: data.utm_creative
            }
            setCampaignData(payload);
        }
    }

    const getUserAddresses = async (userId) => {
        try {
            const res = await userAddresses(userId);
            setSavedAddresses(res);
            if (res[0]) setCurrentAddress(res[0]);
            else setCurrentAddress(res);
            setLoading(true);
        } catch (error) {
            getErrorMessage(error);
        }
    };

    const initializePinCodes = async () => {
        if (currentAddress)
            setOrderForm((prevState) => ({ ...orderForm, ...currentAddress }));
        try {
            const res = await getPinCodes();
            setPinCodes(res);
            if (res) {
                const flag = pinCodes.find(
                    (item) => item.pincode == currentAddress.billing_postal_code
                );
                if (flag) {
                    const status = "green";
                    const message = "Delivery is available";
                    const valid = 1;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                } else {
                    const status = "red";
                    const message = "Delivery is not available here";
                    const valid = 0;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                }
            }
        } catch (error) {
            getErrorMessage(error);
        }
    };

    const shippingAddress = {
        shipping_first_name: "",
        shipping_last_name: "",
        shipping_company_name: "",
        shipping_country: "India",
        shipping_street_address: "",
        shipping_apt: "",
        shipping_city: "",
        shipping_postal_code: 0,
        shipping_state: "",
        shipping_email: "",
        shipping_contact_number: 0,
        gstin: "",
    };

    const initialState = {
        order_number: "",
        invoice_number: "",
        products: "",
        billing_address: "",
        shipping_address: "",
        payment_method_title: "",
        cart_discount_amount: 0,
        subtotal_amount: 0,
        shipping_method_title: "",
        shipping_amount: 0,
        refund_amount: 0,
        total_amount: 0,
        total_tax_amount: 0,
        campaign: "",
        source: "",
        customer_note: "",
        ...billingAddress,
        ...shippingAddress,
    };

    const [encRequestRes, setEncRequestRes] = useState(null);
    const payementForm = useRef();
    const order_number = getRandomNumber();
    const accessCode = "AVMR93HG37AQ35RMQA";
    const userId = getLocalStorage("usrid");
    const { setUserCart, setCartCount } = useContext(AppContext);
    const [errorMsg, setErrorMsg] = useState(null);
    const [checkedShipping, setCheckedShipping] = useState(false);
    const [orderForm, setOrderForm] = useState(initialState);
    const [paymentMode, setPaymentMode] = useState("cc avenue");
    const paymentDetails = getLocalStorage("pymt");
    let cartProducts = getLocalStorage("crtl");
    const userWarranty = getLocalStorage("usrWrnty");
    const userExchange = getLocalStorage("usrExChnge");

    const handlePlaceOrder = async (e) => {
        e.preventDefault();
        const { subTotal, shippingCharge, total } = paymentDetails;
        const {
            billing_first_name,
            billing_last_name,
            billing_company_name,
            billing_country,
            billing_street_address,
            billing_apt,
            billing_city,
            billing_postal_code,
            billing_state,
            billing_email,
            billing_contact_number,
            shipping_first_name,
            shipping_last_name,
            shipping_company_name,
            shipping_country,
            shipping_street_address,
            shipping_apt,
            shipping_city,
            shipping_postal_code,
            shipping_state,
            shipping_email,
            shipping_contact_number,
            gstin,
        } = orderForm;

        // Add warranty for particuler product
        userWarranty.forEach((w) => {
            cartProducts.forEach((c) => {
                if (c.product_id == w.productId) {
                    c.product.warranty = w;
                }
            })
        })

        userExchange.forEach((e) => {
            cartProducts.forEach((c) => {
                if (c.product_id == e.productId) {
                    c.product.exchangeProduct = e;
                }
            })
        })

        let payload = {
            user_id: userId,
            order_number: order_number,
            invoice_number: getRandomNumber(),
            products: cartProducts,
            billing_address: {
                billing_first_name,
                billing_last_name,
                billing_company_name,
                billing_country,
                billing_street_address,
                billing_apt,
                billing_city,
                billing_postal_code,
                billing_state,
                billing_email,
                billing_contact_number,
                gstin,
            },
            shipping_address: {
                shipping_first_name,
                shipping_last_name,
                shipping_company_name,
                shipping_country,
                shipping_street_address,
                shipping_apt,
                shipping_city,
                shipping_postal_code,
                shipping_state,
                shipping_email,
                shipping_contact_number,
                gstin,
            },
            payment_method_title: paymentMode,
            cart_discount_amount: 0.0,
            subtotal_amount: subTotal,
            shipping_method_title: "free shippment",
            shipping_amount: shippingCharge,
            refund_amount: 0.0,
            total_amount: total,
            total_tax_amount: 0.0,
            campaign: "",
            source: "",
        };

        if (campaignData) payload = { ...payload, ...campaignData };

        placeOrder(payload)
            .then((res) => {
                setOrderForm({ ...initialState });
                setLocalStorage("ord", order_number);
                setUserCart([]);
                setCartCount(0);
                setLocalStorage("usrWrnty", []);
                setLocalStorage("usrExChnge", []);
                if (paymentMode === "cc avenue") {
                    ccAvenue();
                } else if (paymentMode === "cash on delivery") {
                    changeOrderStatus({
                        order_number,
                        order_status_id: orderedId,
                    });
                    router.push("/redirect_success_url");
                }
            })
            .catch((err) => {
                const { message } = getErrorMessage(err);
                setErrorMsg(message);
                router.push("/redirect_failed_url");
            });

        removeAllFromCart(userId)
            .then((res) => { })
            .catch((err) => {
                const { message } = getErrorMessage(err);
                setErrorMsg(message);
            });
        try {
            const payload = {
                user_id: userId,
                billing_first_name,
                billing_last_name,
                billing_company_name,
                billing_country,
                billing_street_address,
                billing_apt,
                billing_city,
                billing_postal_code,
                billing_state,
                billing_email,
                billing_contact_number,
                shipping_first_name,
                shipping_last_name,
                shipping_company_name,
                shipping_country,
                shipping_street_address,
                shipping_apt,
                shipping_city,
                shipping_postal_code,
                shipping_state,
                shipping_email,
                shipping_contact_number,
            };
            const res = await addUserAddress(payload);
        } catch (error) {
            getErrorMessage(error.message);
        }
    };

    const ccAvenue = async () => {
        const { total } = paymentDetails;
        let redirect_url = encodeURIComponent(`${API_BASE_URL_CCAVENUE}handleresponse`);
        let merchant_id = MERCHANT_ID;
        const {
            billing_company_name,
            billing_country,
            billing_street_address,
            billing_city,
            billing_postal_code,
            billing_state,
            billing_contact_number,
            shipping_first_name,
            shipping_country,
            shipping_street_address,
            shipping_city,
            shipping_postal_code,
            shipping_state,
            shipping_email,
            shipping_contact_number,
        } = orderForm;
        let request = `merchant_id=${merchant_id}&order_id=${order_number}&currency=INR&amount=${total}&redirect_url=${redirect_url}&cancel_url=${redirect_url}&language=EN&billing_name=${billing_company_name}&billing_address=${billing_street_address}&billing_city=${billing_city}&billing_state=${billing_state}&billing_zip=${billing_postal_code}&billing_country=${billing_country}&billing_tel=${billing_contact_number}&delivery_name=${shipping_first_name}&delivery_address=${shipping_street_address}&delivery_city=${shipping_city}&delivery_state=${shipping_state}&delivery_zip=${shipping_postal_code}&delivery_country=${shipping_country}&delivery_tel=${shipping_contact_number}&billing_email=${shipping_email}`;
        const params = { request };
        encryptdata({ params })
            .then((res) => {
                setEncRequestRes(res["encRequest"]);
                payementForm.current.dispatchEvent(new Event("submit"));
                payementForm.current.submit();
            })
            .catch((err) => {
                const { message } = getErrorMessage(err);
                setErrorMsg(message);
            });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        let shippingObj;
        if (name.includes("billing_")) {
            shippingObj = name.replace("billing", "shipping");
        }
        if (!checkedShipping && shippingObj) {
            setOrderForm({ ...orderForm, [name]: value, [shippingObj]: value });
        } else {
            setOrderForm({ ...orderForm, [name]: value });
        }
        if (name.includes("postal_code")) {
            const flag = pinCodes.find((item) => item.pincode == value);
            if (flag) {
                const status = "green";
                const message = "Delivery is available.";
                const valid = 1;
                setShow((prevState) => ({
                    ...prevState,
                    status,
                    message,
                    valid,
                }));
            } else {
                const status = "red";
                const message = "Delivery is not available here.";
                const valid = 0;
                setShow((prevState) => ({
                    ...prevState,
                    status,
                    message,
                    valid,
                }));
            }
        }
    };

    const handlePaymentMethod = (value) => {
        setPaymentMode(value);
    };

    const handleSetAddress = async (address) => {
        setOrderForm((prevState) => ({ ...orderForm, ...address }));
        if (pinCodes) {
            pinCodes.forEach((item) => {
                const flag = pinCodes.find(
                    (item) => item.pincode == address.billing_postal_code
                );
                if (flag) {
                    const status = "green";
                    const message = "Delivery is available";
                    const valid = 1;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                } else {
                    const status = "red";
                    const message = "Delivery is not available here";
                    const valid = 0;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                }
            });
        }
        try {
            const payload = {
                id: address.id,
                is_primary: true,
            };
            const res = await markDefaultAddress(payload);
            setCurrentAddress(address);
        } catch (error) {
            getErrorMessage(error);
        }
    };

    const handleCheckbox = (e) => {
        setCheckedShipping(!checkedShipping);
        const {
            billing_first_name,
            billing_last_name,
            billing_company_name,
            billing_country,
            billing_street_address,
            billing_apt,
            billing_city,
            billing_postal_code,
            billing_state,
            billing_email,
            billing_contact_number,
        } = orderForm;

        if (checkedShipping) {
            const obj = {
                shipping_first_name: billing_first_name,
                shipping_last_name: billing_last_name,
                shipping_company_name: billing_company_name,
                shipping_country: billing_country,
                shipping_street_address: billing_street_address,
                shipping_apt: billing_apt,
                shipping_city: billing_city,
                shipping_postal_code: billing_postal_code,
                shipping_state: billing_state,
                shipping_email: billing_email,
                shipping_contact_number: billing_contact_number,
            };
            const updatedValues = Object.assign({}, orderForm, obj);
            setAddressForm({ ...updatedValues });
        } else {
            const updatedValues = Object.assign({}, orderForm, {
                ...shippingAddress,
            });
            setAddressForm({ ...updatedValues });
        }
    };

    const {
        billing_first_name,
        billing_last_name,
        billing_company_name,
        billing_country,
        billing_street_address,
        billing_apt,
        billing_city,
        billing_postal_code,
        billing_state,
        billing_email,
        billing_contact_number,
        shipping_first_name,
        shipping_last_name,
        shipping_company_name,
        shipping_country,
        shipping_street_address,
        shipping_apt,
        shipping_city,
        shipping_postal_code,
        shipping_state,
        shipping_email,
        shipping_contact_number,
        customer_note,
        gstin,
    } = orderForm;

    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="mb-5">
                    <h1 className="text-center">Checkout</h1>
                </div>

                <form onSubmit={handlePlaceOrder}>
                    <div className="row">
                        <div className="col-lg-7 order-lg-1">
                            <div className="pb-7 mb-7">
                                {savedAddresses.length > 0 && (
                                    <div className="mx-md-auto mx-lg-0 col-md-8 col-lg-10 col-xl-12">
                                        <div className="mb-2">
                                            <div className="card p-5 border-width-2 border-color-1 borders-radius-17">
                                                <div className="text-gray-9 font-size-14 pb-2 border-color-1">
                                                    Deliver To:{" "}
                                                    <span className="text-green font-weight-bold">
                                                        {currentAddress &&
                                                            `${currentAddress.billing_first_name} ${currentAddress.billing_last_name}`}
                                                    </span>
                                                    <br />
                                                    <button
                                                        type="button"
                                                        className="btn btn-outline-danger change-add"
                                                        data-toggle="modal"
                                                        data-target="#exampleModalCenter"
                                                    >
                                                        Change Address
                                                    </button>
                                                    <span>
                                                        {currentAddress && (
                                                            <>
                                                                <span>{currentAddress.billing_company_name}</span><br />
                                                                <span>{" "}{currentAddress.billing_street_address}</span>
                                                                <span>{" "}{currentAddress.billing_city}</span>
                                                                <span>{" "}{currentAddress.billing_postal_code}</span>

                                                            </>
                                                        )
                                                        }
                                                    </span>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )}
                                {savedAddresses.length > 0 && (
                                    <button
                                        type="button"
                                        htmlFor="shippingdiffrentAddress"
                                        data-toggle="collapse"
                                        data-target="#addMoreAddress"
                                        aria-expanded="false"
                                        aria-controls="shopCartfour"
                                        className="btn btn-primary-dark-w mb-2 ml-3 mt-2"
                                    >
                                        Add New Address
                                    </button>
                                )}
                                <div
                                    id="addMoreAddress"
                                    className="collapse card p-5 border-width-2 border-color-1 borders-radius-17"
                                >
                                    <div
                                        className=" row"
                                        aria-labelledby="shopCartHeadingFour"
                                        data-parent="#shopCartAccordion3"
                                    >
                                        <AddressForm setLoading={setLoading} />
                                    </div>
                                </div>
                                <div
                                    className="modal fade"
                                    id="exampleModalCenter"
                                    tabIndex="-1"
                                    role="dialog"
                                    aria-labelledby="exampleModalCenterTitle"
                                    aria-hidden="true"
                                >
                                    <div
                                        className="modal-dialog modal-dialog-centered"
                                        role="document"
                                    >
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h6
                                                    className="modal-title"
                                                    id="exampleModalLongTitle"
                                                >
                                                    Change Delivery
                                                    Address
                                                </h6>
                                                <button
                                                    type="button"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    aria-label="Close"
                                                >
                                                    <span aria-hidden="true">
                                                        &times;
                                                    </span>
                                                </button>
                                            </div>
                                            <div className="modal-body">
                                                {savedAddresses &&
                                                    savedAddresses.map((result, index) => (
                                                        <div className="form-check mb-2" key={index}>
                                                            <input
                                                                className="form-check-input"
                                                                type="radio"
                                                                value={
                                                                    result.billing_name
                                                                }
                                                                checked={
                                                                    currentAddress.id ===
                                                                    result.id
                                                                }
                                                                name="radiovalues"
                                                                onChange={() =>
                                                                    handleSetAddress(
                                                                        result
                                                                    )
                                                                }
                                                            />
                                                            <label className="form-check-label">
                                                                {
                                                                    (
                                                                        <>
                                                                            <span>Name: {result.billing_first_name}  {result.billing_last_name}</span><br />
                                                                            <span>Address: {result.billing_street_address}</span>
                                                                            <span> {result.billing_city}</span>
                                                                            <span> {result.billing_postal_code}</span><br />
                                                                            <span>Contact Number: {result.billing_contact_number}</span>
                                                                        </>
                                                                    )
                                                                }
                                                            </label>
                                                        </div>
                                                    )
                                                    )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {savedAddresses.length == 0 && (
                                    <>
                                        <div className="border-bottom border-color-1 mb-5">
                                            <h3 className="section-title mb-0 pb-2 font-size-25">
                                                Billing Details
                                            </h3>
                                        </div>

                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        First name
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_first_name"
                                                        value={
                                                            billing_first_name
                                                        }
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Last name
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_last_name"
                                                        value={
                                                            billing_last_name
                                                        }
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="w-100" />
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Company name (optional)
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_company_name"
                                                        value={
                                                            billing_company_name
                                                        }
                                                        onChange={handleChange}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Company GSTIN (optional)
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="gstin"
                                                        value={gstin}
                                                        onChange={handleChange}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Country
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_country"
                                                        value={billing_country}
                                                        onChange={handleChange}
                                                        required
                                                        disabled
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-8">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Street address
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_street_address"
                                                        value={
                                                            billing_street_address
                                                        }
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-4">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Apt, suite, etc.
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_apt"
                                                        value={billing_apt}
                                                        onChange={handleChange}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        City
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_city"
                                                        value={billing_city}
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Postcode/Zip
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_postal_code"
                                                        value={
                                                            billing_postal_code
                                                        }
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="w-100" />
                                            <div className="col-md-12">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        State
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_state"
                                                        value={billing_state}
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Email address
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="email"
                                                        className="form-control"
                                                        name="billing_email"
                                                        value={billing_email}
                                                        onChange={handleChange}
                                                        required
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="js-form-message mb-6">
                                                    <label className="form-label">
                                                        Phone
                                                        <span className="text-danger">
                                                            *
                                                        </span>
                                                    </label>
                                                    <input
                                                        type="text"
                                                        className="form-control"
                                                        name="billing_contact_number"
                                                        value={
                                                            billing_contact_number
                                                        }
                                                        onChange={handleChange}
                                                        required
                                                        maxLength="10"
                                                    />
                                                </div>
                                            </div>
                                            <div className="w-100" />
                                        </div>

                                        <div className="border-bottom border-color-1 mb-5">
                                            <h3 className="section-title mb-0 pb-2 font-size-25">
                                                Shipping Details
                                            </h3>
                                        </div>
                                        <div
                                            id="shopCartAccordion3"
                                            className="accordion rounded mb-5"
                                        >
                                            <div className="card border-0">
                                                <div
                                                    id="shopCartHeadingFour"
                                                    className="custom-control custom-checkbox d-flex align-items-center"
                                                >
                                                    <input
                                                        type="checkbox"
                                                        className="custom-control-input"
                                                        id="shippingdiffrentAddress"
                                                        name="shippingdiffrentAddress"
                                                        onChange={
                                                            handleCheckbox
                                                        }
                                                        checked={
                                                            checkedShipping
                                                        }
                                                    />
                                                    <label
                                                        className="custom-control-label form-label"
                                                        htmlFor="shippingdiffrentAddress"
                                                        data-toggle="collapse"
                                                        data-target="#shopCartfour"
                                                        aria-expanded="false"
                                                        aria-controls="shopCartfour"
                                                    >
                                                        Ship to a different
                                                        address?
                                                    </label>
                                                </div>
                                                <div
                                                    id="shopCartfour"
                                                    className="collapse mt-5"
                                                    aria-labelledby="shopCartHeadingFour"
                                                    data-parent="#shopCartAccordion3"
                                                    style={{}}
                                                >
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    First name
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_first_name"
                                                                    value={
                                                                        shipping_first_name
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Last name
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_last_name"
                                                                    value={
                                                                        shipping_last_name
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="w-100" />
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Company name
                                                                    (optional)
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_company_name"
                                                                    value={
                                                                        shipping_company_name
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Company
                                                                    GSTIN
                                                                    (optional)
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="gstin"
                                                                    value={
                                                                        gstin
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Country
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_country"
                                                                    value={
                                                                        shipping_country
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                    disabled
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-8">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Street
                                                                    address
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_street_address"
                                                                    value={
                                                                        shipping_street_address
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Apt, suite,
                                                                    etc.
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_apt"
                                                                    value={
                                                                        shipping_apt
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    City
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_city"
                                                                    value={
                                                                        shipping_city
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Postcode/Zip
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_postal_code"
                                                                    value={
                                                                        shipping_postal_code
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="w-100" />
                                                        <div className="col-md-12">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    State
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_state"
                                                                    value={
                                                                        shipping_state
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Email
                                                                    address
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="email"
                                                                    className="form-control"
                                                                    name="shipping_email"
                                                                    value={
                                                                        shipping_email
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="js-form-message mb-6">
                                                                <label className="form-label">
                                                                    Phone
                                                                    <span className="text-danger">
                                                                        *
                                                                    </span>
                                                                </label>
                                                                <input
                                                                    type="text"
                                                                    className="form-control"
                                                                    name="shipping_contact_number"
                                                                    value={
                                                                        shipping_contact_number
                                                                    }
                                                                    onChange={
                                                                        handleChange
                                                                    }
                                                                    required
                                                                    maxLength="10"
                                                                />
                                                            </div>
                                                        </div>
                                                        {/* <div className="mb-6">
                                                            <div className="mb-3 ml-3">
                                                                <button
                                                                    type="submit"
                                                                    className="btn btn-primary-dark-w px-5"
                                                                >
                                                                    Save
                                                                    Addtress
                                                                </button>
                                                            </div>
                                                        </div> */}
                                                        <div className="w-100" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                )}
                                <div className="js-form-message mb-6 mt-6">
                                    <label className="form-label">
                                        Order notes (optional)
                                    </label>
                                    <div className="input-group">
                                        <textarea
                                            className="form-control p-5"
                                            rows={4}
                                            name="customer_note"
                                            value={customer_note}
                                            onChange={handleChange}
                                            placeholder="Notes about your order, e.g. special notes for delivery."
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 order-lg-2 mb-7 mb-lg-0">
                            <div className="pl-lg-3 ">
                                <div className="bg-gray-1 rounded-lg">
                                    <div className="p-4 mb-4 checkout-table">
                                        {/* Order Summary */}
                                        <OrderDetails
                                            cart={cartProducts}
                                            payment={paymentDetails}
                                            paymentMethod={handlePaymentMethod}
                                        />
                                        {pinCodes &&
                                            pinCodes.find(
                                                (item) =>
                                                    item.pincode ==
                                                    shipping_postal_code &&
                                                    item.pincode ==
                                                    billing_postal_code
                                            ) &&
                                            show.valid ? (
                                            <button
                                                type="submit"
                                                className="btn btn-primary-dark-w btn-block btn-pill font-size-20 mb-3 py-3"
                                            >
                                                Place Order
                                            </button>
                                        ) : (
                                            <button
                                                type="submit"
                                                className="btn btn-primary-dark-w btn-block btn-pill font-size-20 mb-3 py-3"
                                                disabled
                                            >
                                                Place Order
                                            </button>
                                        )}
                                    </div>
                                </div>
                            </div>
                            {errorMsg ? (
                                <p className="text-red mb-4 text-center">
                                    {errorMsg}
                                </p>
                            ) : (
                                <></>
                            )}
                            <div>
                                <span
                                    className={`ml-3 mt-2 font-size-20 text-${show.status}`}
                                >
                                    {show.message}
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
                <form
                    ref={payementForm}
                    id="paymentBox"
                    method="post"
                    name="redirect"
                    action={ccAvenueEndPoint}
                >
                    <input
                        type="hidden"
                        id="encRequest"
                        name="encRequest"
                        value={encRequestRes}
                    />
                    <input
                        type="hidden"
                        name="access_code"
                        id="access_code"
                        value={accessCode}
                    />
                </form>
            </div>
        </>
    );
};

export default Checkout;
