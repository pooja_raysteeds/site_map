import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import dateFormat from "dateformat";
import { getLocalStorage } from "../../services/helper"
import { getOrderDetails } from "../../api/checkout"
import { getErrorMessage } from "../../services/errorHandle"
import MetaTagHeader from "../../components/MetaTagHeader";


const paymentSuccess = () => {
    const orderNumber = getLocalStorage("ord")
    const [successInfo, setSuccessInfo] = useState({})

    useEffect(() => {
        getOrderDetails(orderNumber).then(
            res => setSuccessInfo(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    const { order_number, created_at, payment_method_title, total_amount, user } = successInfo
    return (
        <>
            <MetaTagHeader />
            <div className="bg-gray-13 bg-md-transparent">
                <div className="container">
                    <div className="my-md-3">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                                <li className="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                                    <Link href="/"><a>Home</a></Link>
                                </li>
                                <li
                                    className="breadcrumb-item flex-shrink-0 flex-xl-shrink-1 active"
                                    aria-current="page"
                                >
                                    Payment Success
                                </li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div className="container mr-18">
                <div className="mb-12 text-center d-flex align-items-center justify-content-center flex-column">
                    <div className="width-50 height-50 bg-green d-flex align-items-center justify-content-center rounded-circle font-weight-bold font-size-12 text-whites">
                        <i className="fa fa-check text-white lead" />
                    </div>
                    <h2 className="text-green">Payment Successful</h2>
                    <h5>Thank you. Your order has been received.</h5>
                    <div className="d-flex flex-column justify-content-around mt-5 mob-redirect" style={{ width: "40rem" }}>
                        <div className=" d-flex justify-content-between align-item-center order-successfont"> <strong>Order Number:</strong> <h6 className="order-successfont">{order_number}</h6></div>
                        <div className=" d-flex justify-content-between align-item-center order-successfont"> <strong>Date:</strong> <h6 className="order-successfont">{dateFormat(created_at, "fullDate")}</h6></div>
                        <div className=" d-flex justify-content-between align-item-center order-successfont"> <strong>Email:</strong>  <h6 className="order-successfont">{user?.email}</h6></div>
                        <div className=" d-flex justify-content-between align-item-center order-successfont"> <strong>Total:</strong> <h6 className="order-successfont">₹{total_amount}</h6></div>
                        <div className=" d-flex justify-content-between align-item-center order-successfont"> <strong>Payment Method:</strong><h6 className="order-successfont">{payment_method_title}</h6> </div>
                    </div>
                    <div className="d-flex mt-8 mr-6 justify-content-between align-item-center ml-5 mob-redirect" style={{ width: "30rem" }}>
                        <Link href="/">
                            <a className="btn btn-block btn-pill height-50 mt-2 mr-3  border-width-2 border-color-5 successorder">
                                Continue Shopping
                            </a>
                        </Link>
                        <Link href="/orders">
                            <a className="btn btn-primary-dark-w btn-block btn-pill font-size-16">
                                View Order
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}

export default paymentSuccess
