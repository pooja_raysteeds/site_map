import React from 'react'
import MetaTagHeader from "../../components/MetaTagHeader";
import Breadcrumb from "../../components/Breadcrumb";
const breadcrumb = [
    {
        title: "Terms of Use",
    },
];

const termuse = () => {
    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="mb-12 text-center">
                    <h1>TERMS OF USE </h1>
                </div>
                <p className="text-gray-44">

                    For the purpose of these Terms of Use, wherever the context so requires “You” or “User” shall mean any natural or legal person who has agreed to become a buyer on Platform by providing data while registering on the Platform as Registered User. The term “Top Ten Electronic Shoppe”, “We”,” Us”,” Our” shall mean Top Ten Electronic Shoppe and its affiliates.

                </p>
                <p>
                    Your use of this website and services and tools are governed by the following terms and conditions (“Terms of Use”) including the applicable policies which are incorporated herein by way of reference. By mere use of this website, you shall be contracting with Top Ten Electronic Shoppe, the owner of the Platform. These terms and conditions including the policies constitute Your binding obligations, with Top Ten Electronic Shoppe
                </p>
                <p>
                    In no event shall Top Ten Electronic Shoppe and its website www.toptenelectronics.in be liable for any special, incidental, indirect or consequential damages of any kind in connection with these terms and conditions even if user has been informed in advance of the possibility of such damages.
                </p>
                <p>
                    Top Ten Electronic Shoppe and its website www.toptenelectronics.in will not be liable to you in any way or in relation to the contents of, or use of, or otherwise in connection with, this website. Top Ten Electronic Shoppe does not warrant that this site; information, content, materials, product (including software) or services included on or otherwise made available to you through this site; their servers; or electronic communication sent from Top Ten Electronics Shoppe are free of viruses or other harmful components.
                </p>
                <p>
                    Nothing on this website constitutes, or is meant to constitute, advice of any kind.
                </p>
                <p>
                    Top Ten Electronic Shoppe and its website www.toptenelectronics.in tries to be as accurate as possible. However, www.toptenelectronics.in does not warrant that product description or other content of this site is accurate, complete, reliable, current, or error-free. If a product offered by Top Ten Electronic itself is not as described, your sole remedy is to return it in unused condition.
                </p>
                <p>
                    Pricing on the product is as it shows, however due to any technical issue, typographical error or product information received from our suppliers’ prices of the product may vary. The price taken into consideration will be at the time of order confirmation and payment done by the customer. If www.toptenelectronics.in comes across any such difference in pricing due to any issues, it has all the rights to rectify the same or cancel the order.
                </p>
                <p>
                    You agree to defend, indemnify and hold harmless Top Ten Electronic Shoppe, its website www.toptenelectronics.in, its employees, directors, officers, agents and their successors and assigns from and against any and all claims, liabilities, damages, losses, costs and expenses, including attorney’s fees, caused by or arising out of claims based upon your actions or inactions, which may result in any loss or liability to Top Ten Electronics or any third party including but not limited to breach of any warranties, representations or undertakings or in relation to the non-fulfilment of any of your obligations under this User Agreement or arising out of the your violation of any applicable laws, regulations including but not limited to Intellectual Property Rights, payment of statutory dues and taxes, claim of libel, defamation, violation of rights of privacy or publicity, loss of service by other subscribers and infringement of intellectual property or other rights. This clause shall survive the expiry or termination of this User Agreement.
                </p>
                <p>
                    These terms and conditions are governed by and to be interpreted in accordance with laws of India, without regard to the choice or conflicts of law provisions of any jurisdiction. You agree, in the event of any dispute arising in relation to these terms and conditions or any dispute arising in relation to the web site whether in contract or otherwise, to submit to the jurisdiction of the courts located at Mumbai, India for the resolution of all such disputes.
                </p>
                <p>
                    When You use any of the services provided by Us through the Platform, including but not limited to, (e.g., Product Reviews), You will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be considered as part and parcel of this Terms of Use. We reserve the right, at Our sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time without any prior written notice to You. You shall ensure to review these Terms of Use periodically for updates/changes. Your continued use of the Platform following the posting of changes will mean that You accept and agree to the revisions. As long as You comply with these Terms of Use, we grant You a personal, non-exclusive, non-transferable, limited privilege to enter and use the Platform. By impliedly or expressly accepting these Terms of Use, you also accept and agree to be bound by our Policies including but not limited to Privacy Policy as amended from time to time.
                </p>
                <p>
                    The content of the pages of this website is for your general information and use only. It is subject to change without notice.
                </p>
                <p>
                    This website uses cookies to monitor browsing preferences. If you do allow cookies to be used, the following personal information may be stored by us for use by third parties
                </p>
                <p>
                    Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.
                </p>
                <p>
                    Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.
                </p>
                <p>
                    This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.
                </p>
                <p>
                    All trademarks reproduced in this website which are not the property of, or licensed to, the operator is acknowledged on the website.
                </p>
                <p>
                    Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offence.
                </p>
                <p>
                    From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).
                </p>
                <p>
                    The information contained in this website is for general information purposes only. The information is provided by <a href="">www.toptenelectronics.in </a> and while we endeavour to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.
                </p>
                <p>
                    In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website.
                </p>
                <p>
                    Through this website you are able to link to other websites which are not under the control of <a href="">www.toptenelectronics.in</a>. We have no control over the nature, content and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.
                </p>
                <p>
                    Every effort is made to keep the website up and running smoothly. However, www.toptenelectronics.in takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.
                </p>



            </div>
        </>
    )
}

export default termuse
