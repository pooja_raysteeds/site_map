import React, { useEffect, useState, useContext } from "react";
import Link from "next/link";
import _ from "lodash";
import { getUserCart, getCartProducts, removeFromCart, updateCartQty } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import { getLocalStorage, setLocalStorage, getS3Url } from "../../services/helper"
import AppContext from "../../context/AppContext"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";


const Cart = () => {
    const [cartList, setCartList] = useState([]);
    const userId = getLocalStorage("usrid");
    const cartValue = getLocalStorage("crt")
    const { setUserCart } = useContext(AppContext);
    const userWarranty = getLocalStorage("usrWrnty");
    const userExchange = getLocalStorage("usrExChnge");
    const breadcrumb = [
        {
            title: "Cart",
        },
    ];
    useEffect(() => {
        getCartProduct()
    }, []);

    const getCartProduct = () => {
        if (userId) {
            getUserCart(userId).then(
                res => {
                    setCartList(res)
                    setLocalStorage("crtl", res)
                    const data = res.map(c => ({ product_id: c.product_id, quantity: c.quantity }))
                    setUserCart(data)
                }
            ).catch(
                err => getErrorMessage(err)
            )
        } else {
            const carts = getLocalStorage('crt');
            const ids = carts.map(item => item.product_id);
            getCartProducts(ids).then(
                res => {
                    const merged = _.map(res, (item => _.assign({ product: item }, _.find(carts, ['product_id', item.id]))));
                    setCartList(merged)
                }
            ).catch(
                err => getErrorMessage(err)
            )
        }
    }

    const removeCartProduct = (product_id) => {
        const filtered = cartValue.filter(p => p.product_id !== product_id)
        const payload = { product_id, user_id: userId }
        const updatedWrnty = userWarranty.filter((item) => { return item.productId != product_id });
        setLocalStorage("usrWrnty", updatedWrnty);
        if (userId) {
            removeFromCart(payload).then(
                res => {
                    setUserCart(filtered)
                    getCartProduct()
                }
            ).catch(err =>
                getErrorMessage(err)
            )
        } else {
            setUserCart(filtered)
            getCartProduct()
        }
    }

    const handleQuantity = (product_id, quantity) => {
        const data = { product_id, quantity }
        const updatedCart = _.map(cartValue, (p => p.product_id === product_id ? { ...p, quantity: p.quantity += quantity } : p));
        if (userId) {
            const payload = { ...data, user_id: userId }
            updateCartQty(payload).then(
                res => getCartProduct()
            ).catch(
                err => getErrorMessage(err)
            )
        } else {
            setUserCart(updatedCart)
            getCartProduct()
        }
    }

    const cartDetails = () => {
        if (cartList.length === 0) return;
        let subTotal = 0;
        let mrpAmount = 0;
        let savedAmount = 0;
        let shippingCharge = 0;
        let total = 0;
        let warrantyAmount = 0;
        let exchangeAmount = 0;
        cartList.map((item) => {
            subTotal += item.product.selling_price * item.quantity;
            mrpAmount += item.product.mrp * item.quantity;
        })
        savedAmount = mrpAmount - subTotal;
        // Check for warranty amount
        userWarranty.forEach(w => {
            cartList.forEach(c => {
                if (w.productId == c.product_id) {
                    warrantyAmount += parseInt(w.amount);
                }
            })
        });
        // Check for exchange amount
        userExchange.forEach(e => {
            cartList.forEach(c => {
                if (e.productId == c.product_id) {
                    exchangeAmount += parseInt(e.amount);
                }
            })
        })
        total = (subTotal + shippingCharge + warrantyAmount) - exchangeAmount;
        setLocalStorage("pymt", { subTotal, savedAmount, shippingCharge, total, warrantyAmount, exchangeAmount })

        return (
            <div className="mb-8 cart-total">
                <div className="row">
                    <div className="col-xl-5 col-lg-6 offset-lg-6 offset-xl-7 col-md-8 offset-md-4">
                        <div className="border-bottom border-color-1 mb-3">
                            <h3 className="d-inline-block section-title mb-0 pb-2 font-size-26">
                                Cart Summary
                            </h3>
                        </div>
                        <table className="table mb-3 mb-md-0">
                            <tbody>
                                <tr className="cart-subtotal">
                                    <th>Subtotal</th>
                                    <td data-title="Subtotal">
                                        <span className="amount">
                                            ₹{subTotal}
                                        </span>
                                    </td>
                                </tr>
                                {
                                    warrantyAmount > 0 && (
                                        <tr className="cart-subtotal">
                                            <th className="text-green">Warranty</th>
                                            <td data-title="Subtotal">
                                                <span className="amount text-green">
                                                    ₹{warrantyAmount}
                                                </span>
                                            </td>
                                        </tr>
                                    )
                                }
                                {
                                    exchangeAmount > 0 && (
                                        <tr className="cart-subtotal">
                                            <th className="text-red">Exchange Product Amount</th>
                                            <td data-title="Subtotal">
                                                <span className="amount text-red">
                                                    - ₹{exchangeAmount}
                                                </span>
                                            </td>
                                        </tr>
                                    )
                                }
                                <tr className="shipping">
                                    <th>Total Saved</th>
                                    <td data-title="Shipping">
                                        <span className="amount text-green">₹{savedAmount}</span>
                                    </td>
                                </tr>
                                <tr className="shipping">
                                    <th>Shipping</th>
                                    <td data-title="Shipping">
                                        Free Shipping:{" "}
                                        <span className="amount">₹{shippingCharge}</span>
                                    </td>
                                </tr>
                                <tr className="cart-subtotal">
                                    <th>Total Amount</th>
                                    <td data-title="Subtotal">
                                        <span className="amount">
                                            ₹{total}
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <>
            <MetaTagHeader />
            <div className="container">
                <Breadcrumb data={breadcrumb} />
                <div>
                    <h1 className="text-center ecart-mb0">My Cart</h1>
                </div>
                {
                    cartList.length > 0 ? (
                        <div className="mb-10 cart-table largeviewprodetails">
                            <form className="mb-4">
                                <table className="table" cellSpacing={0}>
                                    <thead>
                                        <tr>
                                            <th className="product-remove">&nbsp;</th>
                                            <th className="product-thumbnail">&nbsp;</th>
                                            <th className="product-name">Product</th>
                                            {
                                                userExchange.length > 0 && (
                                                    <th className="product-name">Exchange Product</th>
                                                )
                                            }
                                            <th className="product-price">Inc.GST</th>
                                            <th className="product-price">Price</th>
                                            <th className="product-quantity w-lg-15">
                                                Quantity
                                            </th>
                                            <th className="product-subtotal">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {cartList ? (
                                            cartList.map(item => (
                                                <tr key={item.product_id} className="border-bottom">
                                                    <td className="text-center">
                                                        <a
                                                            onClick={() => removeCartProduct(item.product_id)}
                                                            className="text-gray-32 font-size-26 cursor-pointer-on"
                                                        >
                                                            <i className="ec ec-close-remove mr-1 font-size-15" />
                                                        </a>
                                                    </td>
                                                    <td className="d-none d-md-table-cell">
                                                        <Link href={`/shop/${item.product.slug_url}`}>
                                                            <a>
                                                                <img
                                                                    rel="preload"
                                                                    className="img-fluid max-width-100 p-1 border border-color-1"
                                                                    src={getS3Url(item.product.galleries[0].image_url)}
                                                                    alt="Image Description"
                                                                />
                                                            </a>
                                                        </Link>
                                                    </td>
                                                    <td data-title="Product" className="width-20625">
                                                        <Link href={`/shop/${item.product.slug_url}`}>
                                                            <a className="text-gray-90">
                                                                {item.product.title}
                                                            </a>
                                                        </Link>
                                                        {
                                                            userWarranty && userWarranty.map((w, index) => {
                                                                if (w.productId == item.product_id) {
                                                                    return (
                                                                        <p key={index} className="text-green">{`Extended Warranty ${w.warranty} years`}</p>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                    </td>
                                                    {
                                                        userExchange && (userExchange.filter(e => e.productId == item.product_id).length > 0 ? (
                                                            <>
                                                                {
                                                                    userExchange.map(e => {
                                                                        if (e.productId == item.product_id) {
                                                                            return (
                                                                                <td key={e.productId} data-title="Exchange Product" className="width-1250">
                                                                                    <span>{`${e.item} - ₹${e.amount}/-`}</span>
                                                                                </td>
                                                                            )
                                                                        }
                                                                    })
                                                                }
                                                            </>
                                                        ) : (
                                                            <></>
                                                        ))
                                                    }
                                                    <td data-title="Inc.GST">
                                                        ₹{
                                                            <span>{(parseInt(item.product.selling_price) -  (item.product.selling_price * (100 / (100 +  item.product.gst_percent)))).toFixed(2)}</span>
                                                        }
                                                    </td>
                                                    <td data-title="Price">
                                                        <span>
                                                            ₹{item.product.selling_price}
                                                        </span><br />
                                                    </td>
                                                    <td data-title="Quantity">
                                                        <span className="sr-only">
                                                            Quantity
                                                        </span>
                                                        <div className="border rounded-pill py-1 width-122 px-3 border-color-1">
                                                            <div className="js-quantity row align-items-center">
                                                                <div className="col">
                                                                    <span>{item.quantity}</span>
                                                                </div>
                                                                <div className="col-auto pr-1">
                                                                    <a
                                                                        className="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 cursor-pointer-on"
                                                                        onClick={item.quantity <= 1 ?
                                                                            () => removeCartProduct(item.product_id) :
                                                                            () => handleQuantity(item.product_id, -1)
                                                                        }
                                                                    >
                                                                        <small className="fas fa-minus btn-icon__inner" />
                                                                    </a>
                                                                    <a
                                                                        className="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 cursor-pointer-on"
                                                                        onClick={() => handleQuantity(item.product_id, +1)}
                                                                    >
                                                                        <small className="fas fa-plus btn-icon__inner" />
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td data-title="Total">
                                                        <span>
                                                            ₹{item.product.selling_price * item.quantity}
                                                        </span> <br />
                                                        {
                                                            userWarranty && userWarranty.map((w, index) => {
                                                                if (w.productId == item.product_id) {
                                                                    return (
                                                                        <span key={index} className="text-green" style={{ fontSize: "14px" }}>{`+ ${w.amount}`}</span>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                        {
                                                            userExchange && userExchange.map((e, index) => {
                                                                if (e.productId == item.product_id) {
                                                                    return (
                                                                        <p key={index} className="text-red" style={{ fontSize: "14px" }}>{`- ₹${e.amount}`}</p>
                                                                    )
                                                                }
                                                            })
                                                        }
                                                    </td>
                                                </tr>
                                            ))
                                        ) : (
                                            <></>
                                        )}

                                        <tr>
                                            <td
                                                colSpan={6}
                                                className="space-top-2 justify-content-center"
                                            >
                                                <div className="pt-md-3">
                                                    <div className="d-block d-md-flex flex-center-between">
                                                        <div className="mb-3 mb-md-0 w-xl-40">
                                                            {/* <label
                                                            className="sr-only"
                                                            htmlFor="subscribeSrEmailExample1"
                                                        >
                                                            Coupon code
                                                        </label>
                                                        <div className="input-group">
                                                            <input
                                                                type="text"
                                                                className="form-control"
                                                                name="text"
                                                                id="subscribeSrEmailExample1"
                                                                placeholder="Coupon code"
                                                                aria-label="Coupon code"
                                                                aria-describedby="subscribeButtonExample2"
                                                                required
                                                            />
                                                            <div className="input-group-append">
                                                                <button
                                                                    className="btn btn-block btn-dark px-4"
                                                                    type="button"
                                                                    id="subscribeButtonExample2"
                                                                >
                                                                    <i className="fas fa-tags d-md-none" />
                                                                    <span className="d-none d-md-inline">
                                                                        Apply coupon
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div> */}
                                                        </div>
                                                        <div className="d-md-flex">
                                                            <Link href={userId ? '/checkout' : '/login'}>
                                                                <a
                                                                    className="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-md-inline-block"
                                                                >
                                                                    Proceed to Checkout
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    ) : (
                        <div className="largeviewprodetails">
                            <h3 className="d-flex justify-content-center empty-cart ecart-mb0">
                                <img rel="preload" className="img-fluid empty-cart-width"
                                    src="../../assets/img/emptycart.png"
                                    alt="empty cart" />
                            </h3>
                            <div className="mb-8 py-md-3 d-flex justify-content-center empty-cart">
                                <Link href="/">
                                    <a className="btn btn-block btn-primary-dark continue-shopping">
                                        Continue Shopping
                                    </a>
                                </Link>
                            </div>
                        </div>
                    )
                }

                {/* Mobile View */}
                {
                    cartList.length > 0 ? (
                        <section className="mobileviewprodetails">
                            <div className="row">
                                {cartList ? (
                                    cartList.map(item => (
                                        <div className="col-lg-8" key={item.product_id}>
                                            <div className="mb-3">
                                                <div className="pt-4 wish-list">
                                                    <div className="row mb-4">
                                                        <div className="col-md-5 col-lg-3 col-xl-3">
                                                            <div className="view zoom overlay z-depth-1 rounded mb-3 mb-md-0">
                                                                <Link href={`/shop/${item.product.slug_url}`}>
                                                                    <a>
                                                                        <img
                                                                            rel="preload"
                                                                            className="img-fluid max-width-100 p-1 border border-color-1"
                                                                            src={getS3Url(item.product.galleries[0].image_url)}
                                                                            alt="Image Description"
                                                                        />
                                                                    </a>
                                                                </Link>
                                                                <a
                                                                    onClick={() => removeCartProduct(item.product_id)}
                                                                    className="text-gray-32 font-size-26 cursor-pointer-on ml-19 removepro"
                                                                >
                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-7 col-lg-9 col-xl-9">
                                                            <div>
                                                                <div className="d-flex justify-content-between">
                                                                    <div>
                                                                        <p className="mb-3 text-uppercase small"><strong><Link href={`/shop/${item.product.slug_url}`}>
                                                                            <a className="text-gray-90">
                                                                                {item.product.title}
                                                                            </a>
                                                                        </Link><br />
                                                                            {
                                                                                userWarranty && userWarranty.map((w, index) => {
                                                                                    if (w.productId == item.product_id) {
                                                                                        return (
                                                                                            <span key={index} className="text-green text-uppercase small">{`Extended Warranty ${w.warranty} years`}</span>
                                                                                        )
                                                                                    }
                                                                                })
                                                                            }
                                                                            <br /> {
                                                                                userExchange && userExchange.map((e, index) => {
                                                                                    if (e.productId == item.product_id) {
                                                                                        return (
                                                                                            <span key={index} className="text-red text-uppercase small">{`${e.item} - ₹${e.amount}/-`}</span>
                                                                                        )
                                                                                    }
                                                                                })
                                                                            }
                                                                            <p className="mb-3 text-uppercase">Inc.GST</p>
                                                                        </strong></p>
                                                                    </div>
                                                                    <div>

                                                                        <span id="passwordHelpBlock" className="form-text text-center">
                                                                            ₹{item.product.selling_price}  </span><br />
                                                                        <div className="my-4">
                                                                            <span style={{ fontSize: "14px", display: "block" }}>
                                                                                {(parseInt(item.product.selling_price) * 0.28).toFixed(2)}
                                                                            </span>
                                                                            {
                                                                                userWarranty && userWarranty.map((w, index) => {
                                                                                    if (w.productId == item.product_id) {
                                                                                        return (
                                                                                            <span key={index} className="text-green" style={{ fontSize: "14px" }}>{`+ ₹${w.amount}`}</span>
                                                                                        )
                                                                                    }
                                                                                })
                                                                            }
                                                                            {
                                                                                userExchange && userExchange.map((e, index) => {
                                                                                    if (e.productId == item.product_id) {
                                                                                        return (
                                                                                            <p key={index} className="text-red" style={{ fontSize: "14px", display: "block" }}>{`- ₹${e.amount}`}</p>
                                                                                        )
                                                                                    }
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div className="d-flex justify-content-between align-items-center">
                                                                    <div className="border rounded-pill py-1 width-122 px-3 border-color-1">
                                                                        <div className="js-quantity row align-items-center">
                                                                            <div className="col">
                                                                                <span>{item.quantity}</span>
                                                                            </div>
                                                                            <div className="col-auto pr-1">
                                                                                <a
                                                                                    className="js-minus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 cursor-pointer-on"
                                                                                    onClick={item.quantity <= 1 ?
                                                                                        () => removeCartProduct(item.product_id) :
                                                                                        () => handleQuantity(item.product_id, -1)
                                                                                    }
                                                                                >
                                                                                    <small className="fas fa-minus btn-icon__inner" />
                                                                                </a>
                                                                                <a
                                                                                    className="js-plus btn btn-icon btn-xs btn-outline-secondary rounded-circle border-0 cursor-pointer-on"
                                                                                    onClick={() => handleQuantity(item.product_id, +1)}
                                                                                >
                                                                                    <small className="fas fa-plus btn-icon__inner" />
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <p className="mb-0"><span><strong> ₹{item.product.selling_price * item.quantity}</strong></span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <hr className="mb-4" />

                                                </div>
                                            </div>
                                        </div>
                                    ))
                                ) : (
                                    <></>
                                )}

                            </div>
                            <div className="d-md-flex mb-3">
                                <Link href={userId ? '/checkout' : '/login'}>
                                    <a
                                        className="btn btn-primary-dark-w ml-md-2 px-5 px-md-4 px-lg-5 w-100 w-md-auto d-md-inline-block"
                                    >
                                        Proceed to Checkout
                                    </a>
                                </Link>
                            </div>
                        </section>) : (<div className="mobileviewprodetails">
                            <h3 className="d-flex justify-content-center empty-cart ecart-mb0">
                                <img rel="preload" className="img-fluid empty-cart-width"
                                    src="../../assets/img/emptycart.png"
                                    alt="empty cart" />
                            </h3>
                            <div className="mb-8 pb-0dot5 d-flex justify-content-center empty-cart">
                                <Link href="/">
                                    <a className="btn btn-block btn-primary-dark continue-shopping">
                                        Continue Shopping
                                    </a>
                                </Link>
                            </div>
                        </div>)
                }
                {/* End Mobile View */}
                {cartDetails()}

            </div>
        </>
    );
};

export default Cart;
