import MetaTagHeader from "../../components/MetaTagHeader";
import Breadcrumb from "../../components/Breadcrumb";
const breadcrumb = [
    {
        title: " Privacy policy",
    },
];
const privacypolicy = () => {
    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="mb-12 text-center">
                    <h1>PRIVACY POLICY</h1>
                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Collection of Personally Identifiable Information and other Information</h3>
                    <p>When you use our website, we collect and store your personal information which is provided by you from time to time. This allows us to provide services and features that most likely meet your needs, and to customize our website to make your experience safer and easier. More importantly, while doing so we collect personal information from you that we consider necessary for achieving this purpose.</p>
                    <p>In general, you can browse the Website without telling us who you are or revealing any personal information about yourself. Once you give us your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional. You always have the option to not provide information by choosing not to use a particular service or feature on the Website. We may automatically track certain information about you based upon your behaviour on our Website. We use this information to do internal research on our users’ demographics, interests, and behaviour to better understand, protect and serve our users. This information is compiled and analysed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on our website or not), which URL you next go to (whether this URL is on our website or not), your computer browser information, and your IP address. We use data collection devices such as “cookies” on certain pages of the Website to help analyse our web page flow, measure promotional effectiveness, and promote trust and safety. “Cookies” are small files placed on your hard drive that assist us in providing our services. We offer certain features that are only available through the use of a “cookie”. We also use cookies to allow you to enter your password less frequently during a session. Cookies can also help us provide information that is targeted to your interests. Most cookies are “session cookies,” meaning that they are automatically deleted at the end of a session. You are always free to decline our cookies if your browser permits, although in that case you may not be able to use certain features on the Website and you may be required to re-enter your password more frequently during a session.
                        Additionally, you may encounter “cookies” or other similar devices on certain pages of the Website that are placed by third parties. We do not control the use of cookies by third parties.
                    </p>
                    <ol>
                        <li>
                            If you choose to buy on the Website, we collect information about your buying behaviour.
                        </li>
                        <li>
                            If you transact with us, we collect some additional information, such as a billing address, a credit / debit card number and a credit / debit card expiration date and/ or other payment instrument details and tracking information from cheques or money orders.
                        </li>
                        <li>
                            If you choose to leave feedback, we will collect that information you provide to us. We retain this information as necessary to resolve disputes, provide customer support and troubleshoot problems as permitted by law.
                        </li>
                        <li>
                            If you send us personal correspondence, such as emails or letters, or if other users or third parties send us correspondence about your activities or postings on the Website, we may collect such information into a file specific to you.
                        </li>
                        <li>
                            We collect personally identifiable information (email address, name, phone number, credit card / debit card / other payment instrument details, etc.) from you when you set up a free account with us. While you can browse some sections of our Website without being a registered member, certain activities (such as placing an order) do require registration. We do use your contact information to send you offers based on your previous orders and your interests.
                        </li>

                    </ol>
                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Use of Demographic / Profile Data / Your Information</h3>
                    <p>
                        We use personal information to provide the services you request. To the extent we use your personal information to market to you, we will provide you the ability to opt-out of such uses. We use your personal information to resolve disputes; troubleshoot problems; help promote a safe service; collect money; measure consumer interest in our products and services, inform you about online and offline offers, products, services, and updates; customize your experience; detect and protect us against error, fraud and other criminal activity; enforce our terms and conditions; and as otherwise described to you at the time of collection.
                    </p>
                    <p>
                        With your consent, we will have access to your SMS, contacts in your directory, location and device information. We may share this data with our affiliates. In the event that consent to this such use of data is withdrawn in the future, we will stop collection of such data but continue to store the data and use it for internal purposes to further improve our services.
                    </p>
                    <p>
                        In our efforts to continually improve our product and service offerings, we collect and analyse demographic and profile data about our users’ activity on our website.
                    </p>
                    <p>
                        We identify and use your IP address to help diagnose problems with our server, and to administer our website. Your IP address is also used to help identify you and to gather broad demographic information.
                    </p>


                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Sharing of personal information</h3>
                    <p className="text-gray-90">
                        We may share personal information with our other corporate entities and affiliates. These entities and affiliates may market to you as a result of such sharing unless you explicitly opt-out.
                    </p>
                    <p>
                        We may disclose personal information to third parties. This disclosure may be required for us to provide you access to our Services, to comply with our legal obligations, to enforce our User Agreement, to facilitate our marketing and advertising activities, or to prevent, detect, mitigate, and investigate fraudulent or illegal activities related to our Services. We do not disclose your personal information to third parties for their marketing and advertising purposes without your explicit consent.
                    </p>
                    <p>
                        We may disclose personal information if required to do so by law or in the good faith belief that such disclosure is reasonably necessary to respond to subpoenas, court orders, or other legal process. We may disclose personal information to law enforcement offices, third party rights owners, or others in the good faith belief that such disclosure is reasonably necessary to: enforce our Terms or Privacy Policy; respond to claims that an advertisement, posting or other content violates the rights of a third party; or protect the rights, property or personal safety of our users or the general public.
                    </p>
                    <p>
                        We and our affiliates will share / sell some or all of your personal information with another business entity should we (or our assets) plan to merge with, or be acquired by that business entity, or re-organization, amalgamation, restructuring of business. Should such a transaction occur that other business entity (or the new combined entity) will be required to follow this privacy policy with respect to your personal information.
                    </p>
                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Security Precautions</h3>
                    <p className="text-gray-90">
                        Our website has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.
                    </p>
                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Your Consent</h3>
                    <p className="text-gray-90">
                        By using the Website and/ or by providing your information, you consent to the collection and use of the information you disclose on the Website in accordance with this Privacy Policy, including but not limited to Your consent for sharing your information as per this privacy policy.
                    </p>
                </div>
                <div className="mb-10">
                    <h3 className="mb-6 pb-2 font-size-25">Grievance Officer</h3>
                    <p className="text-gray-90">
                        In accordance with Information Technology Act 2000 and rules made there under, please contact us regarding any questions regarding this statement on <a href="#" className="text-blue font-weight-bold"> topten.customercare@gmail.com </a>

                    </p>
                </div>
            </div>
        </>
    )
}

export default privacypolicy
