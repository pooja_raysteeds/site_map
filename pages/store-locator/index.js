import React from "react"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";
const breadcrumb = [
    {
        title: "Store Locator",
    },
];

const StoreLocator = () => {

    const storeLocator = [
        {
            branch: "VASHI",
            shop: "L.G. SHOPPE, SHOP NO.8A,GROUND FLOOR",
            address: "REAL TECH PARK,PLOT-39/2,SEC.30A",
            city: "VASHI-400703",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "KAMOTHE",
            shop: "SHOP NO.18/19/20,SHEETAL DHARA COMPLEX",
            address: "PLOT NO.28, SECTOR-07",
            city: "KAMOTHE-400209",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "AIROLI",
            shop: "L.G. SHOPPE, SHOP NO.1/2/3/4",
            address: "PILLANI CHS, SECTOR NO.3",
            city: "AIROLI-400708",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "GHANSOLI",
            shop: "L.G. SHOPPE,1/2/3 MANOSHI",
            address: "SECTOR NO.3, OPP.RLY.STN.",
            city: "GHANSOLI-400701",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "TALOJA",
            shop: "SHOP NO. 17/18, PLOT NO.22",
            address: "SEC-4, PREMCHAND TALOJA",
            city: " NAVI MUMBAI -410208",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "ULWE",
            shop: "GAMI TRIXIE SHOP NO.14/15 ",
            address: "PLOT NO.187, SEC-20, NR - CIDCO COMPLEX ",
            city: "ULWE -410206",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "TNX",
            shop: "SHOP NO.7/8, GROUND FLOOR,  ",
            address: "REAL TECH PARK PLOT NO.39 /2",
            city: "VASHI NAVI MUMBAI -400703",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "SAMSUNG",
            shop: "SHOP NO.11/12 MANOSHI,PLOT NO.5/6 ",
            address: "ECTOR NO.3, OPP.RLY.STN.",
            city: "SGHANSOLI-400701",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },
        {
            branch: "KHARGHAR",
            shop: "SHOP NO.14, RADHA KRISHNA COMPLEX",
            address: "SEC-11, KHARGHAR",
            city: "NAVI MUMBAI -410210",
            phone: "022 – 62676886",
            email: "topten.customercare@gmail.com"
        },

    ]
    return (
        <div className="mt-5">
            <MetaTagHeader />
            <div className="container mb-8">
                <Breadcrumb data={breadcrumb} />
                <div className="mb-5">
                    <h1 className="text-center">Find a Store or Service Center</h1>
                </div>
                <div className="col-lg-5 col-xl-6 p-0">
                    <div className="mb-6">
                        <iframe
                            src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;q=Top+Ten+Electronic+Shoppe&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                            className="mapwidth"
                            height={388}
                            frameBorder={0}
                            style={{ border: 0 }}
                            allowFullScreen
                        />
                    </div>
                </div>

                <div className="mb-5">
                    <h3 className="section-title mb-0 pb-2 font-size-25">
                        Our Stores
                    </h3>
                </div>
                <div className="row row-cols-1 row-cols-md-2 g-4 ">

                    {storeLocator.map((locator) => (
                        <div key={locator.branch} className="card mb-2 ml-3 locatorcard" >
                            <div className="card-body">
                                <address className="mb-6 mt-2 text-lh-23 col">
                                    <b>{locator.branch}</b><br />
                                    {locator.shop}
                                    <div>{locator.address}</div>
                                    <div>{locator.city}</div>
                                </address>
                            </div>
                        </div>
                    ))}

                </div>
            </div>


        </div>
    );
};

export default StoreLocator;
