import React, { useState } from "react";
import Head from "next/head";
import Link from "next/link";
import { getLocalStorage, setLocalStorage } from "../../services/helper";
import { forgotPassword } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import MetaTagHeader from "../../components/MetaTagHeader";




const ForgotPsw = () => {

    const [statusMsg, setStatusMsg] = useState("")
    const [errMsg, setErrMsg] = useState("")

    const setEmail = (payload) => {
        setLocalStorage("rstEmail", payload);
    }

    const handleRecover = async () => {
        try {
            const email = getLocalStorage("rstEmail");
            if (!email) return
            const res = await forgotPassword({ email })
            if (res == 0) {
                setErrMsg("Email does not exist")
                setTimeout(() => {
                    setErrMsg("")
                }, 2000);
                return;
            }
            setStatusMsg("A password reset email has been sent to the email address on file for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.")
        } catch (err) {
            getErrorMessage(err)
        }
    }

    return (
        <>
            <MetaTagHeader />
            <div style={{ maxWidth: "30rem", margin: "auto", marginTop: "2rem" }}>
                <header className="text-center mb-7">
                    <h2 className="h4 mb-0">Recover Password.</h2>
                    <p>
                        Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.
                    </p>
                </header>
                <div className="form-group">
                    <div className="js-form-message js-focus-state">
                        <label className="sr-only" htmlFor="recoverEmail">
                            Your email
                        </label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span
                                    className="input-group-text"
                                    id="recoverEmailLabel"
                                >
                                    <span className="fas fa-user" />
                                </span>
                            </div>
                            <input
                                type="email"
                                className="form-control"
                                name="email"
                                id="recoverEmail"
                                placeholder="Your email"
                                aria-label="Your email"
                                aria-describedby="recoverEmailLabel"
                                required
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success"
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                    </div>
                </div>
                <div className="mb-2">
                    <button
                        type="submit"
                        className="btn btn-block btn-sm btn-primary transition-3d-hover text-white"
                        onClick={handleRecover}
                    >
                        Recover Password
                    </button>
                </div>
                {
                    statusMsg.length > 0 ? (
                        <p className="text-green"> {statusMsg} </p>
                    ) : <></>
                }
                {
                    errMsg.length > 0 ? (
                        <p className="text-red"> {errMsg} </p>
                    ) : <></>
                }
                <div className="text-center mb-4">
                    <span className="small text-muted">
                        Remember your password?
                    </span>
                    <Link href="/login">
                        <a
                            className="js-animation-link small"
                            data-target="#login"
                            data-link-group="idForm"
                            data-animation-in="slideInUp"
                        >
                            Login
                        </a>
                    </Link>
                </div>
            </div>
        </>
    );
};

export default ForgotPsw;
