import React, { useEffect } from "react";
import Layout from "../components/Layout/Layout";
// stylesheets
import "../public/assets/css/font-open-sans.min.css";
import "../public/assets/vendor/font-awesome/css/fontawesome-all.min.css";
import "../public/assets/css/font-electro.min.css";
import "../public/assets/vendor/animate.css/animate.min.css";
import "../public/assets/vendor/hs-megamenu/src/hs.megamenu.min.css";
import "../public/assets/css/theme.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "react-inner-image-zoom/lib/InnerImageZoom/styles.min.css";
import AppState from "../context/AppState"
import { useRouter, Router } from 'next/router'
import { getLocalStorage, setLocalStorage } from "../services/helper"
import NProgress from "nprogress";
import "../public/assets/css/nprogress.css"

function MyApp({ Component, pageProps }) {
  if (!getLocalStorage("usrn")) setLocalStorage("usrn", null)
  if (!getLocalStorage("usrid")) setLocalStorage("usrid", null)
  if (!getLocalStorage("tkn")) setLocalStorage("tkn", null)
  if (!getLocalStorage("crt")) setLocalStorage("crt", [])
  if (!getLocalStorage("crtl")) setLocalStorage("crtl", [])
  if (!getLocalStorage("wstl")) setLocalStorage("wstl", [])
  if (!getLocalStorage("pymt")) setLocalStorage("pymt", {})
  if (!getLocalStorage("usrCmpr")) setLocalStorage("usrCmpr", [])
  if (!getLocalStorage("usrPinCode")) setLocalStorage("usrPinCode", null)
  if (!getLocalStorage("usrExChnge")) setLocalStorage("usrExChnge", [])
  if (!getLocalStorage("usrWrnty")) setLocalStorage("usrWrnty", [])
  if (!getLocalStorage("rcntP")) setLocalStorage("rcntP", [])

  const { query, isReady } = useRouter()

  const getCampaignData = () => {
    const { utm_source, utm_creative, utm_campaign, utm_content, utm_adgroup } = query;
    sessionStorage.setItem('campaignData', JSON.stringify({
      utm_source, utm_creative, utm_campaign, utm_content, utm_adgroup
    }));
  }

  useEffect(() => {
    getCampaignData();
  }, [isReady])

  Router.events.on("routeChangeStart", () => {
    NProgress.start();
  })
  Router.events.on("routeChangeComplete", () => {
    NProgress.done();
  })

  return (
    <>
      <AppState>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AppState>
    </>
  )
}
export default MyApp