
import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'
import Link from "next/link"
import { login } from "../../api/user"
import { getErrorMessage } from "../../services/errorHandle";
import SocialLogin from '../../components/SocialLogin'
import AppContext from "../../context/AppContext"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";

const Login = () => {
    const initialState = {
        email: "",
        password: ""
    };
    const router = useRouter()
    const [userForm, setUserForm] = useState(initialState);
    const [errorMsg, setErrorMsg] = useState(null);
    const { initializeUser } = useContext(AppContext);

    const handleLogin = async (e) => {
        e.preventDefault();
        login(userForm).then(
            res => {
                initializeUser(res)
                router.push("/")
            }).catch(err => {
                const { message } = getErrorMessage(err)
                setErrorMsg(message)
                setUserForm({ ...initialState })
            })
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserForm({ ...userForm, [name]: value });
    }

    const { email, password } = userForm;
    const breadcrumb = [
        {
            title: "Login",
        },
    ];
    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="my-4 mb-8">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 borders-radius-17 border p-4 px-lg-10">
                            <div className="border-bottom border-color-1 mb-6">
                                <h3 className="d-inline-block section-title mb-0 pb-2 font-size-26">
                                    Login
                                </h3>
                            </div>

                            <div className="text-center mb-4">
                                <SocialLogin />
                            </div>

                            <div className="text-center">
                                <span className="u-divider u-divider--xs u-divider--text mb-4">
                                    OR
                                </span>
                            </div>

                            <form onSubmit={handleLogin}>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="signinSrEmailExample3">
                                        Email Address
                                        <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="signinSrEmailExample3"
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        value={email}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="signinSrPasswordExample2">
                                        Password <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="signinSrPasswordExample2"
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        value={password}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="d-flex justify-content-end mb-4">
                                    <Link href="/forgot-password">
                                        <a className="small link-muted text-blue">
                                            Forgot Password?
                                        </a>
                                    </Link>
                                </div>
                                {
                                    errorMsg ? (
                                        <p className="text-red mb-4">
                                            {errorMsg}
                                        </p>
                                    ) : (<></>)
                                }
                                <div className="mb-1">
                                    <div className="mb-3">
                                        <button type="submit" className="btn btn-primary-dark-w px-5">
                                            Login
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div className="text-center mb-4">
                                <span className="small">
                                    Do not have an account?
                                </span>
                                <Link href="/signup">
                                    <a className="small text-blue pl-2">
                                        Signup
                                    </a>
                                </Link>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Login;