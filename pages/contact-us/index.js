import React, { useState } from "react";
import { messageToAdmin } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";
const breadcrumb = [
    {
        title: " Contact Us",
    },
];

const ContactUs = () => {
    const initialState = {
        firstName: null,
        lastName: null,
        email: null,
        subject: null,
        phoneNo: null,
        message: null
    };

    const [form, setForm] = useState(initialState);
    const [msg, setMsg] = useState(null);

    const handeleChange = (e) => {
        const { name, value } = e.target;
        let updatedForm = form;
        updatedForm[name] = value;
        setForm(updatedForm);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!form.firstName || !form.lastName || !form.email || !form.subject || !form.phoneNo || !form.message) {
            const msg = {
                status: "red",
                message: "Please fill all required fields."
            }
            setMsg(msg)
            setTimeout(() => {
                setMsg(null);
            }, 3000);
            return;
        }
        try {
            const payload = form;
            const res = await messageToAdmin(payload);
            if (res.code == 1) {
                const msg = {
                    status: "green",
                    message: "Your message is sent. We will get you back soon!"
                }
                setMsg(msg)
                setTimeout(() => {
                    setMsg(null);
                }, 3000);
            }
        } catch (err) {
            const msg = {
                status: "red",
                message: "Something went wrong. Please try again later!"
            }
            setMsg(msg)
            setTimeout(() => {
                setMsg(null);
            }, 3000);
            getErrorMessage(err);
        }
    }

    return (
        <div className="mt-5">
            <MetaTagHeader />
            <div className="container">
                <Breadcrumb data={breadcrumb} />

                <div className="mb-5">
                    <h1 className="text-center">Contact Us</h1>
                </div>
                <div className="row mb-10">
                    <div className="col-lg-7 col-xl-6 mb-8 mb-lg-0">
                        <div className="mr-xl-6">
                            <div className="border-bottom border-color-1 mb-5">
                                <h3 className="section-title mb-0 pb-2 font-size-25">
                                    Leave us a message
                                </h3>
                            </div>
                            <p className="max-width-830-xl text-gray-90">
                                We are here to answer all your queries please
                                fill in below details.
                            </p>
                            <form
                                className="js-validate"
                                noValidate="novalidate"
                            >
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                First name
                                                <span className="text-danger">
                                                    *
                                                </span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="firstName"
                                                aria-label
                                                required
                                                data-msg="Please enter your frist name."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                                autoComplete="off"
                                                onChange={(e) => handeleChange(e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                Last name
                                                <span className="text-danger">
                                                    *
                                                </span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="lastName"
                                                aria-label
                                                required
                                                data-msg="Please enter your last name."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                                onChange={(e) => handeleChange(e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                Email
                                                <span className="text-danger">
                                                    *
                                                </span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="email"
                                                aria-label
                                                required
                                                data-msg="Please enter your email."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                                onChange={(e) => handeleChange(e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                Phone No
                                                <span className="text-danger">
                                                    *
                                                </span>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="phoneNo"
                                                aria-label
                                                required
                                                data-msg="Please enter your phone."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                                onChange={(e) => handeleChange(e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                Subject
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                name="subject"
                                                aria-label
                                                data-msg="Please enter subject."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"
                                                onChange={(e) => handeleChange(e)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="js-form-message mb-4">
                                            <label className="form-label">
                                                Your Message
                                            </label>
                                            <div className="input-group">
                                                <textarea
                                                    name="message"
                                                    className="form-control p-5"
                                                    rows={4}
                                                    defaultValue={""}
                                                    onChange={(e) => handeleChange(e)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <button
                                        type="button"
                                        className="btn btn-primary-dark-w px-5"
                                        onClick={(e) => handleSubmit(e)}
                                    >
                                        Send Message
                                    </button>
                                </div>
                                {
                                    msg && (
                                        <p className={`text-${msg.status}`}> {msg.message} </p>
                                    )
                                }
                            </form>
                        </div>
                    </div>
                    <div className="col-lg-5 col-xl-6">
                        <div className="mb-6">
                            <iframe
                                src="https://maps.google.com/maps?width=100%&amp;height=600&amp;hl=en&amp;coord=19.066264150898512, 72.99894162671747&amp;q=Top+Ten+Electronic+Shoppe&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed"
                                width="100%"
                                height={288}
                                frameBorder={0}
                                style={{ border: 0 }}
                                allowFullScreen
                            />
                        </div>
                        <div className="border-bottom border-color-1 mb-5">
                            <h3 className="section-title mb-0 pb-2 font-size-25">
                                Our Address
                            </h3>
                        </div>
                        <address className="mb-6 text-lh-23">
                            Shop No – 7 & 8,  Real Tech Park,
                            <div>Plot No 39/2,Sector – 30A, Vashi, Navi Mumbai</div>
                            <div>Thane, Maharashtra– 400 703</div>
                            <div>Phone No: 022 – 62676886</div>
                            <div>
                                Email:{" "}
                                <a
                                    className="text-blue text-decoration-on"
                                    href="mailto:topten.customercare@gmail.com"
                                >
                                    topten.customercare@gmail.com
                                </a>
                            </div>
                        </address>
                        <h5 className="font-size-14 font-weight-bold mb-3">
                            Opening Hours
                        </h5>
                        <div>Monday to Sunday: 10:30 AM – 09:30 PM</div>
                        <h5 className="font-size-14 font-weight-bold mb-1 mt-4">
                            Careers
                        </h5>
                        <p className="text-gray-90">
                            If you’re interested in employment opportunities at
                            Top Ten Electronics, please email us:{" "}
                            <a
                                className="text-blue text-decoration-on"
                                href="mailto:info@toptenelectronics.in"
                            >
                                info@toptenelectronics.in
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactUs;
