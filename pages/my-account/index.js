
import React, { useState, useEffect, useContext } from "react";
import Link from "next/link"
import { useRouter } from 'next/router'
import { getUser, updateUser } from "../../api/user"
import { getLocalStorage, setLocalStorage } from "../../services/helper"
import { getErrorMessage } from "../../services/errorHandle";
import AppContext from "../../context/AppContext";
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";


const MyAccount = () => {

    const router = useRouter();
    let initialState = {
        name: "",
        email: "",
        contact_number: ""
    };
    const [errorMsg, setErrorMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const [userForm, setUserForm] = useState(initialState);
    const { setUsername, setCartCount, resetUser } = useContext(AppContext)
    const breadcrumb = [
        {
            title: "My Account",
        },
    ];
    useEffect(() => {
        getUser(getLocalStorage("usrid")).then(
            res => {
                setUserForm({
                    ...initialState,
                    name: res.name,
                    email: res.email,
                    contact_number: res.contact_number
                })
                setUsername(res.name)
            }).catch(
                err => getErrorMessage(err)
            )
    }, []);

    const handleUpdate = async (e) => {
        e.preventDefault();
        const payload = {
            id: getLocalStorage("usrid"),
            name,
            email,
            contact_number,
            role: "user"
        }

        updateUser(payload).then(
            res => {
                setUsername(res.name)
                setSuccessMsg("User updated succesfully.")
            }).catch(err => {
                const { message } = getErrorMessage(err)
                setErrorMsg(message)
            })
    }

    const handleLogout = async (e) => {
        resetUser()
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setUserForm({ ...userForm, [name]: value });
    }

    const handleChangePassword = (e) => {
        e.preventDefault();
        router.push("/change-password")
    }

    const { name, email, contact_number } = userForm;

    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="my-4">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 borders-radius-17 border p-4 px-lg-10">
                            <div className="border-bottom border-color-1 mb-6">
                                <h3 className="d-inline-block section-title mb-0 pb-2 font-size-26">
                                    My Account
                                </h3>
                            </div>

                            <form onSubmit={handleUpdate}>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrNameExample3">
                                        Full Name <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrNameExample3"
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        value={name}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrEmailExample3">
                                        Email address
                                        <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrEmailExample3"
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        value={email}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrContactExample2">
                                        Contact Number <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrContactExample2"
                                        type="number"
                                        className="form-control"
                                        name="contact_number"
                                        value={contact_number}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                {
                                    errorMsg ? (
                                        <p className="text-red mb-4">
                                            {errorMsg}
                                        </p>
                                    ) : (<></>)
                                }
                                {
                                    successMsg ? (
                                        <p className="text-green mb-4">
                                            {successMsg}
                                        </p>
                                    ) : (<></>)
                                }
                                <div className="d-flex">
                                    <div className="mb-6">
                                        <div className="mb-3">
                                            <button type="submit" className="btn btn-primary-dark-w px-5">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                    <div className="mb-6 ml-4">
                                        <div className="mb-3">
                                            <button onClick={handleChangePassword} className="btn btn-primary-dark-w mobilepadding px-5 ">
                                                Change Password
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                            <div className="text-center mb-4">
                                <span className="small">
                                    Do you want to back on shop?
                                </span>
                                <Link href="/">
                                    <a className="small text-blue pl-2">
                                        Click here.
                                    </a>
                                </Link>
                            </div>
                            <div className="mb-3">
                                <button type="button" className="btn btn-danger px-5" onClick={handleLogout}>
                                    <Link href="/">
                                        <a className="text-white">Logout</a>
                                    </Link>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default MyAccount;