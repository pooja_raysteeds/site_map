import React, { useEffect, useState } from "react";
import Link from "next/link";
import { getUserOrder, updateOrderStatus } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import { deliveredId, cancelledId, failedId } from "../../services/constant";
// import OrderInvoice from "../../components/OrderInvoice"
import { getLocalStorage, getS3Url } from "../../services/helper";
// import { PDFDownloadLink } from "@react-pdf/renderer";
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";

const Orders = () => {
  const [orders, setOrders] = useState([]);
  const userId = getLocalStorage("usrid");
  const breadcrumb = [
    {
      title: "Orders",
    },
  ];
  useEffect(() => {
    getOrders()
  }, []);

  const getOrders = () => {
    getUserOrder(userId).then(
      res => setOrders(res)
    ).catch(
      err => getErrorMessage(err)
    )
  }

  const hanldeOrderCancel = async (e, orderNumber) => {
    e.preventDefault();
    try {
      const payload = {
        order_number: orderNumber,
        order_status_id: 3
      }
      updateOrderStatus(payload).then(
        res => getOrders())
        .catch(err => getErrorMessage(err))

    } catch (err) {
      getErrorMessage(err)
    }
  }

  return (
    <>
      <MetaTagHeader />
      <div className="container">
        <Breadcrumb data={breadcrumb} />
        <div className="mb-4">
          <h1 className="text-center">My Orders</h1>
        </div>
        <div className="mb-10 cart-table largeviewprodetails">
          <form className="mb-4">
            <table className="table" cellSpacing={0}>
              <thead>
                <tr>
                  <th className="product-thumbnail w-lg-15">Order Number</th>
                  <th className="product-name">Product</th>
                  <th className="product-subtotal">Total</th>
                  <th className="product-thumbnail w-lg-15">Order Status</th>
                  <th className="product-thumbnail w-lg-15"></th>
                </tr>
              </thead>
              <tbody>
                {orders && (
                  orders.map(item => (
                    <tr key={item.id} className="border-bottom orderlist">
                      <td className="d-none d-md-table-cell">
                        <span><a href="/">{item.order_number}</a></span>
                      </td>
                      <td data-title="Product"  >
                        {
                          item.products.map((p) => (
                            <div key={p.id}>
                              <Link href={`/shop/${p.product.slug_url}`}>
                                <a className="text-gray-90">
                                  <img
                                    rel="preload"
                                    className="img-fluid max-width-100 p-1 mr-2 border border-color-1 orderimage"
                                    src={getS3Url(p.product.galleries[0].image_url)}
                                    alt="Image Description"
                                  />
                                  {p.product.title} <strong> x {p.quantity} </strong>
                                  {
                                    p.product.exchangeProduct && (
                                      <div className="d-flex flex-row mt-2">
                                        <span className="text-red">Exchange Product : </span>
                                        <p className="pl-2">{`${p.product.exchangeProduct.item} -Rs${p.product.exchangeProduct.amount}`}</p>
                                      </div>
                                    )
                                  }
                                  {
                                    p.product.warranty && (
                                      <div className="d-flex flex-row mt-2">
                                        <span className="text-green">Extended Warranty : </span>
                                        <p className="pl-2 text-green">{`${p.product.warranty.warranty} years`}</p>
                                      </div>
                                    )
                                  }
                                </a>
                              </Link>
                            </div>
                          ))
                        }
                      </td>
                      <td data-title="Total">
                        <span className>
                          {item.total_amount}
                        </span>
                      </td>
                      <td data-title="Order Status">
                        {item.order_status.status}
                      </td>
                      <td data-title="Action">
                        <div className="mb-3">
                          {((item.order_status_id === cancelledId) || (item.order_status_id === failedId)) ? (
                            <></>
                          ) : (
                            <>
                              {
                                item.order_status_id === deliveredId ? (
                                  <span>Download Invoice</span>
                                  // <PDFDownloadLink
                                  //   document={<OrderInvoice data={item} />}
                                  //   fileName={`${item.order_number}.pdf`}
                                  // >
                                  //   {({ loading }) =>
                                  //     loading ? "Loading document..." : "Download Invoice"
                                  //   }
                                  // </PDFDownloadLink>
                                ) : (
                                  <button
                                    onClick={(e) => hanldeOrderCancel(e, item.order_number)}
                                    className="btn btn-primary-dark-w"
                                  >
                                    Cancel Order
                                  </button>
                                )}
                            </>
                          )}
                        </div>
                      </td>
                    </tr>
                  ))
                )}
              </tbody>
            </table>
          </form>
        </div>
        {/* MobileView */}
        <div className="mb-2">
          {orders && (
            orders.map(item => (

              <div className="wrapper mobileviewprodetails">
                <div className="row">
                  <div className="col-lg-6 col-md-8 col-sm-10 offset-lg-0 offset-md-2 offset-sm-1 pt-lg-0 pt-3">
                    <div id="cart" className="bg-white rounded">
                      <div className="border p-2">
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="h6"><strong>Order No:{item.order_number}</strong></div>
                          {((item.order_status_id === cancelledId) || (item.order_status_id === failedId)) ? (
                            <></>
                          ) : (<button
                            onClick={(e) => hanldeOrderCancel(e, item.order_number)}
                            className="btn btn-primary-dark-w  "
                          >
                            Cancel
                          </button>)}
                        </div>
                        {
                          item.products.map((p) => (<div className="d-flex jusitfy-content-between align-items-center pt-3 pb-2 border-bottom">
                            <div className="item pr-2">
                              <img
                                rel="preload"
                                className="img-fluid max-width-100 p-1 mr-2 border border-color-1"
                                src={getS3Url(p.product.galleries[0].image_url)}
                                alt="Image Description"
                              />
                            </div>
                            <div className="d-flex flex-column px-3"> <b className="h6">
                              {p.product.title} <strong> x {p.quantity} </strong></b>

                            </div>
                          </div>
                          ))}

                        <div className="d-flex align-items-center py-2">
                          <div className="display-5">{item.order_status.status}</div>
                          <div className="ml-auto d-flex">
                            <div className="text-primary text-uppercase px-3">Total</div>
                            <div className="font-weight-bold"> ₹{item.total_amount}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            ))
          )}
        </div>
      </div>
      {/* End mobile View  */}
    </>
  );
};

export default Orders;
