import React, { useState, useContext } from "react";
import Head from "next/head";
import Link from "next/link";
import AppContext from "../../context/AppContext";
import { changePassword } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import { getLocalStorage } from "../../services/helper";
import MetaTagHeader from "../../components/MetaTagHeader";


const ChangePsw = () => {
    const [currentPsw, setCurrentPsw] = useState("");
    const [newPsw, setNewPsw] = useState("");
    const [successMsg, setSuccessMsg] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [isValid, setIsValid] = useState(false);
    const { validatePassword, pswValidate } = useContext(AppContext);

    const handleNewPsw = (password) => {
        const flag = validatePassword(password);
        setIsValid(flag);
        setNewPsw(password);
    };

    const handleConfirm = async () => {
        if (!isValid) {
            setErrorMsg("Please enter a valid password.");
            setTimeout(() => {
                setErrorMsg(null);
            }, 2000);
            return
        }
        const userId = getLocalStorage("usrid")
        try {
            const payload = {
                id: userId,
                newPassword: newPsw,
                currentPassword: currentPsw
            }
            const res = await changePassword(payload)
            if (res.statusCode === 1) {
                setSuccessMsg(res.message)
            }
            if (res.statusCode === 0) {
                setErrorMsg(res.message)
            }
            setTimeout(() => {
                setErrorMsg(null)
            }, 1000);
        } catch (error) {
            getErrorMessage(error)
        }
    };

    return (
        <>
            <MetaTagHeader />
            <div
                style={{ maxWidth: "30rem", margin: "auto", marginTop: "2rem" }}
            >
                <header className="text-center mb-7">
                    <h2 className="h4 mb-0">Change Password</h2>
                </header>
                <div className="form-group">
                    <div className="js-form-message js-focus-state">
                        <label className="sr-only" htmlFor="recoverEmail">
                            Current Password
                        </label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span
                                    className="input-group-text"
                                    id="recoverEmailLabel"
                                >
                                    <span className="fas fa-user" />
                                </span>
                            </div>
                            <input
                                type="password"
                                className="form-control"
                                name="email"
                                id="recoverEmail"
                                placeholder="Current password"
                                aria-label="Current password"
                                aria-describedby="recoverEmailLabel"
                                required
                                data-msg="Please enter a password."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success"
                                onChange={(e) => setCurrentPsw(e.target.value)}
                            />
                        </div>
                        <label className="sr-only" htmlFor="recoverEmail">
                            New Password
                        </label>
                        <div className="input-group mt-3">
                            <div className="input-group-prepend">
                                <span
                                    className="input-group-text"
                                    id="recoverEmailLabel"
                                >
                                    <span className="fas fa-user" />
                                </span>
                            </div>
                            <input
                                type="password"
                                className="form-control"
                                name="email"
                                id="recoverEmail"
                                placeholder="New password"
                                aria-label="New password"
                                aria-describedby="recoverEmailLabel"
                                required
                                data-msg="Please enter a password."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success"
                                onChange={(e) => handleNewPsw(e.target.value)}
                            />
                        </div>
                    </div>
                </div>
                {
                    successMsg && (
                        <p className="text-green"> {successMsg} </p>
                    )
                }
                {
                    errorMsg && (
                        <p className="text-red"> {errorMsg} </p>
                    )
                }
                <div className="mb-2">
                    <button
                        type="submit"
                        className="btn btn-block btn-sm btn-primary transition-3d-hover text-white"
                        onClick={handleConfirm}
                    >
                        Confirm
                    </button>
                </div>
                <div className="row flex-column pl-4">
                    {pswValidate.map((item) => (
                        <span className={`${item.style} pb-2`}>
                            {" "}
                            {item.value}{" "}
                        </span>
                    ))}
                </div>
                <div className="text-center mb-4">
                    <span className="small text-muted">
                        Remember your password?
                    </span>
                    <Link href="/login">
                        <a className="js-animation-link small">
                            Login
                        </a>
                    </Link>
                </div>
            </div>
        </>
    );
};

export default ChangePsw;
