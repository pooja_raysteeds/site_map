import React, { useEffect, useState, useContext } from "react";
import Link from "next/link";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import { RatingView } from 'react-simple-star-rating'
import { getProductsByIds } from "../../api/products";
import { getErrorMessage } from "../../services/errorHandle";
import { getS3Url, calcAverageRating } from "../../services/helper";
import AppContext from "../../context/AppContext";
import { getLocalStorage } from "../../services/helper";
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";

const Compare = () => {
    const [compareProduct, setCompareProduct] = useState([]);
    const { removeFromCompare, addProductToCart } = useContext(AppContext);
    const cartValue = getLocalStorage("crt");
    const ids = getLocalStorage("usrCmpr");
    const breadcrumb = [
        {
            title: "Compare",
        },
    ];
    useEffect(() => {
        getProducts();
    }, []);

    const getProducts = async () => {
        try {
            const res = await getProductsByIds(ids);
            setCompareProduct(res);
        } catch (err) {
            getErrorMessage(err);
        }
    };

    const handleCart = (product_id) => {
        const params = { product_id, quantity: 1 };
        addProductToCart(params);
    };

    const handleRemove = (id) => {
        removeFromCompare(id);
        getProducts();
    };

    return (
        <>
            <MetaTagHeader />
            {compareProduct.length > 0 ? (
                <>
                    <Breadcrumb data={breadcrumb} />
                    <div className="container">
                        <div className="table-responsive table-bordered table-compare-list mb-10 border-0">
                            <table className="table">
                                <tbody>
                                    <tr>
                                        <th className="min-width-200">
                                            Product
                                        </th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>
                                                    <Link
                                                        href={`/shop/${item.slug_url}`}
                                                        key={item.id}
                                                    >
                                                        <a className="product d-block">
                                                            <div className="product-compare-image">
                                                                <div className="d-flex mb-3">
                                                                    <img
                                                                        rel="preload"
                                                                        style={{
                                                                            width: "10rem",
                                                                            height: "10rem",
                                                                        }}
                                                                        className="img-fluid mx-auto"
                                                                        src={getS3Url(item.galleries[0].image_url)}
                                                                        alt="Image Description"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <h3 className="product-item__title text-blue font-weight-bold mb-3">
                                                                {item.title}
                                                            </h3>
                                                        </a>
                                                    </Link>
                                                    <div className="text-warning mb-2">
                                                        <RatingView fillColor="#ffc107" ratingValue={calcAverageRating(item.reviews)} />
                                                    </div>
                                                </td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Price</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>
                                                    <div className="product-price">
                                                        ₹{item.selling_price}
                                                    </div>
                                                </td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Availability</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>
                                                    <span>
                                                        {item.stock_quantity}{" "}In Stock
                                                    </span>
                                                </td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Description</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>
                                                    <ReactMarkdown
                                                        children={
                                                            item.highlights
                                                        }
                                                        rehypePlugins={[
                                                            rehypeRaw,
                                                        ]}
                                                    />
                                                </td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Sku</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>{item.sku}</td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Weight</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>{item.weight}</td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Dimensions</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <div key={index}>
                                                    {item.specifications
                                                        .filter((person) => person.key === "Product Dimensions (WxHxD)")
                                                        .map((subitem, index) => (
                                                            <td key={index}>
                                                                {subitem.value}
                                                            </td>
                                                        ))}
                                                </div>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>brands</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>{item.brand.name}</td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>color</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <div key={index}>
                                                    {item.specifications.filter((person) => person.key === "Color")
                                                        .map((subitem, index) => (
                                                            <td key={index}>
                                                                {subitem.value}
                                                            </td>
                                                        ))}
                                                </div>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Remove</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index} className="text-center">
                                                    <a
                                                        onClick={() =>
                                                            handleRemove(
                                                                item.id
                                                            )
                                                        }
                                                        className="text-gray-90"
                                                    >
                                                        <i className="fa fa-times" />
                                                    </a>
                                                </td>
                                            ))}
                                    </tr>
                                    <tr>
                                        <th>Add to cart</th>
                                        {compareProduct &&
                                            compareProduct.map((item, index) => (
                                                <td key={index}>
                                                    {cartValue &&
                                                        cartValue.filter((c) => c.product_id === item.id).length > 0 ? (
                                                        <div className>
                                                            <Link href="/cart">
                                                                <a className="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5">
                                                                    Go to cart
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    ) : (
                                                        <div className>
                                                            <Link href="/cart">
                                                                <a
                                                                    onClick={() =>
                                                                        handleCart(
                                                                            item.id
                                                                        )
                                                                    }
                                                                    className="btn btn-soft-secondary mb-3 mb-md-0 font-weight-normal px-5 px-md-3 px-xl-5"
                                                                >
                                                                    Add to cart
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    )}
                                                </td>
                                            ))}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </>
            ) : (
                <>
                    <h3 className="d-flex justify-content-center empty-cart ecart-mb0 pt-5">
                        Nothing to compare
                    </h3>
                    <div className="mb-2 pb-0dot5 d-flex justify-content-center empty-cart pt-5">
                        <Link href="/">
                            <a className="btn btn-block btn-primary-dark continue-shopping">
                                Continue Shopping
                            </a>
                        </Link>
                    </div>
                </>
            )}
        </>
    );
};

export default Compare;
