import React, { useState } from "react";
import Head from "next/head";
import Link from "next/link";
import { getLocalStorage, setLocalStorage } from "../../services/helper";
import { resetPassword } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import MetaTagHeader from "../../components/MetaTagHeader";

const ResetPsw = () => {

    const [newPsw, setNewPsw] = useState(null);
    const [cnfmPsw, setCnfmPsw] = useState(null);
    const [successMsg, setSuccessMsg] = useState("");
    const [errorMsg, setErrorMsg] = useState("")

    const handleConfirm = async () => {
        const email = getLocalStorage('rstEmail');
        if (!email) return;
        const payload = {
            email,
            newPassword: cnfmPsw
        }
        try {
            const res = await resetPassword(payload);
            if(res.statusCode) {
                setSuccessMsg("Password has been updated")
            }
        } catch(err) {
            setErrorMsg("Something went wrong! Please try again later.")
            getErrorMessage(err);
        }
    }

    return (
        <>
            <MetaTagHeader />
            <div style={{ maxWidth: "30rem", margin: "auto", marginTop: "2rem" }}>
                <header className="text-center mb-7">
                    <h2 className="h4 mb-0">Reset Your Password.</h2>
                </header>
                <div className="form-group">
                    <div className="js-form-message js-focus-state">
                        <label className="sr-only" htmlFor="recoverEmail">
                            Enter New Password
                        </label>
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span
                                    className="input-group-text"
                                    id="recoverEmailLabel"
                                >
                                    <span className="fas fa-user" />
                                </span>
                            </div>
                            <input
                                type="password"
                                className="form-control"
                                name="email"
                                id="recoverEmail"
                                placeholder="Enter new password"
                                aria-label="Enter new password"
                                aria-describedby="recoverEmailLabel"
                                required
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success"
                                onChange={(e) => setNewPsw(e.target.value)}
                            />
                        </div>
                        <label className="sr-only" htmlFor="recoverEmail">
                            Confirm New Password
                        </label>
                        <div className="input-group mt-3">
                            <div className="input-group-prepend">
                                <span
                                    className="input-group-text"
                                    id="recoverEmailLabel"
                                >
                                    <span className="fas fa-user" />
                                </span>
                            </div>
                            <input
                                type="password"
                                className="form-control"
                                name="email"
                                id="recoverEmail"
                                placeholder="Confirm new password"
                                aria-label="Confirm new password"
                                aria-describedby="recoverEmailLabel"
                                required
                                data-msg="Please enter a valid email address."
                                data-error-class="u-has-error"
                                data-success-class="u-has-success"
                                onChange={(e) => setCnfmPsw(e.target.value)}
                            />
                        </div>
                    </div>
                </div>
                {
                    newPsw && cnfmPsw && (
                        <>
                            {
                                newPsw !== cnfmPsw ? <p className="text-red"> Confirm password is not matched </p> : <></>
                            }
                        </>
                    )
                }
                {
                    successMsg.length > 0 && (
                        <p className="text-green"> {successMsg} </p>
                    )
                }
                {
                    errorMsg.length > 0 && (
                        <p className="text-red"> {errMsg} </p>
                    )
                }
                <div className="mb-2">
                    <button
                        type="submit"
                        className="btn btn-block btn-sm btn-primary transition-3d-hover text-white"
                        onClick = {handleConfirm}
                    >
                        Confirm
                    </button>
                </div>
            </div>
        </>
    );
};

export default ResetPsw;
