
import React, { useState, useContext } from "react"
import { useRouter } from 'next/router'
import Link from "next/link"
import { signUp } from "../../api/user"
import { getErrorMessage } from "../../services/errorHandle";
import AppContext from "../../context/AppContext"
import Breadcrumb from "../../components/Breadcrumb";
import MetaTagHeader from "../../components/MetaTagHeader";
const breadcrumb = [
    {
        title: "Signup",
    },
];


const Signup = () => {
    const initialState = {
        name: "",
        email: "",
        password: "",
        confirm_password: "",
        contact_number: ""
    };
    const router = useRouter()
    const [errorMsg, setErrorMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const [userForm, setUserForm] = useState(initialState);
    const [isPswValid, setIsPswValid] = useState(false)
    const { validatePassword, initializeUser, pswValidate } = useContext(AppContext);


    const handleSignup = async (e) => {
        e.preventDefault();
        const payload = {
            name,
            email,
            password,
            contact_number,
            role: "user"
        }
        if (!isPswValid) return;
        signUp(payload).then(
            res => {
                initializeUser(res);
                setSuccessMsg("You have registered succesfully.")
                setUserForm({ ...initialState })
                router.push("/")
            }).catch(err => {
                const { message } = getErrorMessage(err)
                setErrorMsg(message)
            })
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name === "password") {
            const flag = validatePassword(value);
            setIsPswValid(flag)
        }
        setUserForm({ ...userForm, [name]: value });
    }

    const { name, email, password, confirm_password, contact_number } = userForm;
    return (
        <>
            <MetaTagHeader />
            <Breadcrumb data={breadcrumb} />
            <div className="container">
                <div className="my-4">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 borders-radius-17 border p-4 px-lg-10">
                            <div className="border-bottom border-color-1 mb-6">
                                <h3 className="d-inline-block section-title mb-0 pb-2 font-size-26">
                                    Register
                                </h3>
                            </div>

                            {/* <div className="text-center mb-4">
                                <SocialLogin />
                            </div>

                            <div className="text-center">
                                <span className="u-divider u-divider--xs u-divider--text mb-4">
                                    OR
                                </span>
                            </div> */}

                            <form onSubmit={handleSignup}>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrNameExample3">
                                        Full Name <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrNameExample3"
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        value={name}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrEmailExample3">
                                        Email address
                                        <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrEmailExample3"
                                        type="email"
                                        className="form-control"
                                        name="email"
                                        value={email}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrContactExample2">
                                        Contact Number <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrContactExample2"
                                        type="text"
                                        className="form-control"
                                        name="contact_number"
                                        value={contact_number}
                                        onChange={handleChange}
                                        required
                                        maxLength="10"
                                    />
                                </div>
                                <div className="form-group">
                                    <label className="form-label" htmlFor="RegisterSrPasswordExample2">
                                        Password <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrPasswordExample2"
                                        type="password"
                                        className="form-control"
                                        name="password"
                                        value={password}
                                        onChange={handleChange}
                                        aria-label="Password"
                                        required
                                    />
                                    <div className="row flex-column pl-4">
                                        {
                                            pswValidate.map((item) => (
                                                <span className={`${item.style} pb-2`}> {item.value} </span>
                                            ))
                                        }
                                    </div>
                                </div>
                                <div className="js-form-message form-group">
                                    <label className="form-label" htmlFor="RegisterSrConfirmPasswordExample2">
                                        Confirm Password <span className="text-danger">*</span>
                                    </label>
                                    <input
                                        id="RegisterSrConfirmPasswordExample2"
                                        type="password"
                                        className="form-control"
                                        name="confirm_password"
                                        value={confirm_password}
                                        onChange={handleChange}
                                        required
                                    />
                                </div>
                                {
                                    password && confirm_password && (password !== confirm_password) ? (
                                        <p className="text-red mb-2">
                                            Opps! Password does not matched. Please try again.
                                        </p>
                                    ) : (<></>)
                                }
                                <p className="text-gray-90 mb-4">
                                    Your personal data will be used to support your experience
                                    throughout this website, to manage your account, and for other
                                    purposes described in our{" "}
                                    <Link href="/privacy-policy">
                                        <a className="text-blue">
                                            privacy policy.
                                        </a>
                                    </Link>
                                </p>
                                {
                                    errorMsg ? (
                                        <p className="text-red mb-4">
                                            {errorMsg}
                                        </p>
                                    ) : (<></>)
                                }
                                {
                                    successMsg ? (
                                        <p className="text-green mb-4">
                                            {successMsg}
                                        </p>
                                    ) : (<></>)
                                }
                                <div className="mb-6">
                                    <div className="mb-3">
                                        <button type="submit" className="btn btn-primary-dark-w px-5">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </form>

                            <div className="text-center mb-4">
                                <span className="small">
                                    Already have an account?
                                </span>
                                <Link href="/login">
                                    <a className="small text-blue pl-2">
                                        Login
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Signup;