import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <Html lang="en">
                <Head>
                    <meta property="custom" content="ecom" />
                    <link rel="shortcut icon" href="../../favicon.ico" />
                    <noscript>
                        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossOrigin="anonymous" />
                    </noscript>

                    {/* Facebook Domain Verification */}
                    <meta name="facebook-domain-verification" content="90xi6lnu5tsk50duwrlmq8g61wkgyc" />

                    {/* Google Site Verification */}
                    <meta name="google-site-verification" content="humziis7nBn1XmImYh058y-JwJDipoMPbZt6DgHypFw" />

                    {/* Google Tag Manager */}
                    <script dangerouslySetInnerHTML={{
                        __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer','GTM-5F4CX88');`}}>
                    </script>
                </Head>

                <body className="custom_class">
                    {/* Google Tag Manager (noscript) */}
                    <noscript dangerouslySetInnerHTML={{
                        __html: `<iframe  rel="dns-prefetch" src="https://www.googletagmanager.com/ns.html?id=GTM-5F4CX88"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe>`}}>
                    </noscript>
                    <Main />
                    <NextScript />
                    <a className="js-go-to u-go-to" href="#"
                        data-position='{"bottom": 15, "right": 15 }'
                        data-type="fixed"
                        data-offset-top="400"
                        data-compensation="#header"
                        data-show-effect="slideInUp"
                        data-hide-effect="slideOutDown">
                        <span className="fas fa-arrow-up u-go-to__inner"></span>
                    </a>
                </body>
            </Html>
        )
    }
}

export default MyDocument