
import React from "react";
import Breadcrumb from "../../components/Breadcrumb";

const breadcrumb = [
    {
        title: "Sitemap",
    },
];

const Sitemap = () => {
    return (
        <div className="mt-5">
            <div className="container mb-3">
                <Breadcrumb data={breadcrumb} />
                <div className="mb-5">

                    <h1 className="text-left" style={{ color: 'orange' }}>Sitemap</h1>
                    <div class="container">


                        <table class="table table-bordered">

                            <tbody>

                                <td><h2 style={{ color: "orange" }}>Bedroom Furniture </h2>
                                    <div class="row1" >
                                        <ul>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>


                                        </ul>
                                    </div>
                                </td>
                                <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                    <div class="row2">
                                        <ul>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>


                                        </ul>
                                    </div>
                                </td>
                                <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                    <div class="row3">
                                        <ul>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>


                                        </ul>
                                    </div>
                                </td>


                                <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                    <div class="row4">
                                        <ul>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>

                                        </ul>
                                    </div>
                                </td>

                                <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                    <div class="row5">
                                        <ul>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                            <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>

                                        </ul>
                                    </div>
                                </td>


                                <tr>
                                    <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                        <div class="row6">
                                            <ul>
                                                <li><a href="#">Beds in banglore</a></li>
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">Terms and Conditions </a></li>
                                                <li><a href="#">Terms of Use</a></li>
                                                <li><a href="#">Returns and Refunds</a></li>
                                                <li><a href="#">More</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                    <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                        <div class="row" style={{ color: "blue" }}>
                                            <ul>
                                                <li><a href="#" >Beds in banglore</a></li>
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">Terms and Conditions </a></li>
                                                <li><a href="#">Terms of Use</a></li>
                                                <li><a href="#">Returns and Refunds</a></li>
                                                <li><a href="#">More</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                    <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                        <div class="row8">
                                            <ul>
                                                <li><a href="#">Beds in banglore</a></li>
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#">Terms and Conditions </a></li>
                                                <li><a href="#">Terms of Use</a></li>
                                                <li><a href="#">Returns and Refunds</a></li>
                                                <li><a href="#">More</a></li>

                                            </ul>
                                        </div>
                                    </td>
                                    <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                        <div class="row">
                                            <ul>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}> About Us</a></li>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                                <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>

                                            </ul>
                                        </div>
                                    </td>

                                    <div>
                                        <td><h2 style={{ color: "orange" }}>Bedroom Furniture</h2>
                                            <div class="row " style={{
                                                color: "black",
                                                width: 300,
                                                height: 110,
                                                overflow: "scroll"
                                            }}>
                                                <ul>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Beds in banglore</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>About Us</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms and Conditions </a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Terms of Use</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>Returns and Refunds</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }}><a href="#" style={{ color: 'black' }}>More</a></li>
                                                    <li style={{ color: 'red' }} ><a href="#" style={{ color: 'black' }}>new</a></li>
                                                    <li style={{ color: 'red' }} ><a href="#" style={{ color: 'black' }}>new</a></li>


                                                </ul>
                                            </div>
                                        </td>
                                    </div>

                                </tr>
                            </tbody>
                        </table>
                    </div>



                </div>

            </div>
        </div>




    );
};
export default Sitemap;
