import React, { useEffect, useState } from "react"
import Link from 'next/link'
import { getErrorMessage } from "../../services/errorHandle"
import { getDeals } from "../../api/promo"
import { getS3Url } from "../../services/helper"

const Deals = () => {
    const [deals, setDeals] = useState([])

    useEffect(() => {
        getDeals().then(
            res => setDeals(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    if (deals.length === 0) return null;
    return (
        <div className="container mt-5">
            <div className="row mb-6 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                {
                    deals.map(deal => (
                        <div className="col-md-3 mb-4 mb-xl-0 col-xl-3 col-wd-3 flex-shrink-0 flex-xl-shrink-1" key={deal.id}>
                            <Link href={deal.slug_url}>
                                <a className="min-height-146 py-1 py-xl-2 py-wd-1 banner-bg d-flex align-items-center text-gray-90">
                                    <div className="col-6 col-xl-7 col-wd-6 pr-0">
                                        <img rel="preload" className="img-fluid" src={getS3Url(deal.banner_url)} alt="Image Description" />
                                    </div>
                                    <div className="col-6 col-xl-5 col-wd-6 pr-xl-4 pr-wd-3">
                                        <div className="mb-2 pb-1 font-size-18 font-weight-light text-ls-n1 text-lh-23">
                                            {deal.title}
                                        </div>
                                        <div className="link text-gray-90 font-weight-bold font-size-15" href={deal.slug_url}>
                                            Shop Now
                                            <span className="link__icon ml-1">
                                                <span className="link__icon-inner"><i className="ec ec-arrow-right-categproes"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </Link>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Deals
