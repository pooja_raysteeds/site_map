import React, { useEffect, useState, useRef } from 'react';
import Link from 'next/link'
import Slider from "react-slick"
import { getBrands } from "../../api/products"
import { subscribeUser } from "../../api/user"
import { getCategoryHierarchy } from "../../api/category";
import { getS3Url } from "../../services/helper"
import WhatsappWidget from '../../components/WhatsappWidget'
import { getErrorMessage } from "../../services/errorHandle";



const Footer = () => {
    const [brands, setBrands] = useState([]);
    const [categories, setCategories] = useState([])
    const customSlider = useRef(null)
    const [email, setEmail] = useState(null);
    const [msg, setMsg] = useState(null);

    useEffect(() => {
        getBrands().then(
            res => setBrands(res)
        ).catch(
            err => getErrorMessage(err)
        )
        getCategoryHierarchy(0).then(
            res => setCategories(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, [])

    const settings = {
        arrow: true,
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        className: 'u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1',
        responsive: [{
            "breakpoint": 1400,
            "settings": {
                "slidesToShow": 4
            }
        }, {
            "breakpoint": 1200,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 992,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 768,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 554,
            "settings": {
                "slidesToShow": 2
            }
        }],
        dotsClass: "slick-dots slick-thumb text-center",
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (email) {
            try {
                const payload = {
                    email
                };
                const res = await subscribeUser(payload);
                setMsg("You signed up successfully");
                setTimeout(() => {
                    setMsg(null);
                }, 3000);
            } catch (err) {
                getErrorMessage(err);
            }
        }
    }

    const renderArrows = () => {
        return (
            <div className="fr position-absolute font-size-34 u-slick__arrow-normal" style={{ top: "-2.3rem" }}>
                <div
                    className="js-prev arrowbg position-absolute mr-4 top-0 font-size-34 u-slick__arrow-normal top-8 fa fa-angle-left right-1 slick-arrow"
                    onClick={() => customSlider.current.slickPrev()}
                >
                </div>
                <div
                    className="js-next arrowbg position-absolute top-0 font-size-34 u-slick__arrow-normal top-8 fa fa-angle-right right-0 slick-arrow"
                    onClick={() => customSlider.current.slickNext()}
                >
                </div>
            </div>
        );
    };

    return (
        <>
            <div className="bg-primary py-3">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-7 mb-md-3 mb-lg-0">
                            <div className="row align-items-center">
                                <div className="col-auto flex-horizontal-center">
                                    <i className="ec ec-newsletter font-size-40 text-white"></i>
                                    <h2 className="font-size-20 mb-0 ml-3 text-white">Sign up to Newsletter</h2>
                                </div>
                                <div className="col my-4 my-md-0">
                                    {/* <h5 className="font-size-15 ml-4 mb-0 text-white">...and receive <strong>₹20 coupon for first shopping.</strong></h5> */}
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5">
                            <form className="js-validate js-form-message">
                                <label className="sr-only" htmlFor="subscribeSrEmail">Email Address</label>
                                <div className="input-group input-group-pill">
                                    <input onChange={(e) => setEmail(e.target.value)} type="email" className="form-control border-0 height-40" name="email" id="subscribeSrEmail" placeholder="Email address" aria-label="Email address" aria-describedby="subscribeButton" required
                                        data-msg="Please enter a valid email address." />
                                    <div className="input-group-append">
                                        <button onClick={handleSubmit} type="button" className="btn btn-dark btn-sm-wide height-40 py-2" id="subscribeButton">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                            {
                                msg && (
                                    <span className="text-green">{msg}</span>
                                )
                            }

                        </div>
                    </div>
                </div>
            </div>
            <div className="pt-8 pb-4 bg-gray-13">
                <div className="container mt-1">
                    <div className="row">
                        <div className="col-lg-5">
                            <div className="mb-6">
                                <a href="#" className="d-inline-block">
                                    <img rel="preload" className="img-fluid logo-pic" src="../../assets/img/75X75/ten-image.png" alt="Logo" />

                                </a>
                            </div>
                            <div className="mb-4">
                                <div className="row no-gutters">
                                    <div className="col-auto">
                                        <i className="ec ec-support text-primary font-size-56"></i>
                                    </div>
                                    <div className="col pl-3">
                                        <div className="font-size-13 font-weight-light">Got questions? Call us </div>
                                        <a href="tel:+022-62676886 " className="font-size-20 text-gray-90">022-62676886 </a> <br />
                                        <a href="#" className="font-size-15 text-gray-90">Email: topten.customercare@gmail.com</a>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-4">
                                <h6 className="mb-1 font-weight-bold">Address</h6>
                                <address>
                                    Shop No. 7 & 8, Real Tech Park, Plot No 39/2, Sector 30A, Vashi, Navi Mumbai,
                                    Thane, Maharashtra – 400703
                                </address>
                            </div>
                            <div className="my-4 my-md-4">
                                <ul className="list-inline mb-0 opacity-7">
                                    <li className="list-inline-item mr-0">

                                        <a target="_blank" className="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="https://www.facebook.com/toptenelectronicsshoppe">
                                            <span className="fab fa-facebook-f btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li className="list-inline-item mr-0">
                                        <a target="_blank" className="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="https://www.instagram.com/topten_electronics/">
                                            <span className="fab fa-instagram btn-icon__inner"></span>
                                        </a>
                                    </li>
                                    <li className="list-inline-item mr-0">
                                        <a target="_blank" className="btn font-size-20 btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="https://www.linkedin.com/company/top-ten-electronics-shoppe/">
                                            <span className="fab fa-linkedin btn-icon__inner"></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="row">
                                <div className="col-12 col-md mb-8 mb-md-0">
                                    <h6 className="mb-3 font-weight-bold">Find it Fast</h6>
                                    <ul className="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        {
                                            categories && (
                                                categories.map((item) => (
                                                    <li key={item.id}>
                                                        <Link href={`/shop?parent=${item.slug_url}`}>
                                                            <a className="list-group-item list-group-item-action">{item.title}</a>
                                                        </Link>
                                                    </li>
                                                ))
                                            )
                                        }
                                    </ul>
                                </div>

                                <div className="col-12 col-md mb-4 mb-md-0">
                                    <h6 className="mb-3 font-weight-bold">About Us & Policies</h6>

                                    <ul className="list-group list-group-flush list-group-borderless mb-0 list-group-transparent">
                                        <li>
                                            <Link href="/about">
                                                <a className="list-group-item list-group-item-action">About Us</a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="/privacy-policy">
                                                <a className="list-group-item list-group-item-action">Privacy Policy</a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="/terms-and-conditions">
                                                <a className="list-group-item list-group-item-action">Terms & Conditions</a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="/terms-of-use">
                                                <a className="list-group-item list-group-item-action">Terms of Use</a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="/return-and-refunds">
                                                <a className="list-group-item list-group-item-action"> Return & Refunds</a>
                                            </Link>
                                        </li>
                                        <li>
                                            <Link href="/sitemap">
                                                <a className="list-group-item list-group-item-action"> Sitemap</a>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-gray-14 py-2">
                <div className="container">
                    <div className="flex-center-between d-block d-md-flex">
                        <div className="text-md-right mb-2">
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img1.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img2.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img3.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img4.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img5.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img6.jpg" alt="Image Description" />
                            </span>
                            <span className="d-inline-block bg-white border rounded p-1">
                                <img rel="preload" className="max-width-5" src="../../assets/img/cards/img7.jpg" alt="Image Description" />
                            </span>
                        </div>
                        <div className="mb-3 mb-md-0">© 2021 <a href="/" className="font-weight-bold text-gray-90">
                            Top Ten Electronics</a> - All Rights Reserved
                        </div>
                    </div>
                </div>
            </div>

            <WhatsappWidget />
        </>
    )
}

export default Footer
