import React, { useState } from "react";
import { addUserAddress } from "../../api/user";
import { getLocalStorage } from "../../services/helper";
import { getErrorMessage } from "../../services/errorHandle";

const AddressForm = (props) => {
    const billingAddress = {
        billing_first_name: "",
        billing_last_name: "",
        billing_company_name: "",
        billing_country: "India",
        billing_street_address: "",
        billing_apt: "",
        billing_city: "",
        billing_postal_code: 0,
        billing_state: "",
        billing_email: "",
        billing_contact_number: 0,
        billing_company_gstin: ""
    };

    const shippingAddress = {
        shipping_first_name: "",
        shipping_last_name: "",
        shipping_company_name: "",
        shipping_country: "India",
        shipping_street_address: "",
        shipping_apt: "",
        shipping_city: "",
        shipping_postal_code: 0,
        shipping_state: "",
        shipping_email: "",
        shipping_contact_number: 0,
        shipping_company_gstin: ""
    };

    const initialState = {
        ...billingAddress,
        ...shippingAddress,
    };

    const [addressForm, setAddressForm] = useState(initialState);
    const [checkedShipping, setCheckedShipping] = useState(false);
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);
    const userId = getLocalStorage("usrid");

    const handleCheckbox = (e) => {
        setCheckedShipping(!checkedShipping);
        const {
            billing_first_name,
            billing_last_name,
            billing_company_name,
            billing_country,
            billing_street_address,
            billing_apt,
            billing_city,
            billing_postal_code,
            billing_state,
            billing_email,
            billing_contact_number,
        } = orderForm;

        if (checkedShipping) {
            const obj = {
                shipping_first_name: billing_first_name,
                shipping_last_name: billing_last_name,
                shipping_company_name: billing_company_name,
                shipping_country: billing_country,
                shipping_street_address: billing_street_address,
                shipping_apt: billing_apt,
                shipping_city: billing_city,
                shipping_postal_code: billing_postal_code,
                shipping_state: billing_state,
                shipping_email: billing_email,
                shipping_contact_number: billing_contact_number,
            };
            const updatedValues = Object.assign({}, orderForm, obj);
            setOrderForm({ ...updatedValues });
        } else {
            const updatedValues = Object.assign({}, orderForm, {
                ...shippingAddress,
            });
            setOrderForm({ ...updatedValues });
        }
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        let shippingObj;
        if (name.includes("billing_")) {
            shippingObj = name.replace("billing", "shipping");
        }
        if (!checkedShipping && shippingObj) {
            setAddressForm({
                ...addressForm,
                [name]: value,
                [shippingObj]: value,
            });
        } else {
            setAddressForm({ ...addressForm, [name]: value });
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (
            (!addressForm.billing_first_name,
            !addressForm.billing_last_name,
            !addressForm.billing_street_address,
            !addressForm.billing_postal_code,
            !addressForm.billing_city,
            !addressForm.billing_state,
            !addressForm.billing_email,
            !addressForm.billing_contact_number,
            !addressForm.billing_apt,
            !addressForm.shipping_first_name,
            !addressForm.shipping_last_name,
            !addressForm.shipping_street_address,
            !addressForm.shipping_postal_code,
            !addressForm.shipping_city,
            !addressForm.shipping_state,
            !addressForm.shipping_email,
            !addressForm.shipping_contact_number,
            !addressForm.shipping_apt)
        ) {
            setError(true);
            setSuccess(false);
        }
        try {
            const payload = {
                user_id: userId,
                ...addressForm,
            };
            const res = await addUserAddress(payload);
            setError(false);
            setSuccess(true);
            props.setLoading(res);
            setTimeout(() => {
                setSuccess(false);
            }, 3000);
        } catch (error) {
            getErrorMessage(error.message);
        }
    };

    const {
        billing_first_name,
        billing_last_name,
        billing_company_name,
        billing_country,
        billing_street_address,
        billing_apt,
        billing_city,
        billing_postal_code,
        billing_state,
        billing_email,
        billing_contact_number,
        shipping_first_name,
        shipping_last_name,
        shipping_company_name,
        shipping_country,
        shipping_street_address,
        shipping_apt,
        shipping_city,
        shipping_postal_code,
        shipping_state,
        shipping_email,
        shipping_contact_number,
        customer_note,
        billing_company_gstin,
        shipping_company_gstin
    } = addressForm;

    return (
        <form className="js-validate" noValidate="novalidate">
            <div className="mb-3">
                <div className="border-bottom border-color-1 mb-5">
                    <h3 className="section-title mb-0 pb-2 font-size-25">
                        Billing Details
                    </h3>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                First name
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_first_name"
                                value={billing_first_name}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Last name
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_last_name"
                                value={billing_last_name}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="w-100" />
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Company name (optional)
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_company_name"
                                value={billing_company_name}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Company GSTIN (optional)
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_company_gstin"
                                value={billing_company_gstin}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Country
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_country"
                                value={billing_country}
                                onChange={handleChange}
                                required
                                disabled
                            />
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Street address
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_street_address"
                                value={billing_street_address}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                <span className="text-danger">*</span>
                                Apt, suite, etc.
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_apt"
                                value={billing_apt}
                                onChange={handleChange}
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                City
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_city"
                                value={billing_city}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Postcode/Zip
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_postal_code"
                                value={billing_postal_code}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="w-100" />
                    <div className="col-md-12">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                State
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_state"
                                value={billing_state}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Email address
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="email"
                                className="form-control"
                                name="billing_email"
                                value={billing_email}
                                onChange={handleChange}
                                required
                            />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="js-form-message mb-6">
                            <label className="form-label">
                                Phone
                                <span className="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                className="form-control"
                                name="billing_contact_number"
                                value={billing_contact_number}
                                onChange={handleChange}
                                required
                                maxLength="10"
                            />
                        </div>
                    </div>
                    <div className="w-100" />
                </div>

                <div className="border-bottom border-color-1 mb-5">
                    <h3 className="section-title mb-0 pb-2 font-size-25">
                        Shipping Details
                    </h3>
                </div>
                <div id="shopCartAccordion3" className="accordion rounded mb-5">
                    <div className="card border-0">
                        <div
                            id="shopCartHeadingFour"
                            className="custom-control custom-checkbox d-flex align-items-center"
                        >
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="shippingdiffrentAddress"
                                name="shippingdiffrentAddress"
                            />
                            <label
                                className="custom-control-label form-label"
                                htmlFor="shippingdiffrentAddress"
                                data-toggle="collapse"
                                data-target="#shopCartfour"
                                aria-expanded="false"
                                aria-controls="shopCartfour"
                            >
                                Ship to a different address?
                            </label>
                        </div>
                        <div
                            id="shopCartfour"
                            className="mt-5 collapse"
                            aria-labelledby="shopCartHeadingFour"
                            data-parent="#shopCartAccordion3"
                            style={{}}
                        >
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            First name
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_first_name"
                                            value={shipping_first_name}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Last name
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_last_name"
                                            value={shipping_last_name}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="w-100" />
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Company name (optional)
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_company_name"
                                            value={shipping_company_name}
                                            onChange={handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Company GSTIN (optional)
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_company_gstin"
                                            value={shipping_company_gstin}
                                            onChange={handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Country
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_country"
                                            value={shipping_country}
                                            onChange={handleChange}
                                            required
                                            disabled
                                        />
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Street address
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_street_address"
                                            value={shipping_street_address}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            <span className="text-danger">
                                                *
                                            </span>
                                            Apt, suite, etc.
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_apt"
                                            value={shipping_apt}
                                            onChange={handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            City
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_city"
                                            value={shipping_city}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Postcode/Zip
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_postal_code"
                                            value={shipping_postal_code}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="w-100" />
                                <div className="col-md-12">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            State
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_state"
                                            value={shipping_state}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Email address
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            name="shipping_email"
                                            value={shipping_email}
                                            onChange={handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="js-form-message mb-6">
                                        <label className="form-label">
                                            Phone
                                            <span className="text-danger">
                                                *
                                            </span>
                                        </label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            name="shipping_contact_number"
                                            value={shipping_contact_number}
                                            onChange={handleChange}
                                            required
                                            maxLength="10"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="mt-2">
                            <div className="ml-3">
                                <button
                                    type="button"
                                    onClick={handleSubmit}
                                    className="btn btn-primary-dark-w px-5"
                                >
                                    Save Address
                                </button>
                            </div>
                        </div>
                        <div className="w-100" />
                        {error && (
                            <p className="ml-4 mt-2 text-red">
                                Please fill required fields.
                            </p>
                        )}
                        {success && (
                            <p className="ml-4 mt-2 text-green">
                                Address added successfully.
                            </p>
                        )}
                    </div>
                </div>
            </div>
        </form>
    );
};

export default AddressForm;
