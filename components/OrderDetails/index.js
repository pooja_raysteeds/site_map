import React, { useState } from 'react';
import Link from "next/link";

const OrderDetails = ({ cart, payment, paymentMethod }) => {
    const [checkIndex, setCheckIndex] = useState(0)
    const paymentMode = [
        {
            key: "cc avenue",
            value: "CC Avenue",
            label: "Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account."
        },
        {
            key: "cash on delivery",
            value: "Cash on Delivery",
            label: "Pay with cash upon delivery."
        }
    ]

    const onPaymentMode = (e, i) => {
        const { value } = e.target
        paymentMethod(value)
        setCheckIndex(i)
    }

    return (
        <>
            <div className="border-bottom border-color-1 mb-5">
                <h3 className="section-title mb-0 pb-2 font-size-25">
                    Your Order
                </h3>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th className="product-name">Product</th>
                        <th className="product-total">Total</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        cart && cart.map((p, index) => (
                            <tr className="cart_item" key={index}>
                                <td>
                                    {p.product.title}
                                    <strong className="product-quantity">
                                        {` × ${p.quantity}`}
                                    </strong>
                                </td>
                                <td>₹{p.product.selling_price}</td>
                            </tr>
                        ))
                    }
                </tbody>
                <tfoot>
                    <tr>
                        <th>Subtotal</th>
                        <td>₹{payment && payment.subTotal}</td>
                    </tr>
                    <tr>
                        <th className="text-green">Saved Amount</th>
                        <td className="text-green">{`₹${payment && payment.savedAmount}`}</td>
                    </tr>
                    <tr>
                        <th>Shippment Charge</th>
                        <td>{`Free ₹${payment && payment.shippingCharge}`}</td>
                    </tr>
                    {
                        payment && (
                            <tr>
                                <th className="text-green">Product Warranty</th>
                                {payment.warrantyAmount ? <td className="text-green"> + ₹{payment.warrantyAmount}</td> : <td>-</td>}
                            </tr>
                        )
                    }
                    {
                        payment && (
                            <tr>
                                <th className="text-red">Exchange Product Amount</th>
                                {payment.exchangeAmount ? <td className="text-red"> - ₹{payment.exchangeAmount}</td> : <td>-</td>}
                            </tr>
                        )
                    }
                    <tr>
                        <th>Total Amount</th>
                        <td><strong>₹{payment && payment.total}</strong></td>
                    </tr>
                </tfoot>
            </table>
            <div className="border-top border-width-3 border-color-1 pt-3 mb-3">
                <div id="basicsAccordion1">
                    {
                        paymentMode.map((p, index) => (
                            <div key={index} className="border-bottom border-color-1 border-dotted-bottom">
                                <div className="p-3" id="basicsHeadingOne">
                                    <div className="custom-control custom-radio">
                                        <input
                                            type="radio"
                                            className="custom-control-input"
                                            id={p.key}
                                            name={p.key}
                                            value={p.key}
                                            onChange={(e) => onPaymentMode(e, index)}
                                            checked={checkIndex === index}
                                        />
                                        <label
                                            className={`custom-control-label form-label ${checkIndex === index ? '' : 'collapsed'}`}
                                            htmlFor={p.key}
                                            data-toggle="collapse"
                                            data-target={`#${p.key}`}
                                            aria-expanded={checkIndex === index}
                                            aria-controls={p.key}
                                        >
                                            {p.value}
                                        </label>
                                    </div>
                                </div>
                                <div
                                    id={p.key}
                                    className={`collapse ${checkIndex === index ? 'show' : ''} border-top border-color-1 border-dotted-top bg-dark-lighter`}
                                    aria-labelledby="basicsHeadingOne"
                                    data-parent="#basicsAccordion1"
                                >
                                    <div className="p-4">
                                        {p.label}
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
            <div className="form-group d-flex align-items-center justify-content-between px-3 mb-5">
                <div className="form-check">
                    <input
                        className="form-check-input"
                        type="checkbox"
                        defaultValue
                        id="defaultCheck10"
                        required
                        data-msg="Please agree terms and conditions."
                        data-error-class="u-has-error"
                        data-success-class="u-has-success"
                        defaultChecked
                    />
                    <label
                        className="form-check-label form-label"
                        htmlFor="defaultCheck10"
                    >
                        I have read and agree to the website{" "}
                        <Link href="/terms-and-conditions">
                            <a className="text-blue">
                                terms and conditions{" "}
                            </a>
                        </Link>
                        <span className="text-danger">*</span>
                    </label>
                </div>
            </div>
        </>
    );
}

export default OrderDetails;