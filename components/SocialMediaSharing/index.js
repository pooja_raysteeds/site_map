import React from "react";
import { SHARED_BASE_URL } from "../../services/constant"
import {
    FacebookShareButton,
    FacebookIcon,
    WhatsappShareButton,
    WhatsappIcon,
    LinkedinShareButton,
    LinkedinIcon,
    TwitterShareButton,
    TwitterIcon,
    TelegramShareButton,
    TelegramIcon
} from "react-share";

const SocialSharing = ({ slug_url }) => {
    const sharedUrl = `${SHARED_BASE_URL}/shop/${slug_url}`;
    const quote = "Checkout this awesome product";
    const hashtag = "#TopTenElectronics"
    const title = "Hey! Look what i found in TopTen Electronics -> ";

    return (
        <div className="d-flex justify-content-start width-11 mt-4 mb-4">
            <FacebookShareButton
                url={sharedUrl}
                quote={quote}
                hashtag={hashtag}
            >
                <FacebookIcon size={32} round />
            </FacebookShareButton>

            <WhatsappShareButton
                url={sharedUrl}
                title={title}
                className="pl-2"
            >
                <WhatsappIcon size={32} round />
            </WhatsappShareButton>
            <LinkedinShareButton
                url={sharedUrl}
                title={title}
                hashtag={hashtag}
                className="pl-2"
            >
                <LinkedinIcon size={32} round />
            </LinkedinShareButton>
            <TwitterShareButton
                url={sharedUrl}
                title={title}
                hashtag={hashtag}
                className="pl-2"
            >
                <TwitterIcon size={32} round />
            </TwitterShareButton>
            <TelegramShareButton
                url={sharedUrl}
                title={title}
                className="pl-2"
            >
                <TelegramIcon size={32} round />
            </TelegramShareButton>
        </div>
    );
};

export default SocialSharing;
