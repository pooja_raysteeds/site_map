import React, { useContext, useState, useEffect } from "react";
import { RatingView } from "react-simple-star-rating";
import Link from "next/link";
import {
    calcAverageRating,
    calcPercentage,
    getS3Url,
    getLocalStorage,
    setLocalStorage,
} from "../../services/helper";
import AppContext from "../../context/AppContext";
import Slider from "react-slick";
import ReactMarkdown from "react-markdown";
import rehypeRaw from "rehype-raw";
import "./productinfo.module.css";
import SocialSharing from "../../components/SocialMediaSharing";
import InnerImageZoom from "react-inner-image-zoom";
import { useRouter } from "next/router";
import { getPinCodes } from "../../api/checkout";
import { getErrorMessage } from "../../services/errorHandle";
import { notifyMe } from "../../api/user"
import { getExchangedProducts, getProductsWarranty } from "../../api/products"
import Styles from "./productinfo.module.css";

const ProductInfo = ({ data }) => {
    if (data && Object.keys(data).length === 0) return null;

    useEffect(() => {
        initializePinCodes();
        getOldProductsExchange();
        initializeWarrantyProducts();
    }, []);

    const getOldProductsExchange = async () => {
        try {
            const res = await getExchangedProducts();
            setOldProducts(res);
        } catch (error) {
            getErrorMessage(error);
        }
    }

    const initializeWarrantyProducts = () => {
        const params = {
            category_id: data.category_id,
            product_price: data.selling_price
        }
        getProductsWarranty({ params }).then(
            res => setWarrantyProducts(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }

    const sortedGallaries = data.galleries;
    sortedGallaries.sort((a, b) => {
        var keyA = a.sequence_number,
            keyB = b.sequence_number;
        // Compare the 2 dates
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
    });

    const router = useRouter();
    const {
        addProductToCart,
        addProductToWishlist,
        removeProductToWishlist,
        addToCompare,
        setUserCurrentPinCode,
    } = useContext(AppContext);
    const userId = getLocalStorage("usrid");
    let userCompares = getLocalStorage("usrCmpr");
    const usrExChnge = getLocalStorage("usrExChnge");
    const [currentPinCode, setCurrentPinCode] = useState(null);
    const [pinCodes, setPinCodes] = useState(null);
    const [show, setShow] = useState({ status: "red", message: "", valid: 0 });
    const [notifyEmail, setNotifyEmail] = useState("");
    const [msg, setMsg] = useState({ status: "red", msg: null });
    const [oldProducts, setOldProducts] = useState(null);
    const [exchangeFlag, setExchangeFlag] = useState(false);
    const [exchangeProduct, setExchangeProduct] = useState(null);
    const [currentWarrantyProduct, setCurrentWarrantyProduct] = useState(null);
    const [currentOption, setCurrentOption] = useState("");
    const [warrantyProducts, setWarrantyProducts] = useState([]);
    const userWarranty = getLocalStorage("usrWrnty");
    const userExchange = getLocalStorage("usrExChnge");

    const settings = {
        customPaging: function (i) {
            return (
                <div className={Styles.productimgwidth}>
                    <a>
                        <div className={Styles.productthubimg}>
                            <img
                                rel="preload"
                                className="img-fluid"
                                src={getS3Url(data.galleries[i].image_url)}
                            />
                        </div>
                    </a>
                </div>
            );
        },
        dots: true,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    const handleBuyNow = (data) => {
        let subTotal = 0;
        let mrpAmount = 0;
        let savedAmount = 0;
        let shippingCharge = 0;
        let warrantyAmount = 0;
        let exchangeAmount = 0;
        let total = 0;
        const params = { product_id: data.id, quantity: 1 };
        let cartList = [];
        cartList.push({ ...params, product: data });
        setLocalStorage("crtl", cartList);
        cartList.map((item) => {
            subTotal += item.product.selling_price * item.quantity;
            mrpAmount += item.product.mrp * item.quantity;
        });
        savedAmount = mrpAmount - subTotal;
        // Check for warranty amount
        userWarranty.forEach(w => {
            cartList.forEach(c => {
                if (w.productId == c.product_id) {
                    warrantyAmount += parseInt(w.amount);
                }
            })
        });
        // Check for exchange amount
        userExchange.forEach(e => {
            cartList.forEach(c => {
                if (e.productId == c.product_id) {
                    exchangeAmount += parseInt(e.amount);
                }
            })
        })
        total = (subTotal + shippingCharge + warrantyAmount) - exchangeAmount;
        setLocalStorage("pymt", {
            subTotal,
            savedAmount,
            shippingCharge,
            total,
            exchangeAmount,
            warrantyAmount
        });
        addProductToCart(params);
    };

    const handleCart = (product_id, quantity) => {
        const params = { product_id, quantity };
        addProductToCart(params);
    };

    const handleWishlist = (e, product_id) => {
        e.preventDefault();
        addProductToWishlist(product_id);
    };

    const removeWishlist = (e, product_id) => {
        e.preventDefault();
        removeProductToWishlist(product_id);
    };

    const handleCompare = (id) => {
        addToCompare(id);
        router.push("/compare");
    };

    const initializePinCodes = async () => {
        try {
            const res = await getPinCodes();
            setPinCodes(res);
            const pinCode = getLocalStorage("usrPinCode");
            setCurrentPinCode(pinCode);
            if (pinCode && res) {
                const flag = res.find((item) => item.pincode == pinCode);
                if (flag) {
                    const status = "green";
                    const message = "Delivery is available";
                    const valid = 1;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                } else {
                    const status = "red";
                    const message = "Delivery is not available here";
                    const valid = 0;
                    setShow((prevState) => ({
                        ...prevState,
                        status,
                        message,
                        valid,
                    }));
                }
            }
        } catch (error) {
            getErrorMessage(error);
        }
    };

    const handlePinCode = async (current) => {
        setCurrentPinCode(current);
        if (pinCodes && currentPinCode) {
            const flag = pinCodes.find(
                (item) => item.pincode == current
            );
            if (flag) {
                const status = "green";
                const message = "Delivery is available";
                const valid = 1;
                setShow((prevState) => ({
                    ...prevState,
                    status,
                    message,
                    valid,
                }));
            } else {
                const status = "red";
                const message = "Delivery is not available here";
                const valid = 0;
                setShow((prevState) => ({
                    ...prevState,
                    status,
                    message,
                    valid,
                }));
            }
            setLocalStorage("usrPinCode", current);
        }
    };

    const handleNotifyMe = async () => {
        if (notifyEmail) {
            const payload = {
                email: notifyEmail,
                isSent: false,
                productId: data.id
            }
            const res = await notifyMe(payload);
            if (res) {
                const status = "green";
                const msg = "We’ll notify you when this product is back in stock";
                setMsg({ ...msg, status, msg });
            } else {
                const status = "red";
                const msg = "Something went wrong!";
                setMsg({ ...msg, status, msg });
            }
            setTimeout(() => {
                const msg = null;
                setMsg({ ...msg, msg })
                setNotifyEmail("")
            }, 4000);
        }
    }

    const handleExchangeChange = async (e) => {
        const { value } = e.target;
        const type = e.target.selectedOptions[0].getAttribute('type');
        const payload = {
            item: type,
            amount: value,
            productId: data.id
        }
        setExchangeProduct(payload);
        let userExChnge = getLocalStorage("usrExChnge");
        const updatedExChnge = userExChnge.filter((item) => { return item.productId != data.id })
        updatedExChnge.push(payload);
        setLocalStorage("usrExChnge", updatedExChnge);
    }

    const handleBuyWithExchange = (flag) => {
        if (flag == 1) {
            setExchangeFlag(true);
        }
        else {
            setLocalStorage("usrExChnge", [])
            setExchangeFlag(false);
            setExchangeProduct(null);
        };
    }

    const removeExchangeProduct = () => {
        setLocalStorage("usrExChnge", [])
        setExchangeFlag(false);
        setExchangeProduct(null);
    }

    const handleWarrantyChange = (e) => {
        const warranty = e.target.selectedOptions[0].getAttribute('warranty');
        const amount = e.target.selectedOptions[0].getAttribute('amount');
        const payload = {
            warranty,
            amount,
            productId: data.id
        }
        if (warranty && amount) {
            let usrWrnty = getLocalStorage("usrWrnty");
            const updatedWrnty = usrWrnty.filter((item) => { return item.productId != data.id });
            updatedWrnty.push(payload);
            setLocalStorage("usrWrnty", updatedWrnty);
            setCurrentWarrantyProduct(payload);
        }
    }

    const removeCurrentWrntyPrdct = () => {
        setCurrentWarrantyProduct(null);
        setLocalStorage("usrWrnty", []);
    }

    return (
        <div className="row rowProduct">
            <div className="col-md-6 col-lg-4 col-xl-5 mb-18 mb-md-0">
                {data.stock_quantity && data.stock_quantity ? (
                    <Slider
                        id="sliderSyncingNav"
                        className="js-slick-carousel u-slick mb-2 arrows"
                        data-nav-for="#sliderSyncingThumb"
                        {...settings}
                    >
                        {data.galleries.map((g) => (
                            <div key={g.id}>
                                <InnerImageZoom
                                    src={getS3Url(g.image_url)}
                                    zoomSrc={getS3Url(g.image_url)}
                                />
                            </div>
                        ))}
                    </Slider>
                ) : (
                    <Slider
                        id="sliderSyncingNav"
                        className="js-slick-carousel u-slick mb-2 arrows"
                        data-nav-for="#sliderSyncingThumb"
                        {...settings}
                    >
                        {data.galleries.map((g) => (
                            <div key={g.id} className="outofstock">
                                <img
                                    rel="preload"
                                    src={getS3Url(g.image_url)}
                                    alt={data.parent.title}
                                    className="ousimage"
                                    style={{ width: "100%" }}
                                    title={data.parent.title}
                                />
                                <div className="middle">
                                    <div className="text">Out of stock</div>
                                </div>
                            </div>
                        ))}
                    </Slider>
                )}
            </div>
            <div className="col-md-6 col-lg-4 col-xl-4 mb-md-6 mb-lg-0">
                <div className="mb-2">
                    <Link href={`/shop?parent=${data.parent.slug_url}`}>
                        <a className="font-size-12 text-gray-5 mb-2 d-inline-block">
                            {data.parent.title}
                        </a>
                    </Link>
                    <h1 className="font-size-25 text-lh-1dot2">{data.title}</h1>
                    <div className="mb-2 d-inline-flex align-items-center">
                        <div className="text-warning mr-2">
                            <RatingView
                                fillColor="#ffc107"
                                ratingValue={calcAverageRating(data.reviews)}
                            />
                        </div>
                        <span className="text-secondary font-size-13">
                            {`(${data.reviews.length} customer reviews)`}
                        </span>
                    </div>

                    <div className="mb-2 font-size-14 pl-3 ml-1 text-gray-110">
                        <p className="mb-2">
                            <strong>SKU</strong>: {data.sku}
                        </p>
                        <p className="mb-2">
                            <strong>Brand</strong>:
                            <Link href={`/shop?brand=["${data.brand.slug_url}"]`}>
                                <a className="font-size-12 text-gray-8 mb-2 pl-2">
                                    <span className="text-red font-size-15">
                                        {data.brand.name}
                                    </span>
                                </a>
                            </Link>
                        </p>
                        <p className="mb-4">
                            <strong>Category</strong>:
                            <Link href={`/shop?category=${data.category.slug_url}`}>
                                <a className="font-size-12 text-gray-8 mb-2 pl-2">
                                    <span className="text-red font-size-15">
                                        {data.category.title}
                                    </span>
                                </a>
                            </Link>
                        </p>

                        <ReactMarkdown
                            children={data.highlights}
                            rehypePlugins={[rehypeRaw]}
                        />
                    </div>

                    {/* Old Product Exchange Card */}
                    <div className="card py-3 px-2 border-width-2 border-color-1 borders-radius-17 mb-5 mt-6" style={{ maxWidth: "24rem" }}>
                        <div className="d-flex align-items-center border-color-1 border-bottom mb-2">
                            <input type="radio" id="Without" name="fav_language" value={0} onChange={(e) => handleBuyWithExchange(e.target.value)} defaultChecked />
                            <label className="ml-2 mt-2" htmlFor="Without">Buy Without Exchange</label><br />
                        </div>
                        <div className="d-flex align-items-center">
                            <input type="radio" id="With" name="fav_language" value={1} onChange={(e) => handleBuyWithExchange(e.target.value)} />
                            <label className="ml-2 mt-2" htmlFor="With">Buy With Exchange</label><br />
                        </div>

                        {
                            oldProducts && exchangeFlag && (
                                <select className={`mx-3 mt-2 mb-21 text-gray-9 ${Styles.selectMain}`} name={currentOption} onChange={(e) => handleExchangeChange(e)}>
                                    <option selected disabled>Select</option>
                                    {
                                        oldProducts.map((item, index) => (
                                            <option className="p-5" key={index} value={item.amount} type={item.segment}>
                                                {`${item.segment} - ₹${item.amount}`}
                                            </option>
                                        ))
                                    }
                                </select>
                            )
                        }
                    </div>

                    {/* Social Media Sharing Icons */}
                    <SocialSharing slug_url={data.slug_url} />
                </div>

            </div>

            <div className="mx-md-auto mx-lg-0 col-md-6 col-lg-4 col-xl-3">
                <div className="mb-2">
                    <div className="card p-5 border-width-2 border-color-1 borders-radius-17">
                        <div className="text-gray-9 font-size-14 pb-2 border-color-1 border-bottom mb-5">
                            Availability:{" "}
                            <span className="text-green font-weight-bold">
                                {data.stock_quantity} in stock
                            </span>
                        </div>
                        <div className="mb-3 d-flex">
                            <div className="font-size-34 text-red">{`₹${data.selling_price}`}</div>
                            <del className="font-size-26 mt-2 pl-2">
                                {`₹${data.mrp}`}
                            </del>
                        </div>
                        <div className="mb-3">
                            <span className="text-green font-size-18 font-weight-bold">
                                {`${calcPercentage(
                                    data.mrp,
                                    data.selling_price
                                )}% off`}
                            </span>
                            <span className="text-gray-9 font-size-16 ml-2">
                                {`(₹${(data.mrp - data.selling_price).toFixed(2)})`}
                            </span>
                        </div>
                        {data.stock_quantity === 0 ? (
                            <>
                                <input
                                    type="email"
                                    placeholder="Your email"
                                    className="form-control mb-2"
                                    value={notifyEmail}
                                    onChange={(e) => setNotifyEmail(e.target.value)}
                                />
                                <a className="btn btn-block btn-primary-dark mb-2 text-white" onClick={handleNotifyMe}>
                                    Notify Me
                                </a>
                                {
                                    msg.msg && (
                                        <p className={`text-${msg.status}`}>{msg.msg}</p>
                                    )
                                }
                            </>
                        ) : (
                            <>
                                <div className="mb-2 pb-0dot5">
                                    {getLocalStorage("crt").filter((c) => c.product_id === data.id).length > 0 ? (
                                        <Link href="/cart">
                                            <a className="btn btn-block btn-primary-dark">
                                                <i className="ec ec-add-to-cart mr-2 font-size-20" />
                                                Go to Cart
                                            </a>
                                        </Link>
                                    ) : (
                                        <Link href="/cart">
                                            <a
                                                onClick={(e) =>
                                                    handleCart(data.id, 1)
                                                }
                                                className="btn btn-block btn-primary-dark"
                                            >
                                                <i className="ec ec-add-to-cart mr-2 font-size-20" />
                                                Add to Cart
                                            </a>
                                        </Link>
                                    )}
                                </div>
                                <div className="mb-3">
                                    {userId ? (
                                        <Link href="/checkout">
                                            <a
                                                onClick={() =>
                                                    handleBuyNow(data)
                                                }
                                                className="btn btn-block btn-dark"
                                            >
                                                Buy Now
                                            </a>
                                        </Link>
                                    ) : (
                                        <Link href="/login">
                                            <a className="btn btn-block btn-dark">
                                                Buy Now
                                            </a>
                                        </Link>
                                    )}
                                </div>
                            </>
                        )}

                        <div className="flex-content-center flex-wrap">
                            {userId ? (
                                <Link href="/wishlist">
                                    {getLocalStorage("wstl").filter(
                                        (w) => w.product_id === data.id
                                    ).length > 0 ? (
                                        <a
                                            onClick={(e) =>
                                                removeWishlist(e, data.id)
                                            }
                                            className="text-gray-6 font-size-13 mr-2"
                                        >
                                            <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                            Remove from Wishlist
                                        </a>
                                    ) : (
                                        <a
                                            onClick={(e) =>
                                                handleWishlist(e, data.id)
                                            }
                                            className="text-gray-6 font-size-13 mr-2"
                                        >
                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                            Wishlist
                                        </a>
                                    )}
                                </Link>
                            ) : (
                                <Link href="/login">
                                    <a className="text-gray-6 font-size-13 mr-2">
                                        <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                        Wishlist
                                    </a>
                                </Link>
                            )}
                            {userCompares &&
                                (userCompares.filter((item) => item == data.id)
                                    .length > 0 ? (
                                    <Link href="/compare">
                                        <a
                                            onClick={() =>
                                                handleCompare(data.id)
                                            }
                                            className="text-gray-6 font-size-13"
                                        >
                                            <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                            Go to Compare
                                        </a>
                                    </Link>
                                ) : (
                                    <Link href="/compare">
                                        <a
                                            onClick={() =>
                                                handleCompare(data.id)
                                            }
                                            className="text-gray-6 font-size-13"
                                        >
                                            <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                            Compare
                                        </a>
                                    </Link>
                                ))}
                            {
                                exchangeFlag && exchangeProduct && (
                                    <div className="d-flex flex-row align-items-center px-2 py-1 card border-width-2 border-color-1 borders-radius-17 mt-2">
                                        <span className="text-red">{exchangeProduct.item}</span>
                                        <span className="text-grey pl-4" onClick={removeExchangeProduct} style={{ cursor: "pointer" }}>X</span>
                                    </div>
                                )
                            }
                            {
                                currentWarrantyProduct && (
                                    <div className="d-flex flex-row align-items-center px-2 py-1 card border-width-2 border-color-1 borders-radius-17 mt-2">
                                        <span className="text-green">Ext. Warranty: {currentWarrantyProduct.warranty} years</span>
                                        <span className="text-grey pl-4" onClick={removeCurrentWrntyPrdct} style={{ cursor: "pointer" }}>X</span>
                                    </div>
                                )
                            }
                        </div>
                    </div>
                </div>

                {/* Check for Product Delivery */}
                <div className="card py-3 px-2 border-width-2 border-color-1 borders-radius-17 mt-4">
                    <h6 className="text-gray-9">Check for Delivery</h6>
                    <div className="d-flex flex-row justify-content-between align-items-center">
                        <input
                            maxLength={6}
                            placeholder="Enter Your Pincode"
                            className="form-control"
                            value={currentPinCode}
                            onChange={(e) => handlePinCode(e.target.value)}
                        />
                    </div>
                    {
                        show.message && (
                            <p className={`mt-2 text-${show.status}`}>
                                {show.message}
                            </p>
                        )
                    }
                </div>

                {/* Extend Product Warranty */}
                {
                    warrantyProducts.length > 0 && (
                        <div className="card py-3 px-2 border-width-2 border-color-1 borders-radius-17 mt-4">
                            <h6 className="text-gray-9">Extend Product Warranty</h6>
                            <select value={currentWarrantyProduct ? currentWarrantyProduct.item : 'default'} className={`mt-2 text-gray-9 ${Styles.selectMain}`} onChange={(e) => handleWarrantyChange(e)}>
                                <option value="default" selected disabled>Select</option>
                                {
                                    warrantyProducts && (
                                        warrantyProducts.map((item, index) => (
                                            <option key={index} warranty={item.warranty} amount={item.amount}>
                                                {`${item.warranty} ${item.warranty === "1" ? 'Year' : 'Years'} - ₹${item.amount}`}
                                            </option>
                                        ))
                                    )
                                }
                            </select>
                        </div>
                    )
                }
            </div>
        </div>
    );
};

export default ProductInfo;
