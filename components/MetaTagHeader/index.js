import React, { useEffect, useState } from "react";
import Head from 'next/head';
import { getMetaByUrl } from "../../api/category";
import { getErrorMessage } from "../../services/errorHandle"
import { useRouter } from 'next/router'

const MetaTagHeader = () => {
    const initialState = {
        keywords: [],
        description: "default",
        title: "Online Electronics Store with Best Prices | Top Ten Electronics"
    }
    const [metaTag, setMetaTag] = useState(initialState);
    const router = useRouter();

    useEffect(() => {
        const slugUrl = router.asPath;
        getMetaData(slugUrl);
    }, [router]);

    const getMetaData = async (slugUrl) => {
        if (!slugUrl) return;
        const payload = { slugUrl };
        try {
            const res = await getMetaByUrl(payload);
            if (res) setMetaTag(res);
        } catch (err) {
            getErrorMessage(err);
        }
    }

    const { title, description, keywords } = metaTag
    return (
        <Head>
            <title>{title}</title>
            <meta name="description" content={description} />
            <meta name="keywords" content={keywords.join()} />
        </Head>
    );
}

export default MetaTagHeader;