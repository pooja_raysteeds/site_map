const ProductFilters = () => {
    return (
        <>
            <div className="border-bottom border-color-1 mb-5">
                <h3 className="section-title section-title__sm mb-0 pb-2 font-size-18">
                    Filters
                </h3>
            </div>
            <div className="border-bottom pb-4 mb-4">
                <h4 className="font-size-14 mb-3 font-weight-bold">Brands</h4>
                {/* Checkboxes */}
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="brandAdidas"
                        />
                        <label className="custom-control-label" htmlFor="brandAdidas">
                            Adidas
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="brandNewBalance"
                        />
                        <label className="custom-control-label" htmlFor="brandNewBalance">
                            New Balance
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="brandNike"
                        />
                        <label className="custom-control-label" htmlFor="brandNike">
                            Nike
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="brandFredPerry"
                        />
                        <label className="custom-control-label" htmlFor="brandFredPerry">
                            Fred Perry
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="brandTnf"
                        />
                        <label className="custom-control-label" htmlFor="brandTnf">
                            The North Face
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                {/* End Checkboxes */}
                {/* View More - Collapse */}
                <div className="collapse" id="collapseBrand">
                    <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                        <div className="custom-control custom-checkbox">
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="brandGucci"
                            />
                            <label className="custom-control-label" htmlFor="brandGucci">
                                Gucci
                                <span className="text-gray-25 font-size-12 font-weight-normal">
                                    {" "}
                                    (56)
                                </span>
                            </label>
                        </div>
                    </div>
                    <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                        <div className="custom-control custom-checkbox">
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="brandMango"
                            />
                            <label className="custom-control-label" htmlFor="brandMango">
                                Mango
                                <span className="text-gray-25 font-size-12 font-weight-normal">
                                    {" "}
                                    (56)
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                {/* End View More - Collapse */}
                {/* Link */}
                <a
                    className="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2"
                    data-toggle="collapse"
                    href="#collapseBrand"
                    role="button"
                    aria-expanded="false"
                    aria-controls="collapseBrand"
                >
                    <span className="link__icon text-gray-27 bg-white">
                        <span className="link__icon-inner">+</span>
                    </span>
                    <span className="link-collapse__default">Show more</span>
                    <span className="link-collapse__active">Show less</span>
                </a>
                {/* End Link */}
            </div>
            <div className="border-bottom pb-4 mb-4">
                <h4 className="font-size-14 mb-3 font-weight-bold">Color</h4>
                {/* Checkboxes */}
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="categoryTshirt"
                        />
                        <label className="custom-control-label" htmlFor="categoryTshirt">
                            Black{" "}
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="categoryShoes"
                        />
                        <label className="custom-control-label" htmlFor="categoryShoes">
                            Black Leather{" "}
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="categoryAccessories"
                        />
                        <label
                            className="custom-control-label"
                            htmlFor="categoryAccessories"
                        >
                            Black with Red{" "}
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="categoryTops"
                        />
                        <label className="custom-control-label" htmlFor="categoryTops">
                            Gold{" "}
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                    <div className="custom-control custom-checkbox">
                        <input
                            type="checkbox"
                            className="custom-control-input"
                            id="categoryBottom"
                        />
                        <label className="custom-control-label" htmlFor="categoryBottom">
                            Spacegrey{" "}
                            <span className="text-gray-25 font-size-12 font-weight-normal">
                                {" "}
                                (56)
                            </span>
                        </label>
                    </div>
                </div>
                {/* End Checkboxes */}
                {/* View More - Collapse */}
                <div className="collapse" id="collapseColor">
                    <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                        <div className="custom-control custom-checkbox">
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="categoryShorts"
                            />
                            <label className="custom-control-label" htmlFor="categoryShorts">
                                Turquoise{" "}
                                <span className="text-gray-25 font-size-12 font-weight-normal">
                                    {" "}
                                    (56)
                                </span>
                            </label>
                        </div>
                    </div>
                    <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                        <div className="custom-control custom-checkbox">
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="categoryHats"
                            />
                            <label className="custom-control-label" htmlFor="categoryHats">
                                White{" "}
                                <span className="text-gray-25 font-size-12 font-weight-normal">
                                    {" "}
                                    (56)
                                </span>
                            </label>
                        </div>
                    </div>
                    <div className="form-group d-flex align-items-center justify-content-between mb-2 pb-1">
                        <div className="custom-control custom-checkbox">
                            <input
                                type="checkbox"
                                className="custom-control-input"
                                id="categorySocks"
                            />
                            <label className="custom-control-label" htmlFor="categorySocks">
                                White with Gold{" "}
                                <span className="text-gray-25 font-size-12 font-weight-normal">
                                    {" "}
                                    (56)
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                {/* End View More - Collapse */}
                {/* Link */}
                <a
                    className="link link-collapse small font-size-13 text-gray-27 d-inline-flex mt-2"
                    data-toggle="collapse"
                    href="#collapseColor"
                    role="button"
                    aria-expanded="false"
                    aria-controls="collapseColor"
                >
                    <span className="link__icon text-gray-27 bg-white">
                        <span className="link__icon-inner">+</span>
                    </span>
                    <span className="link-collapse__default">Show more</span>
                    <span className="link-collapse__active">Show less</span>
                </a>
                {/* End Link */}
            </div>

            <div className="range-slider">
                <h4 className="font-size-14 mb-3 font-weight-bold">Price</h4>
                {/* Range Slider */}
                <span className="irs js-irs-0 u-range-slider u-range-slider-indicator u-range-slider-grid">
                    <span className="irs">
                        <span className="irs-line" tabIndex={0}>
                            <span className="irs-line-left" />
                            <span className="irs-line-mid" />
                            <span className="irs-line-right" />
                        </span>
                        <span className="irs-min" style={{ display: "none" }}>
                            0
                        </span>
                        <span className="irs-max" style={{ display: "none" }}>
                            1
                        </span>
                        <span className="irs-from" style={{ display: "none", left: "0%" }}>
                            0
                        </span>
                        <span className="irs-to" style={{ display: "none", left: "0%" }}>
                            0
                        </span>
                        <span className="irs-single" style={{ display: "none", left: "0%" }}>
                            0
                        </span>
                    </span>
                    <span className="irs-grid" />
                    <span className="irs-bar" style={{ left: "2.96296%", width: "94.0741%" }} />
                    <span className="irs-shadow shadow-from" style={{ display: "none" }} />
                    <span className="irs-shadow shadow-to" style={{ display: "none" }} />
                    <span className="irs-slider from" style={{ left: "0%" }} />
                    <span className="irs-slider to" style={{ left: "94.0741%" }} />
                </span>
                <input
                    className="js-range-slider irs-hidden-input"
                    type="text"
                    data-extra-classes="u-range-slider u-range-slider-indicator u-range-slider-grid"
                    data-type="double"
                    data-grid="false"
                    data-hide-from-to="true"
                    data-prefix="$"
                    data-min={0}
                    data-max={3456}
                    data-from={0}
                    data-to={3456}
                    data-result-min="#rangeSliderExample3MinResult"
                    data-result-max="#rangeSliderExample3MaxResult"
                    tabIndex={-1}
                    readOnly
                />
                {/* End Range Slider */}
                <div className="mt-1 text-gray-111 d-flex mb-4">
                    <span className="mr-0dot5">Price: </span>
                    <span>$</span>
                    <span id="rangeSliderExample3MinResult" className>
                        0
                    </span>
                    <span className="mx-0dot5"> — </span>
                    <span>$</span>
                    <span id="rangeSliderExample3MaxResult" className>
                        3456
                    </span>
                </div>
                <button type="submit" className="btn px-4 btn-primary-dark-w py-2 rounded-lg">
                    Filter
                </button>
            </div>
        </>
    );
}

export default ProductFilters;