import React, { useEffect, useState, useContext, useRef } from "react";
import Link from "next/link";
import { useRouter } from 'next/router'
import { getErrorMessage } from "../../services/errorHandle";
import { getS3Url, calcPercentage, getLocalStorage } from "../../services/helper"
import { F_O_T_TAB_LIMIT } from "../../services/constant";
import { getFilteredProducts } from "../../api/products";
import AppContext from "../../context/AppContext";
import Slider from "react-slick";

const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  arrows: true,
  lazyLoad: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  initialSlide: 0,
  className: 'u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1',
  customPaging: function (i) {
    return (
      <span />
    );
  },
  responsive: [{
    "breakpoint": 1800,
    "settings": {
      "slidesToShow": 4
    }
  }, {
    "breakpoint": 1400,
    "settings": {
      "slidesToShow": 4
    }
  }, {
    "breakpoint": 1200,
    "settings": {
      "slidesToShow": 3
    }
  }, {
    "breakpoint": 992,
    "settings": {
      "slidesToShow": 3
    }
  }, {
    "breakpoint": 768,
    "settings": {
      "slidesToShow": 2
    }
  }, {
    "breakpoint": 554,
    "settings": {
      "slidesToShow": 1
    }
  }],
  dotsClass: "text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0",
};

const FeaturedTab = () => {
  const [products, setProducts] = useState([]);
  const [params, setParams] = useState({ is_featured: true, limit: F_O_T_TAB_LIMIT });
  const userId = getLocalStorage("usrid");
  const cartValue = getLocalStorage("crt");
  const wishlistValue = getLocalStorage("wstl");
  const userCompares = getLocalStorage("usrCmpr");
  const { addProductToCart, addProductToWishlist, removeProductToWishlist, addToCompare } = useContext(AppContext);
  const customSlider = useRef(null)
  const router = useRouter()


  useEffect(() => {
    getFilteredProducts(params)
      .then((res) => setProducts(res))
      .catch((err) => getErrorMessage(err));
  }, [params]);

  const handleTabChange = (e, val) => {
    if (val === "is_featured") {
      setParams({ is_featured: true, limit: F_O_T_TAB_LIMIT });
    }
    if (val === "on_sale") {
      setParams({ on_sale: true, limit: F_O_T_TAB_LIMIT });
    }
    if (val === "top_rated") {
      setParams({ top_rated: true, limit: F_O_T_TAB_LIMIT });
    }
  };

  const handleCart = (product_id) => {
    const params = { product_id, quantity: 1 }
    addProductToCart(params)
  }

  const handleWishlist = (e, product_id) => {
    e.preventDefault()
    addProductToWishlist(product_id)
  }

  const removeWishlist = (e, product_id) => {
    e.preventDefault()
    removeProductToWishlist(product_id);
  }

  const handleCompare = (e, id) => {
    e.preventDefault();
    addToCompare(id);
    router.push("/compare")
  }

  return (
    <div className="container fot" >
      <div className="mb-6">
        <div className="position-relative bg-white text-center z-index-2">
          <ul
            className="nav nav-classic nav-tab justify-content-center"
            id="pills-tab"
            role="tablist"
          >
            <li
              className="nav-item"
              onClick={(e) => handleTabChange(e, "is_featured")}
            >
              <a
                className="nav-link active js-animation-link"
                id="pills-one-example1-tab"
                data-toggle="pill"
                href="#pills-one-example1"
                role="tab"
                aria-controls="pills-one-example1"
                aria-selected="true"
                data-target="#pills-one-example1"
                data-link-group="groups"
                data-animation-in="slideInUp"
              >
                <div className="d-md-flex justify-content-md-center align-items-md-center">
                  Featured
                </div>
              </a>
            </li>
            <li
              className="nav-item"
              onClick={(e) => handleTabChange(e, "on_sale")}
            >
              <a
                className="nav-link js-animation-link"
                id="pills-two-example1-tab"
                data-toggle="pill"
                href="#pills-two-example1"
                role="tab"
                aria-controls="pills-two-example1"
                aria-selected="false"
                data-target="#pills-two-example1"
                data-link-group="groups"
                data-animation-in="slideInUp"
              >
                <div className="d-md-flex justify-content-md-center align-items-md-center">
                  On Sale
                </div>
              </a>
            </li>
            <li
              className="nav-item"
              onClick={(e) => handleTabChange(e, "top_rated")}
            >
              <a
                className="nav-link js-animation-link"
                id="pills-three-example1-tab"
                data-toggle="pill"
                href="#pills-three-example1"
                role="tab"
                aria-controls="pills-three-example1"
                aria-selected="false"
                data-target="#pills-three-example1"
                data-link-group="groups"
                data-animation-in="slideInUp"
              >
                <div className="d-md-flex justify-content-md-center align-items-md-center">
                  Top Rated
                </div>
              </a>
            </li>
          </ul>
        </div>
        <div className="position-relative tab-content" id="pills-tabContent">
          <div
            className="tab-pane fade pt-2 show active"
            id="pills-one-example1"
            role="tabpanel"
            aria-labelledby="pills-one-example1-tab"
            data-target-group="groups"
          >
            <Slider {...settings} ref={customSlider} >
              {products.map((p) => (
                <div key={p.id} className="product-item">
                  <div className="product-item__outer h-100 w-100">
                    <div className="product-item__inner px-xl-4 p-3">
                      <div className="product-item__body pb-xl-2">
                        <div className="mb-2">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="font-size-12 text-gray-5">
                              {p.category.title}
                            </a>
                          </Link>
                        </div>
                        <h5 className="mb-1 product-item__title">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="text-blue font-weight-bold">
                              {p.title}
                            </a>
                          </Link>
                        </h5>
                        <div className="mb-2">
                          {p.stock_quantity && p.stock_quantity ? (
                            <Link href={`/shop/${p.slug_url}`}>
                              <a className="d-block text-center">
                                {
                                  p.galleries.length > 0 && (
                                    <img
                                      rel="preload"
                                      className="img-fluid"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                  )
                                }
                              </a>
                            </Link>) : (<Link
                              href={`/shop/${p.slug_url}`}
                              key={p.id}
                            >
                              <a className="d-block text-center">
                                {

                                  <div className="outofstock">
                                    <img
                                      rel="preload"
                                      className="img-fluid ousimage"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                    <div className="middlefotlist">
                                      <div className="textlist">Out of stock</div>
                                    </div>
                                  </div>

                                }
                              </a>
                            </Link>)}
                        </div>

                        <span className="text-green font-size-15 font-weight-bold mt-1">
                          {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                        </span>
                        <div className="flex-center-between mb-1">
                          <div className="prodcut-price d-flex">
                            <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                            <del className="delete-price mt-1">
                              {`₹${p.mrp}`}
                            </del>
                          </div>
                          {p.stock_quantity === 0 ? (<></>) : (
                            <div className="d-none d-xl-block prodcut-add-cart">
                              {
                                cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                  <Link href="/cart">
                                    <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                      <i className="ec ec-shopping-bag" />
                                    </a>
                                  </Link>
                                ) : (
                                  <Link href="/cart">
                                    <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                      <i className="ec ec-add-to-cart" />
                                    </a>
                                  </Link>
                                )
                              }
                            </div>)}
                        </div>
                      </div>
                      <div className="product-item__footer">
                        <div className="border-top pt-2 flex-center-between flex-wrap">
                          {
                            userCompares && (
                              userCompares.filter(item => item == p.id).length > 0 ? (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Go to Compare
                                  </a>
                                </Link>
                              ) : (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Compare
                                  </a>
                                </Link>
                              )
                            )
                          }
                          {
                            userId ? (
                              <Link href="/wishlist">
                                {
                                  wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                    <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                      Remove from Wishlist
                                    </a>
                                  ) : (
                                    <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                      Wishlist
                                    </a>
                                  )
                                }
                              </Link>
                            ) : (
                              <Link href="/login">
                                <a className="text-gray-6 font-size-13 mr-2">
                                  <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                  Wishlist
                                </a>
                              </Link>
                            )
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </Slider>
          </div>
          <div
            className="tab-pane fade pt-2"
            id="pills-two-example1"
            role="tabpanel"
            aria-labelledby="pills-two-example1-tab"
            data-target-group="groups"
          >
            <Slider {...settings} >
              {products.map((p) => (
                <li key={p.id} className="product-item">
                  <div className="product-item__outer h-100 w-100">
                    <div className="product-item__inner px-xl-4 p-3">
                      <div className="product-item__body pb-xl-2">
                        <div className="mb-2">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="font-size-12 text-gray-5">
                              {p.category.title}
                            </a>
                          </Link>
                        </div>
                        <h5 className="mb-1 product-item__title">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="text-blue font-weight-bold">
                              {p.title}
                            </a>
                          </Link>
                        </h5>
                        <div className="mb-2">
                          {p.stock_quantity && p.stock_quantity ? (
                            <Link href={`/shop/${p.slug_url}`}>
                              <a className="d-block text-center">
                                {
                                  p.galleries.length > 0 && (
                                    <img
                                      rel="preload"
                                      className="img-fluid"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                  )
                                }
                              </a>
                            </Link>) : (<Link
                              href={`/shop/${p.slug_url}`}
                              key={p.id}
                            >
                              <a className="d-block text-center">
                                {

                                  <div className="outofstock">
                                    <img
                                      rel="preload"
                                      className="img-fluid ousimage"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                    <div className="middlefotlist">
                                      <div className="textlist">Out of stock</div>
                                    </div>
                                  </div>

                                }
                              </a>
                            </Link>)}
                        </div>

                        <span className="text-green font-size-15 font-weight-bold mt-1">
                          {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                        </span>
                        <div className="flex-center-between mb-1">
                          <div className="prodcut-price d-flex">
                            <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                            <del className="delete-price mt-1">
                              {`₹${p.mrp}`}
                            </del>
                          </div>
                          {p.stock_quantity === 0 ? (<></>) : (
                            <div className="d-none d-xl-block prodcut-add-cart">
                              {
                                cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                  <Link href="/cart">
                                    <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                      <i className="ec ec-shopping-bag" />
                                    </a>
                                  </Link>
                                ) : (
                                  <Link href="/cart">
                                    <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                      <i className="ec ec-add-to-cart" />
                                    </a>
                                  </Link>
                                )
                              }
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="product-item__footer">
                        <div className="border-top pt-2 flex-center-between flex-wrap">
                          {
                            userCompares && (
                              userCompares.filter(item => item == p.id).length > 0 ? (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Go to Compare
                                  </a>
                                </Link>
                              ) : (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Compare
                                  </a>
                                </Link>
                              )
                            )
                          }
                          {
                            userId ? (
                              <Link href="/wishlist">
                                {
                                  wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                    <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                      Remove from Wishlist
                                    </a>
                                  ) : (
                                    <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                      Wishlist
                                    </a>
                                  )
                                }
                              </Link>
                            ) : (
                              <Link href="/login">
                                <a className="text-gray-6 font-size-13 mr-2">
                                  <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                  Wishlist
                                </a>
                              </Link>
                            )
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              ))}
            </Slider>
          </div>
          <div
            className="tab-pane fade pt-2"
            id="pills-three-example1"
            role="tabpanel"
            aria-labelledby="pills-three-example1-tab"
            data-target-group="groups"
          >
            <Slider {...settings} >
              {products.map((p) => (
                <li key={p.id} className="product-item">
                  <div className="product-item__outer h-100 w-100">
                    <div className="product-item__inner px-xl-4 p-3">
                      <div className="product-item__body pb-xl-2">
                        <div className="mb-2">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="font-size-12 text-gray-5">
                              {p.category.title}
                            </a>
                          </Link>
                        </div>
                        <h5 className="mb-1 product-item__title">
                          <Link href={`/shop/${p.slug_url}`}>
                            <a className="text-blue font-weight-bold">
                              {p.title}
                            </a>
                          </Link>
                        </h5>
                        {/* <div className="mb-2">
                          <a className="d-block text-center">
                            {
                              p.galleries.length > 0 && (
                                <img
                                  className="img-fluid"
                                  src={getS3Url(p.galleries[0].image_url)}
                                  alt={p.title}
                                />
                              )
                            }
                          </a>
                        </div> */}
                        <div className="mb-2">
                          {p.stock_quantity && p.stock_quantity ? (
                            <Link href={`/shop/${p.slug_url}`}>
                              <a className="d-block text-center">
                                {
                                  p.galleries.length > 0 && (
                                    <img
                                      rel="preload"
                                      className="img-fluid"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                  )
                                }
                              </a>
                            </Link>) : (<Link
                              href={`/shop/${p.slug_url}`}
                              key={p.id}
                            >
                              <a className="d-block text-center">
                                {

                                  <div className="outofstock">
                                    <img
                                      rel="preload"
                                      className="img-fluid ousimage"
                                      src={getS3Url(p.galleries[0].image_url)}
                                      alt={p.title}
                                    />
                                    <div className="middlefotlist">
                                      <div className="textlist">Out of stock</div>
                                    </div>
                                  </div>

                                }
                              </a>
                            </Link>)}
                        </div>

                        <span className="text-green font-size-15 font-weight-bold mt-1">
                          {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                        </span>
                        <div className="flex-center-between mb-1">
                          <div className="prodcut-price d-flex">
                            <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                            <del className="delete-price mt-1">
                              {`₹${p.mrp}`}
                            </del>
                          </div>
                          {p.stock_quantity === 0 ? (<></>) : (
                            <div className="d-none d-xl-block prodcut-add-cart">
                              {
                                cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                  <Link href="/cart">
                                    <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                      <i className="ec ec-shopping-bag" />
                                    </a>
                                  </Link>
                                ) : (
                                  <Link href="/cart">
                                    <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                      <i className="ec ec-add-to-cart" />
                                    </a>
                                  </Link>
                                )
                              }
                            </div>
                          )}
                        </div>
                      </div>
                      <div className="product-item__footer">
                        <div className="border-top pt-2 flex-center-between flex-wrap">
                          {
                            userCompares && (
                              userCompares.filter(item => item == p.id).length > 0 ? (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Go to Compare
                                  </a>
                                </Link>
                              ) : (
                                <Link href="/compare">
                                  <a onClick={(e) => handleCompare(e, p.id)} className="text-gray-6 font-size-13">
                                    <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                    Compare
                                  </a>
                                </Link>
                              )
                            )
                          }
                          {
                            userId ? (
                              <Link href="/wishlist">
                                {
                                  wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                    <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                      Remove from Wishlist
                                    </a>
                                  ) : (
                                    <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                      <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                      Wishlist
                                    </a>
                                  )
                                }
                              </Link>
                            ) : (
                              <Link href="/login">
                                <a className="text-gray-6 font-size-13 mr-2">
                                  <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                  Wishlist
                                </a>
                              </Link>
                            )
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeaturedTab;
