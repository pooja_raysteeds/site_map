import React, { useState, useContext, useEffect, useRef } from "react";
import Link from 'next/link'
import { useRouter } from 'next/router'
import AppContext from "../../context/AppContext"
import { getLocalStorage } from "../../services/helper"
import { getErrorMessage } from "../../services/errorHandle"
import debounce from 'lodash.debounce';
import { getSearch } from "../../api/products"
import Styles from "./header.module.css"

const SideMenu = ({ data }) => {
    const router = useRouter()
    const { cartCount, wishListCount, resetUser, username, isMobileView } = useContext(AppContext)
    const userId = getLocalStorage("usrid")
    const [search, setSearch] = useState("")
    const [searchList, setSearchList] = useState(null)
    const [showSearch, setShowSearch] = useState(false);
    const [toggleHamburger, setToggleHamburger] = useState(true)
    const [iconAria, setIconAria] = useState(false);
    const wrapperRef = useRef(null);

    const handleClickOutside = event => {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            setShowSearch(false);
        }
    };


    const handleChange = (e) => {
        setShowSearch(true)
        const { value } = e.target || null
        if (!value || (value && value.length === 1)) {
            setSearchList(null)
        }
        setSearch(value)
    }

    useEffect(() => {
        if (search && search.length > 1) {
            debouncedFetchData(search, res => {
                setSearchList(res)
            });
        }
        document.addEventListener("click", handleClickOutside, false);
        return () => {
            document.removeEventListener("click", handleClickOutside, false);
        };
    }, [search]);

    const debouncedFetchData = debounce((query, cb) => {
        fetchSearchList(query);
    }, 500);

    const fetchSearchList = (query) => {
        getSearch(query).then(
            res => setSearchList(res)
        ).catch(
            err => getErrorMessage(err)
        )
    };

    const handleSearch = (e) => {
        e.preventDefault();
        router.push(`/shop?search=${search}`)
    }

    const handleToggle = () => {
        setToggleHamburger(!toggleHamburger)
        setIconAria(!iconAria)
    }
    const handleLogout = async (e) => {
        e.preventDefault();
        resetUser()
        router.push('/')
    }

    return (
        <div className="container my-0dot5 my-xl-0">
            <div className="row align-items-center">
                <div className="col-auto">
                    <nav className="navbar navbar-expand u-header__navbar py-0 justify-content-xl-between max-width-270 min-width-270">
                        <Link href="/">
                            <a
                                className="order-1 order-xl-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-center"
                                aria-label="TopTen"
                            >
                                <img
                                    rel="preload"
                                    className="img-fluid logo-pic"
                                    src="../../assets/img/75X75/ten-image.png"
                                    alt="Logo" />
                            </a>
                        </Link>

                        {/* Fullscreen Toggle Button */}
                        <button
                            id="sidebarHeaderInvokerMenu"
                            type="button"
                            className={`navbar-toggler d-block btn u-hamburger mr-3 mr-xl-0 ${toggleHamburger ? 'target-of-invoker-has-unfolds active' : ''}`}
                            aria-controls="sidebarHeader"
                            aria-haspopup="true"
                            aria-expanded={iconAria}
                            data-unfold-event="click"
                            data-unfold-hide-on-scroll="false"
                            data-unfold-target="#sidebarHeader1"
                            data-unfold-type="css-animation"
                            data-unfold-animation-in="fadeInLeft"
                            data-unfold-animation-out="fadeOutLeft"
                            data-unfold-duration={500}
                        >
                            <span id="hamburgerTriggerMenu" className="u-hamburger__box">
                                <span className="u-hamburger__inner mob-hamburger-inner" />
                            </span>
                        </button>
                    </nav>

                    {/* ========== HEADER SIDEBAR ========== */}
                    <aside
                        id="sidebarHeader1"
                        className={`u-sidebar u-sidebar--left ${toggleHamburger ? 'u-unfold--css-animation u-unfold--hidden' : 'u-unfold--css-animation fadeInLeft'}`}

                        aria-labelledby="sidebarHeaderInvokerMenu"
                    >
                        <div className="u-sidebar__scroller">
                            <div className="u-sidebar__container">
                                <div className="u-header-sidebar__footer-offset pb-0 sidemenuscroll">
                                    <div className="position-absolute top-0 right-0 z-index-2 pt-4 pr-7">
                                        <button
                                            type="button"
                                            className="close ml-auto"
                                            aria-controls="sidebarHeader"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                            data-unfold-event="click"
                                            data-unfold-hide-on-scroll="false"
                                            data-unfold-target="#sidebarHeader1"
                                            data-unfold-type="css-animation"
                                            data-unfold-animation-in="fadeInLeft"
                                            data-unfold-animation-out="fadeOutLeft"
                                            data-unfold-duration={500}
                                        >
                                            <span aria-hidden="true">
                                                <i className="ec ec-close-remove text-gray-90 font-size-20" />
                                            </span>
                                        </button>
                                    </div>
                                    <div className="js-scrollbar u-sidebar__body">
                                        <div
                                            id="headerSidebarContent"
                                            className="u-sidebar__content u-header-sidebar__content"
                                        >
                                            <Link href="/">
                                                <a
                                                    className="d-flex ml-0 navbar-brand u-header__navbar-brand u-header__navbar-brand-vertical"
                                                    aria-label="TopTen"
                                                >
                                                    <img
                                                        rel="preload"
                                                        className="img-fluid logo-pic"
                                                        src="../../assets/img/75X75/ten-image.png"
                                                        alt="Logo Aside"
                                                    />
                                                </a>
                                            </Link>

                                            <ul
                                                id="headerSidebarList"
                                                className="u-header-collapse__nav"
                                            >
                                                {
                                                    data.map((category, index) => (
                                                        <li key={category.id} className="u-has-submenu u-header-collapse__submenu">
                                                            <Link href={`/shop?parent=${category.slug_url}`}>
                                                                <a
                                                                    className="u-header-collapse__nav-link u-header-collapse__nav-pointer"
                                                                    role="button"
                                                                    data-toggle="collapse"
                                                                    aria-expanded="false"
                                                                    aria-controls={`headerSidebar${index}HomeCollapse`}
                                                                    data-target={`#headerSidebar${index}HomeCollapse`}
                                                                    onClick={handleToggle}
                                                                >
                                                                    {category.title}
                                                                </a>
                                                            </Link>
                                                            <div
                                                                id={`headerSidebar${index}HomeCollapse`}
                                                                className="collapse"
                                                                data-parent="#headerSidebarContent"
                                                            >
                                                                <ul
                                                                    id="headerSidebarHomeMenu"
                                                                    className="u-header-collapse__nav-list"
                                                                >
                                                                    {
                                                                        category.children.map(child => (
                                                                            <li key={child.id}>
                                                                                <Link href={`/shop?category=${child.slug_url}`}>
                                                                                    <a onClick={handleToggle} className="u-header-collapse__submenu-nav-link">
                                                                                        {child.title}
                                                                                    </a>
                                                                                </Link>
                                                                            </li>
                                                                        ))
                                                                    }
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    ))
                                                }
                                                <ul id="sidemenu-mobileview" className="u-header-collapse__nav">

                                                    <li className="u-has-submenu u-header-collapse__submenu">
                                                        <a className="u-header-collapse__nav-link u-header-collapse__nav-pointer" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="headerSidebarHomeCollapse" data-target="#headerSidebarHomeCollapse">
                                                            My Account
                                                        </a>
                                                        <div id="headerSidebarHomeCollapse" className="collapse" data-parent="#headerSidebarContent">
                                                            <ul id="headerSidebarHomeMenu" className="u-header-collapse__nav-list">

                                                                <li><Link href="/orders">
                                                                    <a className="u-header-collapse__submenu-nav-link" href="#">
                                                                        My Orders
                                                                    </a>
                                                                </Link></li>

                                                                <li> <Link href="/wishlist">
                                                                    <a className="u-header-collapse__submenu-nav-link" href="#">
                                                                        Wishlist
                                                                    </a>
                                                                </Link></li>

                                                                <li> <Link href="/cart">
                                                                    <a className="u-header-collapse__submenu-nav-link" href="#">
                                                                        Cart
                                                                    </a>
                                                                </Link></li>

                                                                <li><Link href="/my-account">
                                                                    <a className="u-header-collapse__submenu-nav-link" href="#">
                                                                        Profile
                                                                    </a>
                                                                </Link></li>




                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div id="sidemenu-mobileview" className="mt-2">

                                                    <li className="nav-item u-header__nav-item"
                                                        data-event="hover"
                                                        data-position="left">
                                                        <Link href="/contact-us">
                                                            <a onClick={handleToggle} className="u-header-collapse__submenu-nav-link">Contact Us</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/compare">
                                                            <a className="u-header-collapse__submenu-nav-link">Compare</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/store-locator">
                                                            <a onClick={handleToggle} className="u-header-collapse__submenu-nav-link">Store Locator</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/track-order">
                                                            <a onClick={handleToggle} className="u-header-collapse__submenu-nav-link">Track Your Order</a>
                                                        </Link></li>
                                                    <li> <Link href="/">
                                                        <a onClick={handleLogout} className="u-header-collapse__submenu-nav-link" href="#">
                                                            Logout
                                                        </a>
                                                    </Link></li>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>

                {/* Search Bar */}
                <div className="col d-none d-xl-block">
                    <form className="js-focus-state" onSubmit={handleSearch}>
                        <label className="sr-only" htmlFor="searchproduct">
                            Search
                        </label>
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control py-2 pl-5 font-size-15 border-right-0 height-40 border-width-2 rounded-left-pill border-primary"
                                name="search"s
                                value={search}
                                autoComplete="off"
                                onChange={handleChange}
                                id="searchproduct-item"
                                placeholder="Search for Products"
                                aria-label="Search for Products"
                                aria-describedby="searchProduct1"
                            />
                            <div className="input-group-append">
                                <button
                                    className="btn btn-primary height-40 py-2 px-3 rounded-right-pill"
                                    type="submit"
                                    id="searchProduct1"
                                    onClick={() => setShowSearch(false)}
                                >
                                    <span className="ec ec-search font-size-24 text-white" />
                                </button>
                            </div>
                        </div>
                        {searchList && showSearch && (
                            <ul ref={wrapperRef} className={Styles.listing + ' list-group dropdown-menu dropdown-unfold ml-3'}>
                                {
                                    searchList.brands.map((item, index) => (
                                        <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                            <Link href={`/shop?brand=["${item.slug_url}"]`}>
                                                <a>{`${item.name} in  Brands`}</a>
                                            </Link>
                                        </li>
                                    ))
                                }
                                {
                                    searchList.categories.map((item, index) => (
                                        <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                            <Link href={`/shop?category=${item.slug_url}`}>
                                                <a>{`${item.title} in  Category`}</a>
                                            </Link>
                                        </li>
                                    ))
                                }
                                {
                                    searchList.products.map((item, index) => (
                                        <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                            <Link href={`/shop/${item.slug_url}`}>
                                                <a>{item.title}</a>
                                            </Link>
                                        </li>
                                    ))
                                }

                                {!searchList.brands.length &&
                                    !searchList.categories.length &&
                                    !searchList.products.length ? (
                                    <li onClick={() => setShowSearch(false)} className={Styles.lists}
                                        key={1}
                                    >
                                        No Data Found
                                    </li>
                                ) : null
                                }
                            </ul>
                        )}
                    </form>
                </div>

                {/* Mobile Header */}
                <div className="col col-xl-auto text-right text-xl-left pl-0 pl-xl-3 position-static">
                    <div className="d-inline-flex">
                        <ul className="d-flex list-unstyled mb-0 align-items-center">
                            <li className="col d-xl-none px-2 px-sm-3 position-static">
                                <a
                                    id="searchClassicInvoker"
                                    className="font-size-22 text-gray-90 text-lh-1 btn-text-secondary"
                                    role="button"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Search"
                                    aria-controls="searchClassic"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    data-unfold-target="#searchClassic"
                                    data-unfold-type="css-animation"
                                    data-unfold-duration={300}
                                    data-unfold-delay={300}
                                    data-unfold-hide-on-scroll="true"
                                    data-unfold-animation-in="slideInUp"
                                    data-unfold-animation-out="fadeOut"
                                >
                                    <span className="ec ec-search mob-icons-color" />
                                </a>
                                <div
                                    id="searchClassic"
                                    className="dropdown-menu dropdown-unfold dropdown-menu-right left-0 mx-2"
                                    aria-labelledby="searchClassicInvoker"
                                >
                                    <form className="js-focus-state input-group px-3" onSubmit={handleSearch}>
                                        <input
                                            className="form-control"
                                            type="search"
                                            name="search"
                                            value={search}
                                            onChange={handleChange}
                                            placeholder="Search Product"
                                        />
                                        <div className="input-group-append">
                                            <button className="btn btn-primary px-3" type="submit">
                                                <i className="font-size-18 ec ec-search mob-icons-color" />
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                {searchList && showSearch && (
                                    <ul className={Styles.listing + ' list-group dropdown-menu dropdown-unfold ml-3'} style={{ top: "9rem" }}>
                                        {
                                            searchList.brands.map((item, index) => (
                                                <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                                    <Link href={`/shop?brand=${item.slug_url}`}>
                                                        <a>{`${item.name} in  Brands`}</a>
                                                    </Link>
                                                </li>
                                            ))
                                        }
                                        {
                                            searchList.categories.map((item, index) => (
                                                <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                                    <Link href={`/shop?category=${item.slug_url}`}>
                                                        <a>{`${item.title} in  Category`}</a>
                                                    </Link>
                                                </li>
                                            ))
                                        }
                                        {
                                            searchList.products.map((item, index) => (
                                                <li onClick={() => setShowSearch(false)} className={Styles.lists} key={index}>
                                                    <Link href={`/shop/${item.slug_url}`}>
                                                        <a>{item.title}</a>
                                                    </Link>
                                                </li>
                                            ))
                                        }

                                        {!searchList.brands.length &&
                                            !searchList.categories.length &&
                                            !searchList.products.length ? (
                                            <li onClick={() => setShowSearch(false)} className={Styles.lists}
                                                key={1}
                                            >
                                                No Data Found
                                            </li>
                                        ) : null
                                        }
                                    </ul>
                                )}
                            </li>
                            <li className="col d-none d-xl-block">
                                <Link href={"/compare"}>
                                    <a
                                        href="/compare"
                                        className="text-gray-90"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Compare"
                                    >
                                        <i className="font-size-22 ec ec-compare" />
                                    </a>
                                </Link>
                            </li>
                            <li className="col pr-xl-0 px-2 px-sm-3">
                                <Link href={userId ? "/wishlist" : "/login"}>
                                    <a
                                        className="text-gray-90 position-relative d-flex "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="wishlist"
                                    >
                                        <i className="font-size-22 ec ec-favorites" />
                                        <span className="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12 text-white">
                                            {wishListCount}
                                        </span>
                                    </a>
                                </Link>
                            </li>
                            {/* <li className="col d-xl-none px-2 px-sm-3">
                                <Link href={userId ? "/my-account" : "/login"}>
                                    <a
                                        className="text-gray-90"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="My Account"
                                    >
                                        <i className="font-size-22 ec ec-user mob-icons-color" />
                                    </a>
                                </Link>
                            </li> */}

                            <li className="col pr-xl-0 px-2 px-sm-3">
                                <Link href="/cart">
                                    <a
                                        className="text-gray-90 position-relative d-flex "
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Cart"
                                    >
                                        <i className="font-size-22 ec ec-shopping-bag mob-icons-color" />
                                        <span className="bg-lg-down-black width-22 height-22 bg-primary position-absolute d-flex align-items-center justify-content-center rounded-circle left-12 top-8 font-weight-bold font-size-12 text-white">
                                            {cartCount}
                                        </span>
                                    </a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SideMenu;