import React, { useEffect, useState, useContext } from "react";
import { useRouter } from 'next/router'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import { getErrorMessage } from "../../services/errorHandle";
import { getCategoryHierarchy } from "../../api/category";
import AppContext from "../../context/AppContext"
import "./header.module.css"
const SideMenu = dynamic(() => import('./SideMenu'))
const Navbar = dynamic(() => import('./Navbar'))

const Header = () => {
    const { username, resetUser } = useContext(AppContext)
    const [categories, setCategories] = useState([])
    const router = useRouter()

    useEffect(() => {
        getCategoryHierarchy(0).then(
            res => setCategories(res)
        ).catch(
            err => getErrorMessage(err)
        )

    }, []);

    const handleLogout = async (e) => {
        e.preventDefault();
        resetUser()
        router.push('/')
    }

    return (
        <header id="header" className="u-header u-header-left-aligned-nav">
            <div className="u-header__section">
                <div className="u-header-topbar py-2 d-none d-xl-block">
                    <div className="container">
                        <div className="d-flex align-items-center">
                            <div className="topbar-left">
                                <Link href="/">
                                    <a className="text-gray-110 font-size-13 u-header-topbar__nav-link">
                                        Welcome to Top Ten Electronics Store
                                    </a>
                                </Link>
                            </div>
                            <div className="topbar-right ml-auto">
                                <ul className="list-inline mb-0">
                                    <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <span className="u-header-topbar__nav-link">
                                            <i className="ec ec-shopping-bag mr-1" />{" "}
                                            100% Satisfaction
                                        </span>
                                    </li>
                                    {/* <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <span className="u-header-topbar__nav-link">
                                            <i className="ec ec-returning mr-1" />{" "}
                                            Quick Delivery
                                        </span>
                                    </li> */}
                                    <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <span className="u-header-topbar__nav-link">
                                            <i className="ec ec-payment mr-1" />{" "}
                                            Secure Payment
                                        </span>
                                    </li>
                                    <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <Link href="/store-locator">
                                            <a className="u-header-topbar__nav-link">
                                                <i className="ec ec-map-pointer mr-1"></i> Store Locator
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <Link href="/track-order">
                                            <a className="u-header-topbar__nav-link">
                                                <i className="ec ec-transport mr-1" />{" "}Track Your Order
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                        <Link href="/contact-us">
                                            <a className="u-header-topbar__nav-link">
                                                <i className="ec ec-phone mr-1" />{" "}Contact Us
                                            </a>
                                        </Link>
                                    </li>
                                    {username && username ? (
                                        <div className="dropdown-details u-header-topbar__nav-item-border">
                                            <span className="d-none d-sm-inline-flex align-items-center">
                                                <i className="ec ec-user mr-1" />{" "}{username}
                                            </span>
                                            <div className="dropdown-details-content">
                                                <Link href="/orders">
                                                    <a className="dropdown-item active" href="#">
                                                        My Orders
                                                    </a>
                                                </Link>
                                                <Link href="/wishlist">
                                                    <a className="dropdown-item" href="#">
                                                        Wishlist
                                                    </a>
                                                </Link>
                                                <Link href="/cart">
                                                    <a className="dropdown-item" href="#">
                                                        Cart
                                                    </a>
                                                </Link>
                                                <Link href="/my-account">
                                                    <a className="dropdown-item" href="#">
                                                        Profile
                                                    </a>
                                                </Link>
                                                <Link href="/">
                                                    <a onClick={handleLogout} className="dropdown-item" href="#">
                                                        Logout
                                                    </a>
                                                </Link>

                                            </div>
                                        </div>
                                    ) : (
                                        <li className="list-inline-item mr-0 u-header-topbar__nav-item u-header-topbar__nav-item-border">
                                            <Link href="/login">
                                                <a
                                                    id="sidebarNavToggler"
                                                    role="button"
                                                    className="u-header-topbar__nav-link"
                                                >
                                                    <i className="ec ec-user mr-1" />{" "}
                                                    Login{" "}
                                                </a>
                                            </Link>
                                            <Link href="/signup">
                                                <a
                                                    id="sidebarNavToggler"
                                                    role="button"
                                                    className="u-header-topbar__nav-link"
                                                >
                                                    <span className="text-gray-50">
                                                        or{" "}
                                                    </span>
                                                    Signup
                                                </a>
                                            </Link>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {/* SideMenu Component */}
                <div className="py-2 py-xl-5 bg-primary-down-lg">
                    <SideMenu data={categories} />
                </div>

                {/* Navigation Menubar Component */}
                <div className="d-none d-xl-block bg-primary">
                    <Navbar data={categories} />
                </div>

            </div>
        </header>
    );
}

export default Header
