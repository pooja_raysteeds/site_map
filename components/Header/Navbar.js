import React, { useState } from 'react'
import Link from 'next/link'
import { getS3Url } from "../../services/helper"

const Navbar = ({ data }) => {

    const [showTab, setShowTab] = useState(true);

    return (
        <div className="container">
            <div className="min-height-45">
                <nav className="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--wide u-header__navbar--no-space">
                    <div
                        id="navBar"
                        className="collapse navbar-collapse u-header__navbar-collapse"
                    >
                        <ul className="navbar-nav u-header__navbar-nav">
                            {
                                data.map(category => (
                                    <li
                                        key={category.id}
                                        className="nav-item hs-has-mega-menu u-header__nav-item"
                                        data-event="hover"
                                        data-animation-in="slideInUp"
                                        data-animation-out="fadeOut"
                                    >
                                        <Link href={`/shop?parent=${category.slug_url}`}>
                                            <a
                                                id="smartphonesMegaMenu"
                                                className="nav-link u-header__nav-link u-header__nav-link-toggle text-white"
                                                aria-haspopup="true"
                                                aria-expanded="false"
                                                onMouseEnter={() => setShowTab(true)}
                                                onClick={() => setShowTab(false)}
                                            >
                                                {category.title}
                                            </a>
                                        </Link>
                                        <div
                                            className="hs-mega-menu w-100 u-header__sub-menu"
                                            aria-labelledby="smartphonesMegaMenu"
                                        >
                                            {showTab && (<div className="row u-header__mega-menu-wrapper">
                                                <div className="col-md-8">
                                                    <div className="row">
                                                        {
                                                            category.children.map(child => (
                                                                <div className="col-md-3" key={child.id}>
                                                                    <span className="u-header__sub-menu-title">
                                                                        <Link href={`/shop?category=${child.slug_url}`}>
                                                                            <a onClick={() => setShowTab(false)} className="nav-link u-header__sub-menu-nav-link">
                                                                                {child.title.toUpperCase()}
                                                                            </a>
                                                                        </Link>
                                                                    </span>
                                                                    <ul className="u-header__sub-menu-nav-group mb-3">
                                                                        {
                                                                            child.children.map(subChild => (
                                                                                <li key={subChild.id}>
                                                                                    <Link href={`/shop?subcategory=${subChild.slug_url}`}>
                                                                                        <a onClick={() => setShowTab(false)} className="nav-link u-header__sub-menu-nav-link">
                                                                                            {subChild.title}
                                                                                        </a>
                                                                                    </Link>
                                                                                </li>
                                                                            ))
                                                                        }
                                                                    </ul>
                                                                </div>
                                                            ))
                                                        }
                                                    </div>
                                                </div>
                                                <div className="col-md-4">
                                                    {
                                                        category.icon_url && (
                                                            <Link href={`/shop?category=${category.slug_url}`}>
                                                                <a className="d-block">
                                                                    <img
                                                                        rel="preload"
                                                                        className="img-fluid"
                                                                        style={{ width: "80%" }}
                                                                        src={getS3Url(category.icon_url)}
                                                                        alt={category.title}
                                                                    />
                                                                </a>
                                                            </Link>
                                                        )
                                                    }

                                                </div>
                                            </div>)}
                                        </div>
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    );
}

export default Navbar;