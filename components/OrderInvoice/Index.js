
import React, { Fragment } from "react";
import {
  Page,
  Document,
  Image,
  StyleSheet,
  Text,
  View,
} from "@react-pdf/renderer";
import dateFormat from "dateformat";

const borderColor = "#dd232a";

const styles = StyleSheet.create({
  page: {
    fontFamily: "Helvetica",
    fontSize: 11,
    paddingTop: 30,
    paddingLeft: 60,
    paddingRight: 60,
    lineHeight: 1.5,
    flexDirection: "column",
  },
  logo: {
    width: 100,
    height: 66,
    marginLeft: "auto",
    marginRight: "auto",
  },
  titleContainer: {
    flexDirection: "row",
    marginTop: 24,
  },
  reportTitle: {
    color: "#dd232a",
    letterSpacing: 4,
    fontSize: 25,
    textAlign: "center",
    textTransform: "uppercase",
  },
  invoiceNoContainer: {
    flexDirection: "row",
    marginTop: 36,
    justifyContent: "flex-end",
  },
  invoiceDateContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  invoiceDate: {
    fontSize: 12,
    fontStyle: "bold",
  },
  label: {
    width: 60,
  },
  headerContainer: {
    marginTop: 36,
  },
  billTo: {
    marginTop: 20,
    paddingBottom: 3,
    fontFamily: "Helvetica-Oblique",
  },
  container: {
    flexDirection: "row",
    borderBottomColor: "#dd232a",
    backgroundColor: "#dd232a",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    textAlign: "center",
    fontStyle: "bold",
    flexGrow: 1,
  },
  description: {
    width: "60%",
    borderRightColor: "#dd232a",
    borderRightWidth: 1,
  },
  qty: {
    width: "10%",
    borderRightColor: "#dd232a",
    borderRightWidth: 1,
  },
  rate: {
    width: "15%",
    borderRightColor: "#dd232a",
    borderRightWidth: 1,
  },
  amount: {
    width: "15%",
  },
  tableContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 24,
    borderWidth: 1,
    borderColor: "#dd232a",
  },
  row: {
    flexDirection: "row",
    borderBottomColor: "#dd232a",
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    textAlign: "center",
    fontStyle: "bold",
    flexGrow: 1,
  },
  descriptionRow: {
    width: "60%",
    textAlign: "left",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    paddingLeft: 8,
  },
  qtyRow: {
    width: "10%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    textAlign: "right",
    paddingRight: 8,
  },
  rateRow: {
    width: "15%",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    textAlign: "right",
    paddingRight: 8,
  },
  amountRow: {
    width: "15%",
    textAlign: "right",
    paddingRight: 8,
  },
  rowTotal: {
    flexDirection: "row",
    borderBottomColor: "#dd232a",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    fontSize: 12,
    fontStyle: "bold",
  },
  descriptionTotal: {
    width: "85%",
    textAlign: "right",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    paddingRight: 8,
  },
  total: {
    width: "15%",
    textAlign: "right",
    paddingRight: 8,
  },
  titleContainermsg: {
    flexDirection: "row",
    marginTop: 12,
  },
  reportTitlemsg: {
    fontSize: 12,
    textAlign: "center",
    textTransform: "uppercase",
  },
  rowTotal: {
    flexDirection: "row",
    borderBottomColor: "#bff0fd",
    borderBottomWidth: 1,
    alignItems: "center",
    height: 24,
    fontSize: 12,
    fontStyle: "bold",
  },
  descriptionTotal: {
    width: "85%",
    textAlign: "right",
    borderRightColor: borderColor,
    borderRightWidth: 1,
    paddingRight: 8,
  },
  total: {
    width: "15%",
    textAlign: "right",
    paddingRight: 8,
  },
  shipto: {
    flexDirection: "row",
  },
});

const OrderInvoice = ({ data }) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <Image style={styles.logo} src="../../assets/img/75X75/ten-image.png" />
      <View style={styles.titleContainer}>
        <Text style={styles.reportTitle}>Invoice</Text>
      </View>
      <Fragment>
        <View style={styles.invoiceNoContainer}>
          <Text style={styles.label}>Invoice No:</Text>
          <Text style={styles.invoiceDate}>{data.invoice_number}</Text>
        </View>
        <View style={styles.invoiceDateContainer}>
          <Text style={styles.label}>Date: </Text>
          <Text>{dateFormat(data.createdAt, "fullDate")}</Text>
        </View>
      </Fragment>
      <Fragment style={styles.shipto}>
        <View style={styles.headerContainer}>
          <Text style={styles.billTo}>Bill To:</Text>
          <Text>
            {data.billing_address.billing_first_name}
            {data.billing_address.billing_last_name}
          </Text>
          <Text>{data.billing_address.billing_street_address}</Text>
          <Text>{data.billing_address.billing_contact_number}</Text>
          <Text>{data.billing_address.billing_email}</Text>
        </View>
        <View style={styles.headerContainer}>
          <Text style={styles.billTo}>Ship To:</Text>
          <Text>
            {data.shipping_address.shipping_first_name}
            {data.shipping_address.shipping_last_name}
          </Text>
          <Text>{data.shipping_address.shipping_street_address}</Text>
          <Text>{data.shipping_address.shipping_contact_number}</Text>
          <Text>{data.shipping_address.shipping_email}</Text>
        </View>
      </Fragment>

      <View style={styles.tableContainer}>
        <View style={styles.container}>
          <Text style={styles.description}>Item Description</Text>
          <Text style={styles.qty}>Qty</Text>
          {/* <Text style={styles.rate}>@</Text> */}
          <Text style={styles.amount}>Amount</Text>
        </View>
        {data.products.map((item, index) => (
          <View style={styles.row} key={index}>
            <Text style={styles.descriptionRow}>{item.product.title}</Text>
            <Text style={styles.qty}>{item.quantity}</Text>
            {/* <Text style={styles.rateRow}>0.3</Text> */}
            <Text style={styles.amount}>{item.product.selling_price}</Text>
          </View>
        ))}
        <View style={styles.rowTotal}>
          <Text style={styles.descriptionTotal}>TOTAL</Text>
          <Text style={styles.total}>{data.total_amount}</Text>
        </View>
        {/* <InvoiceTableFooter items={Rowitems} /> */}
      </View>
      <View style={styles.titleContainermsg}>
        <Text style={styles.reportTitlemsg}>Thank you for your shopping</Text>
      </View>
      {/* <InvoiceTableRow /> */}
    </Page>
  </Document>
);

export default OrderInvoice;
