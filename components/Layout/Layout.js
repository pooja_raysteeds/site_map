
import React, { useEffect } from 'react'
import Head from 'next/head'
import Header from '../Header'
import Footer from '../Footer'

const Layout = ({ children }) => {

    useEffect(() => {
        $(window).on('load', function () {
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
                event: 'hover',
                direction: 'horizontal',
                pageContainer: $('.container'),
                breakpoint: 767.98,
                hideTimeOut: 0
            })
        })
        $(document).ready(function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#header'));

            // initialization of animation
            $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                afterOpen: function () {
                    $(this).find('input[type="search"]').focus()
                }
            })

            // initialization of popups
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of countdowns
            var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
                yearsElSelector: '.js-cd-years',
                monthsElSelector: '.js-cd-months',
                daysElSelector: '.js-cd-days',
                hoursElSelector: '.js-cd-hours',
                minutesElSelector: '.js-cd-minutes',
                secondsElSelector: '.js-cd-seconds'
            })

            // initialization of malihu scrollbar
            $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

            // initialization of forms
            $.HSCore.components.HSFocusState.init();

            // initialization of form validation
            $.HSCore.components.HSValidation.init('.js-validate', {
                rules: {
                    confirmPassword: {
                        equalTo: '#signupPassword'
                    }
                }
            })

            // initialization of show animations
            $.HSCore.components.HSShowAnimation.init('.js-animation-link');

            // initialization of fancybox
            $.HSCore.components.HSFancyBox.init('.js-fancybox');

            // initialization of slick carousel
            $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');

            // initialization of hamburgers
            $.HSCore.components.HSHamburgers.init('#hamburgerTrigger');

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
                beforeClose: function () {
                    $('#hamburgerTrigger').removeClass('is-active');
                },
                afterClose: function () {
                    $('#headerSidebarList .collapse.show').collapse('hide');
                }
            })

            $('#headerSidebarList [data-toggle="collapse"]').on('click', function (e) {
                e.preventDefault()

                var target = $(this).data('target')

                if ($(this).attr('aria-expanded') === "true") {
                    $(target).collapse('hide');
                } else {
                    $(target).collapse('show');
                }
            });

            // initialization of unfold component
            $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

            // initialization of select picker
            $.HSCore.components.HSSelectPicker.init('.js-select');
        })
    }, []);

    return (
        <>
            <Head>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/jquery-migrate.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
                <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.appear/0.4.1/jquery.appear.min.js"></script>
                {/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-countdown/2.1.0/js/jquery.countdown.min.js"></script> */}
                <script type="text/javascript" src="../../assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
                {/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/svg-injector/1.1.3/svg-injector.min.js"></script> */}
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.2/jquery.mCustomScrollbar.concat.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.9/typed.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
                <script type="text/javascript" src="../../assets/js/hs.core.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.countdown.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.header.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.hamburgers.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.unfold.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.focus-state.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.malihu-scrollbar.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.validation.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.fancybox.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.onscroll-animation.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.slick-carousel.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.show-animation.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.svg-injector.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.go-to.min.js"></script>
                <script type="text/javascript" src="../../assets/js/components/hs.selectpicker.min.js"></script>
            </Head>

            {/* Render Layout with Header and Footer */}
            <Header />
            {children}
            <Footer />
        </>
    )
}

export default Layout
