import React, { useEffect, useState } from "react"
import Link from 'next/link'
import { RatingView } from 'react-simple-star-rating'
import { getErrorMessage } from "../../services/errorHandle"
import { getS3Url, calcAverageRating, calcPercentage } from "../../services/helper"
import { getFilteredProducts } from "../../api/products"

const VerticalSlider = ({ params, title }) => {
    const [products, setProducts] = useState([])
    useEffect(() => {
        getFilteredProducts(params).then(
            res => setProducts(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    return (
        <div className="widget-column">
            <div className="border-bottom border-color-1 mb-5">
                <h3 className="section-title section-title__sm mb-0 pb-2 font-size-18">
                    {title}
                </h3>
            </div>
            <ul className="list-unstyled products-group">
                {
                    products.map(p => (
                        <li key={p.id} className="product-item product-item__list row no-gutters mb-6 remove-divider">
                            <div className="col-auto">
                                <Link href={`/shop/${p.slug_url}`}>
                                    <a
                                        className="d-block width-75 text-center"
                                    >
                                        {
                                            p.galleries.length > 0 && (
                                                <img
                                                    rel="preload"
                                                    className="img-fluid"
                                                    src={getS3Url(p.galleries[0].image_url)}
                                                    alt={p.title}
                                                    title={p.title}
                                                />
                                            )
                                        }
                                    </a>
                                </Link>
                            </div>
                            <div className="col pl-4 d-flex flex-column">
                                <h5 className="product-item__title mb-0">
                                    <Link href={`/shop/${p.slug_url}`}>
                                        <a
                                            className="text-blue font-weight-bold"
                                        >
                                            {p.title}
                                        </a>
                                    </Link>
                                </h5>
                                {
                                    params.is_featured || !params.on_sale && (
                                        <div className="text-warning mb-2">
                                            <RatingView size={18} fillColor="#ffc107" ratingValue={calcAverageRating(p.reviews)} />
                                        </div>
                                    )
                                }
                                <div className="prodcut-price mb-2">
                                    {
                                        !params.is_featured && (
                                            <span className="text-green font-size-14 font-weight-bold mt-1">
                                                {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                            </span>
                                        )
                                    }
                                    <div className="font-size-20 text-red">{`₹${p.selling_price}`}
                                        {
                                            !params.is_featured && (
                                                <del className="delete-price">
                                                    {`₹${p.mrp}`}
                                                </del>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))
                }
            </ul>
        </div>
    );
}

export default VerticalSlider;