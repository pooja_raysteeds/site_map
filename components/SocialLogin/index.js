import React, { useContext } from "react";
import { GoogleLogin, GoogleLogout } from "react-google-login";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { GOOGLE_CLIENT_ID } from "../../services/constant";
import { loginWithSocial } from "../../api/user";
import { getErrorMessage } from "../../services/errorHandle";
import AppContext from "../../context/AppContext";
import { useRouter } from "next/router";
import { FACEBOOK_APP_ID } from "../../services/constant";

const SocialLogins = () => {
    const { initializeUser } = useContext(AppContext);
    const router = useRouter();

    const handleSuccess = async (response) => {
        let payload;
        if (response.graphDomain == 'facebook') {
            payload = {
                id: response.id,
                name: response.name,
                email: response.email
            }
        } else {
            const { profileObj } = response;
            payload = {
                id: profileObj.googleId,
                name: `${profileObj.givenName} ${profileObj.familyName}`,
                email: profileObj.email,
            };
        }
        try {
            const res = await loginWithSocial(payload);
            initializeUser(res);
            router.push("/");
        } catch (err) {
            getErrorMessage(err);
        }
    };

    const handleError = (response) => {
        getErrorMessage(response);
    };

    return (
        <div className="d-flex">
            <FacebookLogin
                appId={FACEBOOK_APP_ID}
                render={(renderProps) => (
                    <button
                        className="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1"
                        href=""
                        onClick={renderProps.onClick}
                    >
                        <span className="fab fa-facebook-square mr-1" />
                        Facebook
                    </button>
                )}
                fields="name,email,picture"
                callback={handleSuccess}
            />
            <GoogleLogin
                render={(renderProps) => (
                    <a
                        className="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0"
                        href=""
                        onClick={renderProps.onClick}
                    >
                        <span className="fab fa-google mr-1" />
                        Google
                    </a>
                )}
                clientId={GOOGLE_CLIENT_ID}
                onSuccess={handleSuccess}
                onFailure={handleError}
                cookiePolicy={"single_host_origin"}
            />
        </div>
    );
};

export default SocialLogins;
