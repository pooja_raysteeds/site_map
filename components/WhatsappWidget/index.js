import React from 'react';
import FloatingWhatsApp from 'react-floating-whatsapp';
import Logo from "../../public/assets/img/75X75/ten-image-coloured.jpg"

const WhatsappWidget = () => {
    const Styles = {
        position: {
            bottom: "4rem",
            right: "1rem"
        },
        main: {
            zIndex: 999,
        }
    }

    return (
        <FloatingWhatsApp
            phoneNumber="919321331207"
            accountName="Support"
            avatar={Logo.src}
            statusMessage="Top Ten Electronics"
            chatMessage="Hello👋, How can we assist you?"
            placeholder="Type a message..."
            allowEsc
            notificationSound
            styles={Styles.position}
            className="WhatsappWidget-main"
        />
    );
}

export default WhatsappWidget;