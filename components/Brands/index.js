import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link'
import { getS3Url } from "../../services/helper"
import Slider from "react-slick";
import { getErrorMessage } from "../../services/errorHandle"
import { getBrands } from "../../api/products"



const Brands = () => {
    const [brands, setbrands] = useState([]);
    const brandImage = brands.filter(p => p.icon_url !== null)
    useEffect(() => {
        getBrands().then(
            res => setbrands(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        arrows: true,
        lazyLoad: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        initialSlide: 0,
        className: 'u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1',
        customPaging: function (i) {
            return (
                <span />
            );
        },
        responsive: [{
            "breakpoint": 1800,
            "settings": {
                "slidesToShow": 4
            }
        }, {
            "breakpoint": 1400,
            "settings": {
                "slidesToShow": 4
            }
        }, {
            "breakpoint": 1200,
            "settings": {
                "slidesToShow": 4
            }
        }, {
            "breakpoint": 992,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 768,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 554,
            "settings": {
                "slidesToShow": 1
            }
        }],
        dotsClass: "text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0",

    };

    return (
        <div className="brandmain">
            <div className="container brands fot ">
                <div className="mb-8">
                    <div className="position-relative">
                        <Slider {...settings}>
                            {brandImage &&
                                brandImage.map(p => (
                                    <li key={p.id} className="product-item">
                                        <div className="py-2 border-top border-bottom">
                                            <div className="js-slide imgbrand">
                                                <Link href={`/shop?brand=["${p.slug_url}"]`}>
                                                    <a>
                                                        <img
                                                            rel="preload"
                                                            className="img-fluid m-auto brands-height"
                                                            src={getS3Url(p.icon_url)}
                                                            alt="Image Branc"
                                                        />
                                                    </a>
                                                </Link>
                                            </div>
                                        </div>
                                    </li>
                                ))
                            }
                        </Slider>
                    </div>
                </div>
            </div>
        </div >

    )
}

export default Brands;
