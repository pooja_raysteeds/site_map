import React, { useContext } from "react";
import AppContext from "../../context/AppContext"
import Link from 'next/link'
import { getS3Url } from "../../services/helper"
import Sliders from "react-slick"
import "react-responsive-carousel/lib/styles/carousel.min.css"

const Slider = ({ data }) => {
    if (data.length === 0) return null

    const { isMobileView } = useContext(AppContext)
    const settings = {
        dots: true,
        infinite: true,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 4000,
        className: 'u-slick position-static overflow-hidden u-slick-overflow-visble pb-7',
        customPaging: function (i) {
            return (
                <span />
            );
        },
        dotsClass: "text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3  mt-md-1",
    };

    return (
        <div className="home-b">
            <Sliders {...settings}>
                {
                    data.map((item, i) => {
                        return (
                            <div key={i}>
                                <Link href={item.page_url}>
                                    <a className="d-block carousel">
                                        <img
                                            rel="preload"
                                            src={isMobileView ? getS3Url(item.mob_image_url) : getS3Url(item.image_url)}
                                            alt={item.title}
                                            title={item.title}
                                        />
                                    </a>
                                </Link>
                            </div>
                        )
                    })
                }
            </Sliders>
        </div>
    )
}

export default Slider