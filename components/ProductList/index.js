import React, { useEffect, useState, useContext } from "react";
import Link from "next/link";
import { RatingView } from 'react-simple-star-rating'
import Carousel from "../../components/Carousel";
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { getErrorMessage } from "../../services/errorHandle";
import { calcAverageRating, getS3Url, calcPercentage, getLocalStorage } from "../../services/helper"
import { getFilteredProducts } from "../../api/products";
import { getPromoByType } from "../../api/promo";
import AppContext from "../../context/AppContext"
import _ from "lodash";
import Styles from "./productlist.module.css"
import { useRouter } from 'next/router'

const ProductList = ({ value, categoryTitle, priceFilter, productIds, setToggleMobFiler, filterFlag }) => {
    const [products, setProducts] = useState([]);
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(20)
    const [params, setParams] = useState({ ...value, page_size: pageSize, page })
    const [pages, setPages] = useState(0)
    const [promoBanners, setPromoBanners] = useState([]);
    const userId = getLocalStorage("usrid")
    const cartValue = getLocalStorage("crt")
    const wishlistValue = getLocalStorage("wstl")
    const userCompares = getLocalStorage("usrCmpr");
    const { addProductToCart, addProductToWishlist, removeProductToWishlist, addToCompare } = useContext(AppContext);
    const option = [
        { value: "Sort By" },
        { value: "Sort by rating" },
        { value: "Sort by latest" },
        { value: "Sort by price: low to high" },
        { value: "Sort by price: high to low" },
    ]
    const initialPaging = {
        first: 1,
        second: 2,
        third: 3
    }
    const [crntSort, setCrntSort] = useState(option[0].value)
    const [pgnPages, setPgnPages] = useState(initialPaging)
    const router = useRouter()
    if (value &&
        (value.category_url != params.category_url) ||
        (value.parent_url != params.parent_url) ||
        (value.brand_url != params.brand_url) ||
        (value.sub_category != params.sub_category) ||
        (value.search != params.search) ||
        (value.filter_option != params.filter_option) ||
        (value.availability != params.availability) ||
        (value.min_price != params.min_price) ||
        (value.max_price != params.max_price)
    ) {
        setParams({ ...value, page_size: pageSize, page })
    }

    useEffect(() => {
        getFilteredProducts(params)
            .then((res) => {
                const { result, pages } = res
                setProducts(result)
                setPages(pages)
                const ids = result.map(p => p.id)
                productIds(ids)
                const sorted = result.sort(
                    (a, b) => a.selling_price - b.selling_price
                );
                const min = parseInt(sorted[0].selling_price, 10);
                const max = parseInt(sorted[sorted.length - 1].selling_price, 10);
                priceFilter({ min, max })
            })
            .catch((err) => getErrorMessage(err));

        const promoType = value.category_url || value.parent_url || value.sub_category
        getPromoByType(promoType)
            .then((res) => setPromoBanners(res))
            .catch((err) => getErrorMessage(err));
    }, [params, filterFlag]);

    const handleCart = (product_id) => {
        const params = { product_id, quantity: 1 }
        addProductToCart(params)
    }

    const handleWishlist = (e, product_id) => {
        e.preventDefault()
        addProductToWishlist(product_id)
    }

    const removeWishlist = (e, product_id) => {
        e.preventDefault()
        removeProductToWishlist(product_id);
    }

    const handleCompare = (id) => {
        addToCompare(id)
        router.push('/compare')
    }

    const handleSort = async (e) => {
        const option = e.target.value;
        setCrntSort(option)
        if (option === "Sort by rating") {
            setParams({ ...value, top_rated: true, page_size: pageSize, page })
        }
        if (option === "Sort by latest") {
            setParams({ ...value, sort: "created_at", page_size: pageSize, page })
        }
        if (option === "Sort by price: low to high") {
            setParams({ ...value, sort: "mrp", page_size: pageSize, page })
        }
        if (option === "Sort by price: high to low") {
            setParams({ ...value, sort: "-mrp", page_size: pageSize, page })
        }
        if (option === "Sort By") {
            setParams({ ...value, is_featured: true, page_size: pageSize, page })
        }
    }

    const handleShow = (e) => {
        const { value } = e.target
        if (value === "one") {
            setParams((prevState) => ({
                ...prevState,
                page_size: 20,
                page: 1
            }))
            setPageSize(20)
        }
        if (value === "two") {
            setParams((prevState) => ({
                ...prevState,
                page_size: 40,
                page: 1
            }))
            setPageSize(40)
        }
        setPage(1)
        setPgnPages(initialPaging)
    }

    const handlePagination = (e, index, btn) => {
        if (index > pages) return
        if (e) {
            if (page === 1 && btn === 0) return;
            if (page === pages && btn == 1) return;

            if (page - pgnPages.third === 0 && btn === 1) {
                const updated = { ...pgnPages, first: pgnPages.first + 1, second: pgnPages.second + 1, third: pgnPages.third + 1 }
                setPgnPages(updated)
            }
            if (page - pgnPages.first === 0 && btn === 0) {
                const updated = { ...pgnPages, first: pgnPages.first - 1, second: pgnPages.second - 1, third: pgnPages.third - 1 }
                setPgnPages(updated)
            }
            if (btn === 0) {
                setParams((prevState) => ({
                    ...prevState,
                    page: index - 1
                }))
                setPage(index - 1)
            }
            if (btn === 1) {
                setParams((prevState) => ({
                    ...prevState,
                    page: index + 1
                }))
                setPage(index + 1)
            }
        } else {
            setParams((prevState) => ({
                ...prevState,
                page: index
            }))
            setPage(index)
        }
    }

    return (
        <>
            <div className="mb-2 d-xl-block shopBanners">
                <Carousel data={promoBanners} />
            </div>
            <div className="flex-center-between mb-3">
                <h3 className="font-size-25 mb-0">Shop
                    <h1 className="text-red font-size-15 bg-gray-1 borders-radius-9 p-1">
                        {categoryTitle}
                    </h1>
                </h3>

                <p className="font-size-14 text-gray-90 mb-0">
                    Showing {products.length} results
                </p>
            </div>
            {
                products.length === 0 ? (
                    <div className="bg-gray-1 flex-center-between borders-radius-9 py-1 pt-2 pb-2">
                        <span className="ml-3">No Product Listed</span>
                    </div>
                ) : (
                    <>
                        <div className="bg-gray-1 flex-center-between borders-radius-9 py-1 pt-2 pb-2">
                            <div className="d-xl-none">
                                <a
                                    id="sidebarNavToggler1"
                                    className="btn btn-sm py-1 font-weight-normal"
                                    role="button"
                                    aria-controls="sidebarContent1"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                    data-unfold-event="click"
                                    data-unfold-hide-on-scroll="false"
                                    data-unfold-target="#sidebarContent1"
                                    data-unfold-type="css-animation"
                                    data-unfold-animation-in="fadeInLeft"
                                    data-unfold-animation-out="fadeOutLeft"
                                    data-unfold-duration={500}
                                    onClick={() => setToggleMobFiler((prevState) => !prevState)}
                                >
                                    <i className="fas fa-sliders-h" />{" "}
                                    <span className="ml-1">Filters</span>
                                </a>
                            </div>
                            <div className="px-3 d-none d-xl-block">
                                <ul
                                    className="nav nav-tab-shop"
                                    id="pills-tab"
                                    role="tablist"
                                >
                                    <li className="nav-item">
                                        <a
                                            className="nav-link active"
                                            id="pills-one-example1-tab"
                                            data-toggle="pill"
                                            href="#pills-one-example1"
                                            role="tab"
                                            aria-controls="pills-one-example1"
                                            aria-selected="false"
                                        >
                                            <div className="d-md-flex justify-content-md-center align-items-md-center">
                                                <i className="fa fa-th" />
                                            </div>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a
                                            className="nav-link"
                                            id="pills-two-example1-tab"
                                            data-toggle="pill"
                                            href="#pills-two-example1"
                                            role="tab"
                                            aria-controls="pills-two-example1"
                                            aria-selected="false"
                                        >
                                            <div className="d-md-flex justify-content-md-center align-items-md-center">
                                                <i className="fa fa-align-justify" />
                                            </div>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a
                                            className="nav-link"
                                            id="pills-three-example1-tab"
                                            data-toggle="pill"
                                            href="#pills-three-example1"
                                            role="tab"
                                            aria-controls="pills-three-example1"
                                            aria-selected="true"
                                        >
                                            <div className="d-md-flex justify-content-md-center align-items-md-center">
                                                <i className="fa fa-list" />
                                            </div>
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a
                                            className="nav-link"
                                            id="pills-four-example1-tab"
                                            data-toggle="pill"
                                            href="#pills-four-example1"
                                            role="tab"
                                            aria-controls="pills-four-example1"
                                            aria-selected="true"
                                        >
                                            <div className="d-md-flex justify-content-md-center align-items-md-center">
                                                <i className="fa fa-th-list" />
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="d-flex" style={{ margin: "auto" }}>
                                <form>
                                    <div tabIndex="0"
                                        onKeyDown={e => {
                                            if (e.key === "ArrowUp" || e.key === "ArrowDown") {
                                                e.preventDefault();
                                                return false;
                                            }
                                        }}
                                    >
                                        <select
                                            value={crntSort}
                                            onChange={(e) => handleSort(e)}
                                            className={`text-gray-20 ${Styles.selectMain}`}
                                        >
                                            {
                                                option.map((item, index) => (
                                                    <option key={index} value={item.value} selected>
                                                        {item.value}
                                                    </option>
                                                ))
                                            }

                                        </select>
                                    </div>
                                </form>
                                <form className="ml-2 pr-4 d-none d-xl-block">
                                    <div tabIndex="0"
                                        onKeyDown={e => {
                                            if (e.key === "ArrowUp" || e.key === "ArrowDown") {
                                                e.preventDefault();
                                                return false;
                                            }
                                        }}
                                    >  <select
                                        onChange={(e) => handleShow(e)}
                                        className={`text-gray-20 ${Styles.selectMain}`}
                                    >
                                            <option style={{ padding: "10px" }} value="one" selected>Show 20</option>
                                            <option value="two">Show 40</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            {
                                pages !== 1 && (
                                    <nav
                                        className="d-none d-md-flex justify-content-between align-items-center"
                                        aria-label="Page navigation example"
                                    >
                                        <button className="btn btn-primary-dark-w px-5" onClick={(e) => handlePagination(1, page, 0)}> Prev </button>
                                        <ul className="pagination mb-0 pagination-shop justify-content-center justify-content-md-start">
                                            {
                                                pages >= 1 && (
                                                    <li className="page-item">
                                                        <a className={`page-link ${page === pgnPages.first ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.first)}>
                                                            {pgnPages.first}
                                                        </a>
                                                    </li>
                                                )
                                            }
                                            {
                                                pages >= 2 && (
                                                    <li className="page-item">
                                                        <a className={`page-link ${page === pgnPages.second ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.second)}>
                                                            {pgnPages.second}
                                                        </a>
                                                    </li>
                                                )
                                            }
                                            {
                                                pages >= 3 && (
                                                    <li className="page-item">
                                                        <a className={`page-link ${page === pgnPages.third ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.third)}>
                                                            {pgnPages.third}
                                                        </a>
                                                    </li>
                                                )
                                            }
                                        </ul>
                                        <button className="btn btn-primary-dark-w px-5" onClick={(e) => handlePagination(1, page, 1)}> Next </button>
                                    </nav>
                                )
                            }
                        </div>

                        <div className="tab-content" id="pills-tabContent">
                            {/* Tab One */}
                            <div
                                className="tab-pane fade pt-2 show active"
                                id="pills-one-example1"
                                role="tabpanel"
                                aria-labelledby="pills-one-example1-tab"
                                data-target-group="groups"
                            >
                                <ul className="row list-unstyled products-group no-gutters">
                                    {products.map((p) => (
                                        <li
                                            key={p.id}
                                            className="col-6 col-md-3 product-item"
                                        >
                                            <div className="product-item__outer h-100 w-100">
                                                <div className="product-item__inner px-xl-4 p-3">
                                                    <div className="product-item__body pb-xl-2">
                                                        <div className="mb-2">
                                                            <Link
                                                                href={`/shop/${p.slug_url}`}
                                                                key={p.id}
                                                            >
                                                                <a className="font-size-12 text-gray-5">
                                                                    {p.category.title}
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <h2 className="mb-1 product-item__title mobiletexttruncate">
                                                            <Link
                                                                href={`/shop/${p.slug_url}`}
                                                                key={p.id}
                                                            >
                                                                <a className="text-blue font-weight-bold">
                                                                    {p.title}
                                                                </a>
                                                            </Link>
                                                        </h2>
                                                        <div className="mb-2">
                                                            {p.stock_quantity && p.stock_quantity ? (
                                                                <Link
                                                                    href={`/shop/${p.slug_url}`}
                                                                    key={p.id}
                                                                >
                                                                    <a className="d-block text-center">
                                                                        {
                                                                            p.galleries.length > 0 && (
                                                                                <img
                                                                                    rel="preload"
                                                                                    className="img-fluid"
                                                                                    src={getS3Url(p.galleries[0].image_url)}
                                                                                    alt={p.title}
                                                                                    title={p.title}
                                                                                />
                                                                            )
                                                                        }
                                                                    </a>
                                                                </Link>) : (
                                                                <Link
                                                                    href={`/shop/${p.slug_url}`}
                                                                    key={p.id}
                                                                >
                                                                    <a className="d-block text-center">
                                                                        {

                                                                            <div className="outofstock">
                                                                                <img
                                                                                    rel="preload"
                                                                                    className="img-fluid ousimage"
                                                                                    src={getS3Url(p.galleries[0].image_url)}
                                                                                    alt={p.title}
                                                                                    title={p.title}
                                                                                />
                                                                                <div className="middlelist">
                                                                                    <div className="textlist">Out of stock</div>
                                                                                </div>
                                                                            </div>

                                                                        }
                                                                    </a>
                                                                </Link>
                                                            )}
                                                        </div>
                                                        <span className="text-green font-size-15 font-weight-bold mt-1">
                                                            {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                                        </span>
                                                        <div className="flex-center-between mb-1">
                                                            <div className="prodcut-price d-flex">
                                                                <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                                                                <del className="delete-price mt-0">
                                                                    {`₹${p.mrp}`}
                                                                </del>

                                                            </div>
                                                            {p.stock_quantity && p.stock_quantity ? (<div className="d-none d-xl-block prodcut-add-cart">
                                                                {
                                                                    cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                                                        <Link href="/cart">
                                                                            <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                                                                <i className="ec ec-shopping-bag" />
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/cart">
                                                                            <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                                                                <i className="ec ec-add-to-cart" />
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                }
                                                            </div>) : (<></>)}

                                                        </div>
                                                    </div>
                                                    <div className="product-item__footer">
                                                        <div className="border-top pt-2 flex-center-between flex-wrap">
                                                            {
                                                                userCompares && (
                                                                    userCompares.filter(item => item == p.id).length > 0 ? (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Go to Compare
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Compare
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                )
                                                            }
                                                            {
                                                                userId ? (
                                                                    <Link href="/wishlist">
                                                                        {
                                                                            wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                                                                <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                                                                    Remove from Wishlist
                                                                                </a>
                                                                            ) : (
                                                                                <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                                    Wishlist
                                                                                </a>
                                                                            )
                                                                        }
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/login">
                                                                        <a className="text-gray-6 font-size-13 mr-2">
                                                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                            Wishlist
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            {/* Tab Two */}
                            <div
                                className="tab-pane fade pt-2"
                                id="pills-two-example1"
                                role="tabpanel"
                                aria-labelledby="pills-two-example1-tab"
                                data-target-group="groups"
                            >
                                <ul className="row list-unstyled products-group no-gutters">
                                    {products.map((p) => (
                                        <li
                                            key={p.id}
                                            className="col-6 col-md-3 product-item"
                                        >
                                            <div className="product-item__outer h-100">
                                                <div className="product-item__inner px-xl-4 p-3">
                                                    <div className="product-item__body pb-xl-2">
                                                        <div className="mb-2">
                                                            <Link href={`/shop/${p.slug_url}`}>
                                                                <a className="font-size-12 text-gray-5">
                                                                    {p.category.title}
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <h2 className="mb-1 product-item__title">
                                                            <Link href={`/shop/${p.slug_url}`}>
                                                                <a className="text-blue font-weight-bold">
                                                                    {p.title}
                                                                </a>
                                                            </Link>
                                                        </h2>
                                                        <div className="mb-2">
                                                            <Link
                                                                href={"/shop/" + p.id}
                                                                key={p.id}
                                                            >
                                                                <a className="d-block text-center">
                                                                    {
                                                                        p.galleries.length > 0 && (
                                                                            <img
                                                                                rel="preload"
                                                                                className="img-fluid"
                                                                                src={getS3Url(p.galleries[0].image_url)}
                                                                                alt={p.title}
                                                                                title={p.title}
                                                                            />
                                                                        )
                                                                    }
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <div className="mb-3">
                                                            <a
                                                                className="d-inline-flex align-items-center small font-size-14"
                                                                href="#"
                                                            >
                                                                <div className="text-warning mr-2">
                                                                    <RatingView fillColor="#ffc107" ratingValue={calcAverageRating(p.reviews)} />
                                                                </div>
                                                                <span className="text-secondary">
                                                                    {p.reviews.length}
                                                                </span>
                                                            </a>
                                                        </div>
                                                        <div className="text-gray-20 mb-2 font-size-12">
                                                            SKU: {p.sku}
                                                        </div>
                                                        <span className="text-green font-size-15 font-weight-bold mt-1">
                                                            {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                                        </span>
                                                        <div className="flex-center-between mb-1">
                                                            <div className="prodcut-price d-flex">
                                                                <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                                                                <del className="delete-price mt-1">
                                                                    {`₹${p.mrp}`}
                                                                </del>
                                                            </div>
                                                            <div className="d-none d-xl-block prodcut-add-cart">
                                                                {
                                                                    cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                                                        <Link href="/cart">
                                                                            <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                                                                <i className="ec ec-shopping-bag" />
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/cart">
                                                                            <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                                                                <i className="ec ec-add-to-cart" />
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="product-item__footer">
                                                        <div className="border-top pt-2 flex-center-between flex-wrap">
                                                            {
                                                                userCompares && (
                                                                    userCompares.filter(item => item == p.id).length > 0 ? (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Go to Compare
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Compare
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                )
                                                            }
                                                            {
                                                                userId ? (
                                                                    <Link href="/wishlist">
                                                                        {
                                                                            wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                                                                <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                                                                    Remove from Wishlist
                                                                                </a>
                                                                            ) : (
                                                                                <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                                    Wishlist
                                                                                </a>
                                                                            )
                                                                        }
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/login">
                                                                        <a className="text-gray-6 font-size-13 mr-2">
                                                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                            Wishlist
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            {/* Tab Three */}
                            <div
                                className="tab-pane fade pt-2"
                                id="pills-three-example1"
                                role="tabpanel"
                                aria-labelledby="pills-three-example1-tab"
                                data-target-group="groups"
                            >
                                <ul className="d-block list-unstyled products-group prodcut-list-view">
                                    {products.map((p) => (
                                        <li
                                            key={p.id}
                                            className="product-item remove-divider"
                                        >
                                            <div className="product-item__outer w-100">
                                                <div className="product-item__inner remove-prodcut-hover py-4 row">
                                                    <div className="product-item__header col-6 col-md-4">
                                                        <div className="mb-2">
                                                            <Link
                                                                href={"/shop/" + p.id}
                                                                key={p.id}
                                                            >
                                                                <a className="d-block text-center">
                                                                    {
                                                                        p.galleries.length > 0 && (
                                                                            <img
                                                                                rel="preload"
                                                                                className="img-fluid"
                                                                                src={getS3Url(p.galleries[0].image_url)}
                                                                                alt={p.title}
                                                                                title={p.title}
                                                                            />
                                                                        )
                                                                    }
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                    <div className="product-item__body col-6 col-md-5">
                                                        <div className="pr-lg-10">
                                                            <div className="mb-2">
                                                                <Link href={`/shop/${p.slug_url}`}>
                                                                    <a className="font-size-12 text-gray-5">
                                                                        {p.category.title}
                                                                    </a>
                                                                </Link>
                                                            </div>
                                                            <h2 className="mb-2 product-item__title">
                                                                <Link href={`/shop/${p.slug_url}`}>
                                                                    <a className="text-blue font-weight-bold">
                                                                        {p.title}
                                                                    </a>
                                                                </Link>
                                                            </h2>
                                                            <div className="mb-3 d-none d-md-block">
                                                                <a
                                                                    className="d-inline-flex align-items-center small font-size-14"
                                                                    href="#"
                                                                >
                                                                    <div className="text-warning mr-2">
                                                                        <RatingView fillColor="#ffc107" ratingValue={calcAverageRating(p.reviews)} />
                                                                    </div>
                                                                    <span className="text-secondary">
                                                                        {p.reviews.length}
                                                                    </span>
                                                                </a>
                                                            </div>
                                                            <ul className="font-size-12 p-0 text-gray-110 mb-4 d-none d-md-block">
                                                                <ReactMarkdown children={p.highlights} rehypePlugins={[rehypeRaw]} />
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="product-item__footer col-md-3 d-md-block">
                                                        <div className="mb-3">

                                                            <div className="prodcut-price d-flex">
                                                                <div className="text-gray-100 text-red mr-1">
                                                                    {`₹${p.selling_price}`}
                                                                </div>
                                                                <del className="delete-price mr-2 mt-1">
                                                                    {`₹${p.mrp}`}
                                                                </del>
                                                            </div>
                                                            <span className="text-green font-size-15 font-weight-bold mt-1">
                                                                {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                                            </span>
                                                            <div className="prodcut-add-cart mt-2">
                                                                {
                                                                    cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                                                        <Link href="/cart">
                                                                            <a className="btn btn-sm btn-block btn-primary-dark btn-wide transition-3d-hover" title="Go to Cart">
                                                                                Go to Cart
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/cart">
                                                                            <a onClick={() => handleCart(p.id)} className="btn btn-sm btn-block btn-primary-dark btn-wide transition-3d-hover" title="Add to Cart">
                                                                                Add to Cart
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap">
                                                            {
                                                                userCompares && (
                                                                    userCompares.filter(item => item == p.id).length > 0 ? (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Go to Compare
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Compare
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                )
                                                            }
                                                            {
                                                                userId ? (
                                                                    <Link href="/wishlist">
                                                                        {
                                                                            wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                                                                <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13 mx-wd-3">
                                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                                                                    Remove from Wishlist
                                                                                </a>
                                                                            ) : (
                                                                                <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13 mx-wd-3">
                                                                                    <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                                    Wishlist
                                                                                </a>
                                                                            )
                                                                        }
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/login">
                                                                        <a className="text-gray-6 font-size-13 mx-wd-3">
                                                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                            Wishlist
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </div>

                            {/* Tab Four */}
                            <div
                                className="tab-pane fade pt-2"
                                id="pills-four-example1"
                                role="tabpanel"
                                aria-labelledby="pills-four-example1-tab"
                                data-target-group="groups"
                            >
                                <ul className="d-block list-unstyled products-group prodcut-list-view-small">
                                    {products.map((p) => (
                                        <li
                                            key={p.id}
                                            className="product-item remove-divider"
                                        >
                                            <div className="product-item__outer w-100">
                                                <div className="product-item__inner remove-prodcut-hover py-4 row">
                                                    <div className="product-item__header col-6 col-md-2">
                                                        <div className="mb-2">
                                                            <Link
                                                                href={`/shop/${p.slug_url}`}
                                                                key={p.id}
                                                            >
                                                                <a className="d-block text-center">
                                                                    {
                                                                        p.galleries.length > 0 && (
                                                                            <img
                                                                                rel="preload"
                                                                                className="img-fluid"
                                                                                src={getS3Url(p.galleries[0].image_url)}
                                                                                alt={p.title}
                                                                                title={p.title}
                                                                            />
                                                                        )
                                                                    }
                                                                </a>
                                                            </Link>
                                                        </div>
                                                    </div>
                                                    <div className="product-item__body col-6 col-md-7">
                                                        <div className="pr-lg-10">
                                                            <div className="mb-2">
                                                                <Link href={`/shop/${p.slug_url}`}>
                                                                    <a className="font-size-12 text-gray-5">
                                                                        {p.category.title}
                                                                    </a>
                                                                </Link>
                                                            </div>
                                                            <h2 className="mb-2 product-item__title">
                                                                <Link href={`/shop/${p.slug_url}`}>
                                                                    <a className="text-blue font-weight-bold">
                                                                        {p.title}
                                                                    </a>
                                                                </Link>
                                                            </h2>
                                                            <div className="prodcut-price d-md-none">
                                                                <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>

                                                            </div>
                                                            <ul className="font-size-12 p-0 text-gray-110 mb-4 d-none d-md-block">
                                                                <ReactMarkdown children={p.highlights} rehypePlugins={[rehypeRaw]} />
                                                            </ul>
                                                            <div className="mb-3 d-none d-md-block">
                                                                <a
                                                                    className="d-inline-flex align-items-center small font-size-14"
                                                                    href="#"
                                                                >
                                                                    <div className="text-warning mr-2">
                                                                        <RatingView fillColor="#ffc107" ratingValue={calcAverageRating(p.reviews)} />
                                                                    </div>
                                                                    <span className="text-secondary">
                                                                        {p.reviews.length}
                                                                    </span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="product-item__footer col-md-3 d-md-block">
                                                        <span className="text-green font-size-15 font-weight-bold mt-1">
                                                            {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                                        </span>
                                                        <div className="flex-center-between mb-3">
                                                            <div className="prodcut-price d-flex">
                                                                <div className="text-gray-100 text-red">
                                                                    {`₹${p.selling_price}`}
                                                                </div>
                                                                <del className="delete-price mt-1">
                                                                    {`₹${p.mrp}`}
                                                                </del>
                                                            </div>
                                                            <div className="d-none d-xl-block prodcut-add-cart mt-2">
                                                                {
                                                                    cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                                                        <Link href="/cart">
                                                                            <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                                                                <i className="ec ec-shopping-bag" />
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/cart">
                                                                            <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                                                                <i className="ec ec-add-to-cart" />
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                }
                                                            </div>
                                                        </div>
                                                        <div className="flex-horizontal-center justify-content-between justify-content-wd-center flex-wrap border-top pt-3">
                                                            {
                                                                userCompares && (
                                                                    userCompares.filter(item => item == p.id).length > 0 ? (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Go to Compare
                                                                            </a>
                                                                        </Link>
                                                                    ) : (
                                                                        <Link href="/compare">
                                                                            <a onClick={() => handleCompare(p.id)} className="text-gray-6 font-size-13">
                                                                                <i className="ec ec-compare mr-1 font-size-15" />{" "}
                                                                                Compare
                                                                            </a>
                                                                        </Link>
                                                                    )
                                                                )
                                                            }
                                                            {
                                                                userId ? (
                                                                    <Link href="/wishlist">
                                                                        {
                                                                            wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                                                                <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13 mx-wd-3">
                                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                                                                    Remove from Wishlist
                                                                                </a>
                                                                            ) : (
                                                                                <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13 mx-wd-3">
                                                                                    <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                                    Wishlist
                                                                                </a>
                                                                            )
                                                                        }
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/login">
                                                                        <a className="text-gray-6 font-size-13 mx-wd-3">
                                                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                            Wishlist
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>

                    </>
                )
            }

            {/* Start Pagination */}
            <nav
                className="d-md-flex justify-content-between align-items-center border-top pt-3"
                aria-label="Page navigation example"
            >
                <div className="text-center text-md-left mb-3 mb-md-0">
                    Showing {products.length} results
                </div>
                {
                    pages !== 1 && (
                        <div className="d-flex justify-content-between align-items-center">
                            <button className="btn btn-primary-dark-w px-5" onClick={(e) => handlePagination(1, page, 0)}> Prev </button>
                            <ul className="pagination mb-0 pagination-shop justify-content-center justify-content-md-start">
                                <ul className="pagination mb-0 pagination-shop justify-content-center justify-content-md-start">
                                    {
                                        pages >= 1 && (
                                            <li className="page-item">
                                                <a className={`page-link ${page === pgnPages.first ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.first)}>
                                                    {pgnPages.first}
                                                </a>
                                            </li>
                                        )
                                    }
                                    {
                                        pages >= 2 && (
                                            <li className="page-item">
                                                <a className={`page-link ${page === pgnPages.second ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.second)}>
                                                    {pgnPages.second}
                                                </a>
                                            </li>
                                        )
                                    }
                                    {
                                        pages >= 3 && (
                                            <li className="page-item">
                                                <a className={`page-link ${page === pgnPages.third ? 'current' : ''}`} onClick={(e) => handlePagination(0, pgnPages.third)}>
                                                    {pgnPages.third}
                                                </a>
                                            </li>
                                        )
                                    }
                                </ul>
                            </ul>
                            <button className="btn btn-primary-dark-w px-5" onClick={(e) => handlePagination(1, page, 1)}> Next </button>
                        </div>
                    )
                }
            </nav>
        </>
    );
};

export default ProductList;
