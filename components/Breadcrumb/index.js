import Link from "next/link";

const Breadcrumb = ({ data }) => {
  return (
    <>
      <div className="bg-gray-13 bg-md-transparent">
        <div className="container">
          <div className="my-md-3">
            <nav aria-label="breadcrumb">
              <ol className="breadcrumb mb-3 flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble">
                <li className="breadcrumb-item flex-shrink-0 flex-xl-shrink-1">
                  <Link href="/">
                    <a>Home</a>
                  </Link>
                </li>
                {data.map((x, index) => (
                  <li
                    key={index}
                    className={`breadcrumb-item flex-shrink-0 flex-xl-shrink-1 ${x.link ? '' : 'active'}`}
                    aria-current="page"
                  >
                    {x.link ? (<Link href={x.link ? x.link : ""}>
                      <a>{x.title}</a>
                    </Link>) : (<>{x.title}</>)}
                  </li>))}
              </ol>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
};
export default Breadcrumb;
