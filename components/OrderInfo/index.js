import React from 'react';
import Link from 'next/link'
import Head from 'next/head'
import dateFormat from "dateformat";

const OrderInfo = ({ order }) => {
    if (!order) return;
    return (
        <>
            <div className="container px-1 px-md-4 mx-auto order-info">
                <div className="card mt-1">
                    <div className="row d-flex justify-content-between px-3 top">
                        <div className="d-flex p-2">
                            <h5>ORDER: <span className="text-primary font-weight-bold">#{order.order_number}</span></h5>
                        </div>
                        <div className="d-flex flex-column text-sm-right p-2">
                            <p className="mb-0">Expected Arrival: <span>{dateFormat(order.expected_delivery_date, "fullDate")}</span></p>
                            <p className="mb-0">Order Status: <span className="text-primary font-weight-bold">{order.order_status.status}</span></p>
                            <p className="mb-0">Total: <span className="text-primary font-weight-bold">₹{order.total_amount}</span></p>
                            <p>Transaction Type:<b> {order.payment_method_title}</b></p>

                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="row border rounded p-1 my-3">
                            <div class="col-md-6 py-3">
                                <div class="d-flex flex-column align-items start"> <b>Billing Address</b>
                                    <p class="text-justify pt-2">{order.billing_address.billing_first_name} {order.billing_address.billing_last_name}, {order.billing_address.billing_street_address},{order.billing_address.billing_apt}<br />{order.billing_address.billing_postal_code}</p>
                                    <p class="text-justify">{order.billing_address.billing_city}</p>
                                </div>
                            </div>
                            <div class="col-md-6 py-3">
                                <div class="d-flex flex-column align-items start"> <b>Shipping Address</b>
                                    <p class="text-justify pt-2">{order.shipping_address.shipping_first_name} {order.shipping_address.shipping_last_name}, {order.shipping_address.shipping_street_address},{order.shipping_address.shipping_apt}<br />{order.shipping_address.shipping_postal_code},</p>
                                    <p class="text-justify">{order.shipping_address.shipping_city}</p>
                                </div>
                            </div>
                        </div>
                        <div class="pl-3 font-weight-bold">Products</div>
                        <div class="d-sm-flex justify-content-between rounded my-3 subscriptions">
                            <table class="table">
                                <thead>
                                    {order.products.map(p => (
                                        <tr>
                                            <th scope="col font-weight-normal">{p.product.title}</th>
                                            <th scope="col">Quantity: <b>{p.quantity}</b></th>
                                            <th scope="col">Price:<b>{p.product.selling_price}</b></th>
                                            {
                                                p.product.exchangeProduct && (
                                                    <th>Exchange Product: <b>{p.product.exchangeProduct.item}</b></th>
                                                )
                                            }
                                            {
                                                p.product.warranty && (
                                                    <th>Extended Warranty: <b className="text-green">{p.product.warranty.warranty}</b></th>
                                                )
                                            }

                                        </tr>
                                    ))}
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="mb-8 pb-0dot5 d-flex justify-content-center">
                <Link href="/">
                    <a className="btn btn-block btn-primary-dark continue-shopping">
                        Continue Shopping
                    </a>
                </Link>
            </div>
        </>
    )
}

export default OrderInfo;
