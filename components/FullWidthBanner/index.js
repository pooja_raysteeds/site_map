import React, { useContext } from "react";
import AppContext from "../../context/AppContext"
import { getS3Url } from "../../services/helper"
import Link from 'next/link'

const FullWidthBanner = ({ data }) => {
    if (data.length === 0) return null;

    const { isMobileView } = useContext(AppContext)
    const banner = data[0];
    return (
        <div className="container">
            <div className="mb-8">
                <Link href={banner.page_url}>
                    <a className="d-block text-gray-90">
                        <img
                            rel="preload"
                            src={isMobileView ? getS3Url(banner.mob_image_url) : getS3Url(banner.image_url)}
                            className="bg-img-hero pt-3 banner-pic"
                            style={{ width: "100%" }}
                            title={banner.title}
                        >
                        </img>
                    </a>
                </Link>
            </div>
        </div>
    );
}

export default FullWidthBanner;