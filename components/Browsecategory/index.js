import React, { useEffect, useState } from "react"
import Link from 'next/link'
import { getErrorMessage } from "../../services/errorHandle"
import { getCategoryHierarchy } from "../../api/category"

const BrowseCategory = ({ isMobileView, setToggleMobFiler}) => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        getCategoryHierarchy(0).then(
            res => setCategories(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    const handleClick = () => {
        if (isMobileView) return setToggleMobFiler((prevState) => !prevState)
    }

    return (
        <>
            <ul
                id="sidebarNav"
                className="list-unstyled mb-0 sidebar-navbar view-all"
            >
                <li>
                    <div className="dropdown-title">Browse Categories</div>
                </li>
                {
                    categories.map((category, index) => (
                        <li key={category.id}>
                            <Link href={`/shop?parent=${category.slug_url}`}>
                                <a
                                    className="dropdown-toggle dropdown-toggle-collapse"
                                    role="button"
                                    data-toggle="collapse"
                                    aria-expanded="false"
                                    aria-controls={`sidebarNav${index}Collapse`}
                                    data-target={`#sidebarNav${index}Collapse`}
                                >
                                    {category.title}
                                    {/* <span className="text-gray-25 font-size-12 font-weight-normal">
                                        {" "}
                                        (56)
                                    </span> */}
                                </a>
                            </Link>
                            <div
                                id={`sidebarNav${index}Collapse`}
                                className="collapse"
                                data-parent="#sidebarNav"
                            >
                                <ul id={`sidebarNav${index}`} className="list-unstyled dropdown-list">
                                    {
                                        category.children.map(child => (
                                            <li key={child.id}>
                                                <Link href={`/shop?category=${child.slug_url}`}>
                                                    <a className="dropdown-item" onClick={handleClick}>
                                                        {child.title}
                                                        {/* <span className="text-gray-25 font-size-12 font-weight-normal">
                                                            {" "}
                                                            (56)
                                                        </span> */}
                                                    </a>
                                                </Link>
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>
                        </li>
                    ))
                }
            </ul>
        </>
    )
}

export default BrowseCategory
