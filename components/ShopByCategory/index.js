import React, { useEffect, useState } from "react"
import Link from 'next/link'
import { getErrorMessage } from "../../services/errorHandle"
import { getS3Url } from "../../services/helper";
import { getCategoriesByIDs } from "../../api/category"
import {
    CATEGORY_TYPE_1,
    CATEGORY_TYPE_2,
    CATEGORY_TYPE_3,
    CATEGORY_TYPE_4,
    CATEGORY_TYPE_5,
    CATEGORY_TYPE_6,
    CATEGORY_TYPE_7,
    CATEGORY_TYPE_8
} from "../../services/constant";

const ShopByCategory = () => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
        const params = {
            ids: [
                CATEGORY_TYPE_1,
                CATEGORY_TYPE_2,
                CATEGORY_TYPE_3,
                CATEGORY_TYPE_4,
                CATEGORY_TYPE_5,
                CATEGORY_TYPE_6,
                CATEGORY_TYPE_7,
                CATEGORY_TYPE_8
            ]
        };
        getCategoriesByIDs({ params }).then(
            res => setCategories(res)
        ).catch(
            err => getErrorMessage(err)
        )
    }, []);

    return (
        <div className="container">
            <div className="border-bottom border-color-1 mb-5">
                <h1 className="section-title mb-0 pb-2 font-size-22">Shop By Category</h1>
            </div>
            <div className="row">
                {
                    categories.map(category => (
                        <div className="col-md-3 col-lg-3 mb-5" key={category.id}>
                            <Link href={category.parent_id === 0 ? `/shop?parent=${category.slug_url}` : `/shop?category=${category.slug_url}`}>
                                <a title={category.title}><img rel="preload" className="img-fluid" src={getS3Url(category.icon_url)} alt={category.title} /></a>
                            </Link>
                        </div>
                    ))
                }
            </div>
        </div>



    )
}

export default ShopByCategory
