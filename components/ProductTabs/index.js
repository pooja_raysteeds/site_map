import React, { useEffect, useState } from "react"
import Link from "next/link";
import { Rating, RatingView } from 'react-simple-star-rating'
import dateFormat from "dateformat"
import ReactMarkdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { getProductReview, addProductReview } from "../../api/products"
import { getErrorMessage } from "../../services/errorHandle"
import { getLocalStorage } from "../../services/helper"

const ProductTabs = ({ data }) => {
    if (data && Object.keys(data).length === 0) return null;
    const userId = getLocalStorage("usrid")
    const [rating, setRating] = useState(0)
    const [review, setReview] = useState("")
    const [errorMsg, setErrorMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const [productReviews, setProductReviews] = useState({})

    useEffect(() => {
        fetchProductReviews(data.id);
    }, [data]); 
   
    const fetchProductReviews = (product_id) => {
        getProductReview(product_id).then(
            res => setProductReviews(res),
        ).catch(
            err => getErrorMessage(err)
        )
    }

    const handleRating = (rate) => {
        setRating(rate)
    }

    const handleReview = (e) => {
        const { value } = e.target
        setReview(value)
    }

    const handleProductReview = (e) => {
        e.preventDefault();
        const payload = {
            product_id: data.id,
            user_id: userId,
            rating,
            review
        }

        addProductReview(payload).then(
            res => {
                setRating(0)
                setReview('')
                setSuccessMsg("Thanks for giving  your review.")
                fetchProductReviews(data.id)
            }).catch(
                err => {
                    const { message } = getErrorMessage(err)
                    setErrorMsg(message)
                })
    }

    const renderAddReview = () => {
        return (
            <>
                <h3 className="font-size-18 mb-5">
                    Add a review
                </h3>
                <form onSubmit={handleProductReview}>
                    <div className="row align-items-center mb-4">
                        <div className="col-md-4 col-lg-3">
                            <label
                                htmlFor="rating"
                                className="form-label mb-0"
                            >
                                Your Rating
                            </label>
                        </div>
                        <div className="col-md-8 col-lg-9">
                            <Rating
                                onClick={handleRating}
                                ratingValue={rating}
                            />
                        </div>
                    </div>
                    <div className="js-form-message form-group mb-3 row">
                        <div className="col-md-4 col-lg-3">
                            <label
                                htmlFor="descriptionTextarea"
                                className="form-label"
                            >
                                Your Review
                            </label>
                        </div>
                        <div className="col-md-8 col-lg-9">
                            <textarea
                                className="form-control"
                                rows={3}
                                id="descriptionTextarea"
                                name="review"
                                value={review}
                                onChange={handleReview}
                                required
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="offset-md-4 offset-lg-3 col-auto">
                            <button
                                type="submit"
                                className="btn btn-primary-dark btn-wide transition-3d-hover"
                            >
                                Add Review
                            </button>
                        </div>
                    </div>
                </form>
                <div className="row">
                    <div className="offset-md-4 offset-lg-3 col-auto mt-4">
                        {
                            errorMsg ? (
                                <p className="text-red mb-4">
                                    {errorMsg}
                                </p>
                            ) : (<></>)
                        }
                        {
                            successMsg ? (
                                <p className="text-green mb-4">
                                    {successMsg}
                                </p>
                            ) : (<></>)
                        }
                    </div>
                </div>
            </>
        )
    }

    return (
        <div className="container">
            <div className="mb-8">
                <div className="position-relative position-md-static px-md-6">
                    <ul
                        className="nav nav-classic nav-tab nav-tab-lg justify-content-xl-center flex-nowrap flex-xl-wrap overflow-auto overflow-xl-visble border-0 pb-1 pb-xl-0 mb-n1 mb-xl-0"
                        id="pills-tab-8"
                        role="tablist"
                    >
                        {/* <li className="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a
                                className="nav-link active"
                                id="Jpills-one-example1-tab"
                                data-toggle="pill"
                                href="#Jpills-one-example1"
                                role="tab"
                                aria-controls="Jpills-one-example1"
                                aria-selected="true"
                            >
                                Description
                            </a>
                        </li> */}
                        <li className="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a
                                className="nav-link active"
                                id="Jpills-two-example1-tab"
                                data-toggle="pill"
                                href="#Jpills-two-example1"
                                role="tab"
                                aria-controls="Jpills-two-example1"
                                aria-selected="true"
                            >
                                Specification
                            </a>
                        </li>
                        <li className="nav-item flex-shrink-0 flex-xl-shrink-1 z-index-2">
                            <a
                                className="nav-link"
                                id="Jpills-three-example1-tab"
                                data-toggle="pill"
                                href="#Jpills-three-example1"
                                role="tab"
                                aria-controls="Jpills-three-example1"
                                aria-selected="false"
                            >
                                Reviews
                            </a>
                        </li>
                    </ul>
                </div>
                <div className="borders-radius-17 border p-4 mt-4 mt-md-0 px-lg-10 py-lg-9">
                    <div className="tab-content" id="Jpills-tabContent">
                        {/* Tab One - Product Description */}
                        {/* <div
                            className="tab-pane fade active show"
                            id="Jpills-one-example1"
                            role="tabpanel"
                            aria-labelledby="Jpills-one-example1-tab"
                        >
                            <div className="row">
                                <ReactMarkdown children={data.description} rehypePlugins={[rehypeRaw]} />
                            </div>
                        </div> */}

                        {/* Tab Two - Product Specification */}
                        <div
                            className="tab-pane fade active show"
                            id="Jpills-two-example1"
                            role="tabpanel"
                            aria-labelledby="Jpills-two-example1-tab"
                        >
                            <div className="mx-md-5 pt-1">
                                <h3 className="font-size-18 mb-4">
                                    Technical Specifications
                                </h3>
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <tbody>
                                            {data.specifications.map((item, index) => (
                                                <tr key={index}>
                                                    <th className="px-4 px-xl-5 border-top-0">
                                                        {item.key}
                                                    </th>
                                                    <td className="border-top-0">
                                                        {item.value}
                                                    </td>
                                                </tr>
                                            )
                                            )}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        {/* Tab Three - Product Reviews */}
                        <div
                            className="tab-pane fade"
                            id="Jpills-three-example1"
                            role="tabpanel"
                            aria-labelledby="Jpills-three-example1-tab"
                        >
                            {productReviews && Object.keys(productReviews).length > 0 ? (
                                <>
                                    <div className="row mb-8">
                                        <div className="col-md-6">
                                            <div className="mb-3">
                                                <h3 className="font-size-18 mb-6">
                                                    {`Based on ${productReviews.reviews_total} reviews`}
                                                </h3>
                                                <h2 className="font-size-30 font-weight-bold text-lh-1 mb-0">
                                                    {productReviews.rating_average}
                                                </h2>
                                                <div className="text-lh-1">
                                                    Average Rating
                                                </div>
                                            </div>

                                            <div className="list-unstyled mb-2 mb-md-0 py-1">
                                                <div className="text-warning text-ls-n2 font-size-16">
                                                    <RatingView fillColor="#ffc107" ratingValue={productReviews.rating_average} />
                                                </div>
                                            </div>
                                        </div>

                                        {
                                            userId ? (
                                                <div className="col-md-6">
                                                    {renderAddReview()}
                                                </div>
                                            ) : (
                                                <div className="col-md-6">
                                                    <h3 className="font-size-18 mb-6">
                                                        Please click here for {" "}
                                                        <Link href="/login">
                                                            <a className="text-blue">Login</a>
                                                        </Link>
                                                    </h3>
                                                </div>
                                            )
                                        }
                                    </div>

                                    {productReviews.product_reviews ? (
                                        productReviews.product_reviews.map(r => (
                                            <div key={r.id} className="border-bottom border-color-1 pb-4 mb-4">

                                                <div className="mb-2">
                                                    <strong>
                                                        {r.user.name}
                                                    </strong>
                                                    <span className="font-size-13 text-gray-23">
                                                        {" - "}{dateFormat(r.createdAt, "fullDate")}
                                                    </span>
                                                </div>
                                                <div className="d-flex justify-content-between align-items-center text-secondary font-size-1 mb-2">
                                                    <div className="text-warning text-ls-n2 font-size-16">
                                                        <RatingView fillColor="#ffc107" ratingValue={r.rating} />
                                                    </div>
                                                </div>
                                                <p className="text-gray-90">
                                                    {r.review}
                                                </p>
                                            </div>
                                        ))
                                    ) : (
                                        <h3 className="font-size-18 mb-6">
                                            No Reviews
                                        </h3>
                                    )}
                                </>
                            ) : (
                                <>
                                    <h3 className="font-size-26 mb-6">
                                        No Reviews 
                                    </h3>
                                    
                                    {
                                        userId === null ? (
                                            <div className="row mb-8">
                                                <div className="col-md-6">
                                                    <h3 className="font-size-18 mb-6">
                                                        Please click here for {" "}
                                                        <Link href="/login">
                                                            <a className="text-blue">Login</a>
                                                        </Link>
                                                    </h3>
                                                </div>
                                            </div>
                                        ) : (
                                            <div className="row mb-8">
                                                <div className="col-md-6 offset-md-6">
                                                    {renderAddReview()}
                                                </div>
                                            </div>
                                        )
                                    }
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductTabs;
