import React, { useState, useEffect } from "react";
import dateformat from "dateformat"

const Timer = (data) => {
    const [timerDays, setTimerDays] = useState(0)
    const [timerHours, setTimerHours] = useState(0);
    const [timerMinutes, setTimerMinutes] = useState(0);
    const [timerSeconds, setTimerSeconds] = useState(0);

    let interval;

    useEffect(() => {
        let interval = setInterval(() => {
            const countDownDate = new Date(data.end).getTime();
            const now = new Date().getTime();
            const distance = countDownDate - now;
            const days = Math.floor(distance / (24 * 60 * 60 * 1000));
            const hours = Math.floor(
                (distance % (24 * 60 * 60 * 1000)) / (1000 * 60 * 60)
            );
            const minutes = Math.floor(
                (distance % (60 * 60 * 1000)) / (1000 * 60)
            );
            const seconds = Math.floor((distance % (60 * 1000)) / 1000);

            if (distance < 0) {
                clearInterval(interval.current);
            } else {
                setTimerDays(days)
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }
        }, 1000);
        return () => {
            clearInterval(interval);
        };
    });

    return (
        <div className="mb-2">
            <h6 className="font-size-15 text-gray-2 text-center mb-xl-3 mb-wd-2">
                Hurry Up! Offer ends in:
            </h6>
            <div
                className="js-countdown d-flex justify-content-center timer"
                data-end-date="2020/11/30"
                data-hours-format="%H"
                data-minutes-format="%M"
                data-seconds-format="%S"
            >
                <div className="text-lh-1">
                    <div className="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                        <span className="js-cd-hours">{timerDays}</span>
                    </div>
                    <div className="text-gray-2 font-size-12 text-center">
                        DAYS
                    </div>
                </div>
                <div className="mx-1 pt-1 text-gray-2 font-size-24">:</div>
                <div className="text-lh-1">
                    <div className="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                        <span className="js-cd-hours">{timerHours}</span>
                    </div>
                    <div className="text-gray-2 font-size-12 text-center">
                        HOURS
                    </div>
                </div>
                <div className="mx-1 pt-1 text-gray-2 font-size-24">:</div>
                <div className="text-lh-1">
                    <div className="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                        <span className="js-cd-minutes">{timerMinutes}</span>
                    </div>
                    <div className="text-gray-2 font-size-12 text-center">
                        MINS
                    </div>
                </div>
                <div className="mx-1 pt-1 text-gray-2 font-size-24">:</div>
                <div className="text-lh-1">
                    <div className="text-gray-2 font-size-30 bg-gray-4 py-2 px-2 rounded-sm mb-2">
                        <span className="js-cd-seconds">{timerSeconds}</span>
                    </div>

                    <div className="text-gray-2 font-size-12 text-center">
                        SECS
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Timer;
