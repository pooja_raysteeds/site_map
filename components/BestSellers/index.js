import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link'
import { getS3Url, calcPercentage, getLocalStorage } from "../../services/helper"
import { CATEGORY_SLUG_URLS } from "../../services/constant"
import AppContext from "../../context/AppContext"
import { getBestSellingProducts } from "../../api/products";
import { getErrorMessage } from "../../services/errorHandle";
import Slider from "react-slick";


const BestSellers = () => {
    const userId = getLocalStorage("usrid")
    const cartValue = getLocalStorage("crt")
    const wishlistValue = getLocalStorage("wstl")
    const { addProductToCart, addProductToWishlist, removeProductToWishlist } = useContext(AppContext);
    const [products, setProducts] = useState(null);
    const [currentSlugUrl, setCurrentSlugUrl] = useState(CATEGORY_SLUG_URLS.TOP_20)

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        arrows: true,
        lazyLoad: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        row: 2,
        initialSlide: 0,
        className: 'u-slick position-static overflow-hidden u-slick-overflow-visble pb-7 pt-2 px-1',
        customPaging: function (i) {
            return (
                <span />
            );
        },
        responsive: [{
            "breakpoint": 1800,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 1400,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 1200,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 992,
            "settings": {
                "slidesToShow": 3
            }
        }, {
            "breakpoint": 768,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 554,
            "settings": {
                "slidesToShow": 1
            }
        }],
        dotsClass: "text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0",

    };

    useEffect(() => {
        getProductByCatUrl(currentSlugUrl);
    }, [currentSlugUrl])

    const getProductByCatUrl = async (url) => {
        try {
            const res = await getBestSellingProducts(url);
            setProducts(res)
        } catch (err) {
            getErrorMessage(err);
        }
    }

    const handleCart = (product_id) => {
        const params = { product_id, quantity: 1 }
        addProductToCart(params)
    }

    const handleWishlist = (e, product_id) => {
        e.preventDefault()
        addProductToWishlist(product_id)
    }

    const removeWishlist = (e, product_id) => {
        e.preventDefault()
        removeProductToWishlist(product_id);
    }

    return (
        <div className="container fot">
            <div className="mb-8">
                <div className="d-flex justify-content-between border-bottom border-color-1 flex-md-nowrap flex-wrap border-sm-bottom-0">
                    <h1 className="section-title mb-0 pb-2 font-size-22">Bestsellers</h1>
                    <div className='overflow-auto overflow-lg-visble'>
                        <ul className="nav nav-pills nav-tab-pill mb-2 pt-3 pt-lg-0 mb-0 border-top border-color-1 border-lg-top-0 align-items-center font-size-15 font-size-15-lg flex-nowrap flex-lg-wrap  pr-0" id="pills-tab-1" role="tablist">
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.TOP_20)} className="nav-link rounded-pill active" id="Tpills-one-example1-tab" data-toggle="pill" href="#Tpills-one-example1" role="tab" aria-controls="Tpills-one-example1" aria-selected="true">
                                    Top 20
                                </a>
                            </li>
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.AIR_CONDITIONER)} className="nav-link rounded-pill" id="Tpills-two-example1-tab" data-toggle="pill" href="#Tpills-two-example1" role="tab" aria-controls="Tpills-two-example1" aria-selected="false">
                                    Air Conditioner
                                </a>
                            </li>
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.LAPTOP)} className="nav-link rounded-pill" id="Tpills-three-example1-tab" data-toggle="pill" href="#Tpills-three-example1" role="tab" aria-controls="Tpills-three-example1" aria-selected="false">
                                    Laptop
                                </a>
                            </li>
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.WASHING_MACHINE)} className="nav-link rounded-pill" id="Tpills-four-example1-tab" data-toggle="pill" href="#Tpills-four-example1" role="tab" aria-controls="Tpills-four-example1" aria-selected="false">
                                    Washing Machine
                                </a>
                            </li>
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.TELEVISION)} className="nav-link rounded-pill" id="Tpills-four-example1-tab" data-toggle="pill" href="#Tpills-four-example1" role="tab" aria-controls="Tpills-four-example1" aria-selected="false">
                                    Television
                                </a>
                            </li>
                            <li className="nav-item flex-shrink-0 flex-lg-shrink-1">
                                <a onClick={() => setCurrentSlugUrl(CATEGORY_SLUG_URLS.SPEAKER)} className="nav-link rounded-pill" id="Tpills-four-example1-tab" data-toggle="pill" href="#Tpills-four-example1" role="tab" aria-controls="Tpills-four-example1" aria-selected="false">
                                    Speakers
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="position-relative tab-content" id="pills-tabContent">
                    <div
                        className="tab-pane fade pt-2 show active"
                        id="pills-one-example1"
                        role="tabpanel"
                        aria-labelledby="pills-one-example1-tab"
                        data-target-group="groups"
                    >
                        <Slider {...settings}>
                            {products &&
                                products.map(p => (
                                    <li key={p.id} className="product-item">
                                        <div className="product-item__outer h-100 ">
                                            <div className="product-item__inner p-md-3 row no-gutters">
                                                <div className="col col-lg-auto col-xl-5 col-wd-auto product-media-left">
                                                    <Link href={`/shop/${p.slug_url}`}>
                                                        <a
                                                            className="max-width-150 d-block"
                                                        >
                                                            {
                                                                p.galleries.length > 0 && (
                                                                    <img
                                                                        rel="preload"
                                                                        className="img-fluid"
                                                                        src={getS3Url(p.galleries[0].image_url)}
                                                                        alt={p.title}
                                                                        title={p.title}
                                                                    />
                                                                )
                                                            }
                                                        </a>
                                                    </Link>
                                                </div>
                                                <div className="col col-xl-7 col-wd product-item__body pl-2 pl-lg-3 pl-xl-0 pl-wd-3 mr-wd-1">
                                                    <div className="mb-4 mb-xl-2 mb-wd-4">
                                                        <div className="mb-2">
                                                            <Link href={`/shop/${p.slug_url}`}>
                                                                <a
                                                                    className="font-size-12 text-gray-5"
                                                                >
                                                                    {p.category.title}
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <h5 className="product-item__title">
                                                            <Link href={`/shop/${p.slug_url}`}>
                                                                <a
                                                                    href="../shop/single-product-fullwidth.html"
                                                                    className="text-blue font-weight-bold"
                                                                >
                                                                    {p.title}
                                                                </a>
                                                            </Link>
                                                        </h5>
                                                    </div>

                                                    <span className="text-green font-size-15 font-weight-bold mt-1">
                                                        {`${calcPercentage(p.mrp, p.selling_price)}% off`}
                                                    </span>
                                                    <div className="flex-center-between mb-1">
                                                        <div className="prodcut-price d-flex">
                                                            <div className="text-gray-100 text-red">{`₹${p.selling_price}`}</div>
                                                            <del className="delete-price mt-1">
                                                                {`₹${p.mrp}`}
                                                            </del>
                                                        </div>
                                                        <div className="d-none d-xl-block prodcut-add-cart">
                                                            {
                                                                cartValue.filter(c => c.product_id === p.id).length > 0 ? (
                                                                    <Link href="/cart">
                                                                        <a className="btn-add-cart btn-primary transition-3d-hover" title="Go to Cart">
                                                                            <i className="ec ec-shopping-bag" />
                                                                        </a>
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/cart">
                                                                        <a onClick={() => handleCart(p.id)} className="btn-add-cart btn-primary transition-3d-hover" title="Add to Cart">
                                                                            <i className="ec ec-add-to-cart" />
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="product-item__footer">
                                                        <div className="border-top pt-2 flex-center-between flex-wrap">
                                                            {/* <Link href="/compare">
                                                                <a
                                                                    className="text-gray-6 font-size-13"
                                                                >
                                                                    <i className="ec ec-compare mr-1 font-size-15" /> Compare
                                                                </a>
                                                            </Link> */}
                                                            {
                                                                userId ? (
                                                                    <Link href="/wishlist">
                                                                        {
                                                                            wishlistValue.filter(w => w.product_id === p.id).length > 0 ? (
                                                                                <a onClick={(e) => removeWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-close-remove mr-1 font-size-15" />{" "}
                                                                                    Remove from Wishlist
                                                                                </a>
                                                                            ) : (
                                                                                <a onClick={(e) => handleWishlist(e, p.id)} className="text-gray-6 font-size-13">
                                                                                    <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                                    Wishlist
                                                                                </a>
                                                                            )
                                                                        }
                                                                    </Link>
                                                                ) : (
                                                                    <Link href="/login">
                                                                        <a className="text-gray-6 font-size-13 mr-2">
                                                                            <i className="ec ec-favorites mr-1 font-size-15" />{" "}
                                                                            Wishlist
                                                                        </a>
                                                                    </Link>
                                                                )
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                ))
                            }
                        </Slider>
                    </div>


                </div>
            </div>
        </div>
    )
}

export default BestSellers
