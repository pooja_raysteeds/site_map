import React, { useState, useEffect } from "react"
import Link from 'next/link'
import { getS3Url, calcPercentage } from "../../services/helper"
import Timer from "../../components/timer"
import Slider from "react-slick";


const SpecialOffers = ({ data }) => {
    const [timerHours, setTimerHours] = useState(10);
    const [timerMinutes, setTimerMinutes] = useState(10);
    const [timerSeconds, setTimerSeconds] = useState(10);

    let interval;
    const settings = {
        dots: true,
        infinite: data.length > 2,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        customPaging: function (i) {
            return (
                <span />
            );
        },
        responsive: [{
            "breakpoint": 1800,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 1400,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 1200,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 992,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 768,
            "settings": {
                "slidesToShow": 2
            }
        }, {
            "breakpoint": 554,
            "settings": {
                "slidesToShow": 1
            }
        }],
        dotsClass: "text-center right-0 bottom-1 left-0 u-slick__pagination u-slick__pagination--long mb-0 z-index-n1 mt-3 mt-md-0",

    };
    useEffect(() => {
        let interval = setInterval(() => {
            const countDownDate = new Date("December 30, 2021").getTime();
            const now = new Date().getTime();
            const distance = countDownDate - now;
            const hours = Math.floor(distance % (24 * 60 * 60 * 1000) / (1000 * 60 * 60))
            const minutes = Math.floor(distance % (60 * 60 * 1000) / (1000 * 60))
            const seconds = Math.floor(distance % (60 * 1000) / (1000))

            if (distance < 0) {
                clearInterval(interval.current);
            } else {
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }

        }, 1000)
        return () => { clearInterval(interval) }
    })

    return (
        <div className="container spc">
            <div className="border-bottom border-color-1 mb-5">
                <h1 className="section-title mb-0 pb-2 font-size-22">Special Offers</h1>
            </div>
            <div className="mb-6 set-width" >
                <div className="m-1 col -6 p-xl-4 p-wd-0 border border-width-2 border-primary borders-radius-20 border-wd-0">
                    <div
                        className="slick-dotted slick-slider"

                    >
                        <Slider {...settings}>
                            {
                                data.map(p => (

                                    <div className="js-slide" key={p.id}>
                                        <div className="p-4 p-xl-0 p-wd-4 border border-xl-0 border-width-2 border-primary borders-radius-20 bg-white" style={{ margin: "6px" }}>
                                            <div className="row align-items-md-center" >
                                                <div className="col-md-6 col-xl-7 col-wd-6 px-0 align-self-center">
                                                    <div className="d-inline-flex justify-content-between align-items-center position-absolute left-3 top-0 z-index-9">
                                                        <div className="d-flex align-items-center flex-column justify-content-center bg-primary rounded-pill height-75 width-75 text-lh-1">
                                                            <span className="font-size-12 text-white">Save</span>
                                                            <div className="font-size-20 font-weight-bold text-white">{`${calcPercentage(p.productInfo.mrp, p.productInfo.selling_price)}%`}</div>
                                                        </div>
                                                    </div>
                                                    <div className="mb-4 mb-md-0">
                                                        <div className="row align-items-center">
                                                            <div className="col">
                                                                <Link href={`/shop/${p.productInfo.slug_url}`}>
                                                                    <a
                                                                        className="d-block text-center"
                                                                    >
                                                                        {
                                                                            p.productInfo.galleries.length > 0 && (
                                                                                <img
                                                                                    rel="preload"
                                                                                    className="img-fluid mx-auto"
                                                                                    src={getS3Url(p.productInfo.galleries[0].image_url)}
                                                                                    alt={p.productInfo.galleries[0].caption_img}
                                                                                    title={p.productInfo.galleries[0].caption_img}
                                                                                />
                                                                            )
                                                                        }
                                                                    </a>
                                                                </Link>
                                                            </div>
                                                            <div className="col-auto d-none d-xl-block d-wd-none">
                                                                <ul className="list-group rounded-0">
                                                                    {
                                                                        p.productInfo.galleries.map(g => (
                                                                            <li key={g.id} className="list-group-item my-1 p-0 border-0">
                                                                                <a
                                                                                    className="js-fancybox max-width-70 u-media-viewer"
                                                                                    href="#"
                                                                                    data-src={getS3Url(g.image_url)}
                                                                                    data-fancybox="fancyboxGallery6"
                                                                                    data-caption="Electro in frames - image #01"
                                                                                    data-speed={700}
                                                                                    data-is-infinite="true"
                                                                                >
                                                                                    <img
                                                                                        rel="preload"
                                                                                        className="img-fluid border"
                                                                                        src={getS3Url(g.image_url)}
                                                                                        alt={g.caption_img}
                                                                                        title={g.caption_img}
                                                                                    />
                                                                                    <span className="u-media-viewer__container">
                                                                                        <span className="u-media-viewer__icon">
                                                                                            <span className="fas fa-plus u-media-viewer__icon-inner" />
                                                                                        </span>
                                                                                    </span>
                                                                                </a>
                                                                            </li>
                                                                        ))
                                                                    }
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 col-xl-5 col-wd-6">
                                                    <div className="space-top-xl-2 space-top-wd-0 mt-xl-4 mt-wd-0">
                                                        <h5 className="mb-3 mb-xl-2 mb-wd-1 font-size-14 text-center mx-auto max-width-180 text-lh-18">
                                                            <Link href={`/shop/${p.productInfo.slug_url}`}>
                                                                <a
                                                                    className="text-blue font-weight-bold"
                                                                >
                                                                    {p.productInfo.title}
                                                                </a>
                                                            </Link>
                                                        </h5>
                                                        <div className="d-flex align-items-center justify-content-center mb-3">
                                                            <del className="font-size-20 mr-2 text-gray-2">
                                                                {`₹${p.productInfo.mrp}`}
                                                            </del>
                                                            <ins className="font-size-xl-30 font-size-wd-25 text-red text-decoration-none">
                                                                {`₹${p.productInfo.selling_price}`}
                                                            </ins>
                                                        </div>
                                                        <div className="mb-4 mb-xl-5 mb-wd-2 pb-wd-1 mx-2">
                                                            <div className="d-flex justify-content-between align-items-center mb-2">
                                                                <span>
                                                                    Available: <strong>{p.productInfo.stock_quantity}</strong>
                                                                </span>
                                                                {/* <span>
                                                                Already Sold: <strong>28</strong>
                                                            </span> */}
                                                            </div>
                                                            <div className="rounded-pill bg-gray-3 height-wd-14 height-xl-20 position-relative">
                                                                <span className="position-absolute left-0 top-0 bottom-0 rounded-pill w-30 bg-primary" />
                                                            </div>
                                                        </div>
                                                        <Timer start={p.start_date} end={p.end_date} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </Slider>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SpecialOffers
