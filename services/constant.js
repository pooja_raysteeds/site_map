export const API_BASE_URL = "http://toptenelectronics-dev-api.quolam.com/api";
// export const API_BASE_URL = "http://localhost:3002/api";
export const API_BASE_URL_CCAVENUE = "http://toptenelectronics-dev-ccavenue.quolam.com/";
export const S3_BASE_URL = "https://d1qafoceyvj568.cloudfront.net";
export const ccAvenueEndPoint = "https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction"
export const SHARED_BASE_URL = "http://toptenelectronics-dev.quolam.com/"

// Promo Types
export const PROMO_TYPE_1 = "HOME_SLIDER";
export const PROMO_TYPE_2 = "CAT_5";  // Refrigerator
export const PROMO_TYPE_3 = "CAT_7";  // TV
export const PROMO_TYPE_4 = "CAT_8";  // AC
export const PROMO_TYPE_5 = "CAT_2";  // Washing Machine
export const PROMO_TYPE_6 = "CAT_6";  // Kitchen Apliences
export const PROMO_TYPE_7 = "CAT_3";  // Mircowave
export const PROMO_TYPE_8 = "BAN_1";  // Footer Banner
export const PROMO_TYPE_9 = "SHOP_SLIDER";  // Default Shop Slider
export const PROMO_TYPE_10 = "F_O_T";
export const PROMO_TYPE_11 = "CAT_1";  // Speaker
export const PROMO_TYPE_12 = "CAT_4";  // Laptop

// Product Category
export const CATEGORY_TYPE_1 = 8;   // Refrigerator
export const CATEGORY_TYPE_2 = 6;   // AC
export const CATEGORY_TYPE_3 = 2;   // TV
export const CATEGORY_TYPE_4 = 7;   // Washing Machine
export const CATEGORY_TYPE_5 = 12;  // Kitchen Apliences (Parent)
export const CATEGORY_TYPE_6 = 76;  // Microwave
export const CATEGORY_TYPE_7 = 64;  // Speaker
export const CATEGORY_TYPE_8 = 10;  // Laptop

// BestSellers Category SLUG_URLs
export const CATEGORY_SLUG_URLS = {
    TOP_20: "0",
    AIR_CONDITIONER: "air-conditioner",
    LAPTOP: "laptop",
    WASHING_MACHINE: "washing-machine",
    TELEVISION: "television",
    SPEAKER: "speaker"
}

// Limits
export const F_O_T_TAB_LIMIT = 10;
export const F_O_T_PRODUCTS_LIMIT = 3;
export const LATEST_PRODUCTS_LIMIT = 5;
export const BEST_SELLERS_LIMIT = 6;

//Order Status
export const confirmedStatus = "delivered";
export const cancelledId = 3;
export const failedId = 4;
export const deliveredId = 5;
export const orderedId = 6;
export const cancledStatus = "cancelled"
export const MERCHANT_ID = '265086'

//Google client-ID
export const GOOGLE_CLIENT_ID = "181869422376-r7plloipdkra34ath6ua0k8pn1r8l506.apps.googleusercontent.com";

// Facebook App-ID
export const FACEBOOK_APP_ID = "432744398225609";


