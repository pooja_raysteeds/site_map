export const getErrorMessage = (err, customMessage = "") => {
    try {
        if (err && Object.keys(err).length > 0) {
            return err.error;
        } else if (customMessage !== "") {
            return customMessage;
        } else {
            return "Something went wrong !!";
        }
    } catch (e) {
        return "Server Error !!";
    }
};