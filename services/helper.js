import { S3_BASE_URL } from "./constant";

export const setLocalStorage = (key, value) => {
  if (typeof window !== 'undefined') {
    window.localStorage.setItem(key, JSON.stringify(value));
  }
}

export const getLocalStorage = (key) => {
  let value = null;
  if (typeof window !== 'undefined') {
    value = window.localStorage.getItem(key);
  }
  return JSON.parse(value);
}

export const getS3Url = (url) => {
  const s3url = `${S3_BASE_URL}/${url}`;
  return s3url;
}

export const calcPercentage = (mp, sp) => {
  const diff = parseInt(mp - sp, 10);
  const percent = parseInt((diff * 100) / mp);
  return percent
}

export const calcAverageRating = (data) => {
  const rating_5 = data.filter(r => { return r.rating === 5 });
  const rating_4 = data.filter(r => { return r.rating === 4 });
  const rating_3 = data.filter(r => { return r.rating === 3 });
  const rating_2 = data.filter(r => { return r.rating === 2 });
  const rating_1 = data.filter(r => { return r.rating === 1 });
  const rating_count = (5 * rating_5.length) + (4 * rating_4.length) + (3 * rating_3.length) + (2 * rating_2.length) + (1 * rating_1.length);
  return parseFloat((rating_count / data.length).toFixed(1));
}

export const getRandomNumber = () => {
  return Math.floor(10000000 + Math.random() * 90000000).toString();
}

export const calcPriceSummary = (data) => {
  let subTotal = 0;
  let mrpAmount = 0;
  let savedAmount = 0;
  let shippingCharge = 0;
  let total = 0;
  data.map(item => {
    subTotal += item.product.selling_price * item.quantity;
    mrpAmount += item.product.mrp * item.quantity;
  })
  savedAmount = mrpAmount - subTotal;
  total = subTotal + shippingCharge;
  return { subTotal, savedAmount, shippingCharge, total };
}