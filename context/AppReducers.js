import {useReducer} from "react";
import {USER_LOGIN, LOADING, STORE_USER_CARTS} from "./Actions"

const AppReducer = (state, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return {
                ...state,
                user: action.payload
            }
        
        case LOADING: 
            return {
                ...state,
                loading: action.payload
            }
        
        case STORE_USER_CARTS: 
            return {
                ...state,
                userCarts: action.payload
            }

        default:
            return state;
    }
} 

export default AppReducer;