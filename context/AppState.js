import React, { useState, useEffect } from 'react'
import AppContext from './AppContext'
import _ from "lodash";
import { getLocalStorage, setLocalStorage } from "../services/helper";
import { getErrorMessage } from "../services/errorHandle";
import { addToCart, addBulkCart, addToWishlist, removeFromWishlist } from "../api/user";

const AppState = (props) => {
    let cartValue = getLocalStorage("crt")
    let wishlistValue = getLocalStorage("wstl")
    let userCompares = getLocalStorage("usrCmpr")
    const userId = getLocalStorage("usrid")
    const [isMobileView, setIsMobileView] = useState(false)
    const [username, setUsername] = useState(getLocalStorage("usrn") || null)
    const [cartCount, setCartCount] = useState(cartValue ? cartValue.length : 0)
    const [wishListCount, setWishListCount] = useState(wishlistValue ? wishlistValue.length : 0)
    const [pswValidate, setPswValidate] = useState([
        { value: "A Uppercase letter", key: 1, style: "text-red", flag: false },
        { value: "A Lowercase letter", key: 2, style: "text-red", flag: false },
        { value: "A Digit", key: 3, style: "text-red", flag: false },
        { value: "Minimum 8 characters", key: 4, style: "text-red", flag: false },
    ])

    const handleWindowWidth = () => {
        if (window.innerWidth < 768) {
            setIsMobileView(true)
        } else {
            setIsMobileView(false)
        }
    }

    useEffect(() => {
        handleWindowWidth()
        window.addEventListener("resize", handleWindowWidth);
    }, []);

    const initializeUser = (data) => {
        const { token, user: { id, name, carts, wishlists } } = data
        setLocalStorage("tkn", token)
        setLocalStorage("usrid", id)
        setLocalStorage("usrn", name)
        setUsername(name)
        setUserWishlist(wishlists)
        const merged = _.unionBy(carts, cartValue, 'product_id');
        setUserCart(merged)
        const payload = merged.map(p => ({ ...p, "user_id": id }));
        addBulkCart(payload).then(
            res => { }
        ).catch(
            err => getErrorMessage(err)
        )
    }

    const resetUser = () => {
        setLocalStorage("tkn", null)
        setLocalStorage("usrid", null)
        setLocalStorage("usrn", null)
        setLocalStorage("ord", null)
        setLocalStorage("pymt", {})
        setLocalStorage("crtl", [])
        setUsername(null)
        setUserCart([])
        setUserWishlist([])
    }

    const setUserWishlist = (data) => {
        setLocalStorage("wstl", data)
        setWishListCount(data.length)
    }

    const setUserCart = (data) => {
        setLocalStorage("crt", data)
        setCartCount(data.length)
    }

    const addProductToCart = (data) => {
        if (userId) {
            const payload = { ...data, user_id: userId }
            addToCart(payload).then(
                res => {
                    cartValue.push(payload)
                    setUserCart(cartValue)
                }).catch(err => {
                    getErrorMessage(err)
                })
        } else {
            cartValue.push(data)
            setUserCart(cartValue)
        }
    }

    const addProductToWishlist = (product_id) => {
        const payload = { product_id, user_id: userId }
        addToWishlist(payload).then(
            res => {
                wishlistValue.push({ product_id })
                setUserWishlist(wishlistValue)
            }
        ).catch(err => {
            getErrorMessage(err)
        })
    }

    const removeProductToWishlist = (product_id) => {
        const payload = { product_id, user_id: userId }
        const filtered = wishlistValue.filter(p => p.product_id !== product_id)
        removeFromWishlist(payload).then(
            res => setUserWishlist(filtered)
        ).catch(err => {
            getErrorMessage(err)
        })
    }

    const validatePassword = (password) => {
        if (password.match(/[A-Z]/g)) {
            pswValidate[0].style = "text-green"
            pswValidate[0].flag = true
        } else {
            pswValidate[0].style = "text-red"
            pswValidate[0].flag = false
        }

        if (password.match(/[a-z]/g)) {
            pswValidate[1].style = "text-green"
            pswValidate[1].flag = true

        } else {
            pswValidate[1].style = "text-red"
            pswValidate[1].flag = false
        }

        if (password.match(/[0-9]/g)) {
            pswValidate[2].style = "text-green"
            pswValidate[2].flag = true
        } else {
            pswValidate[2].style = "text-red"
            pswValidate[2].flag = false
        }

        if (password.length >= 8) {
            pswValidate[3].style = "text-green"
            pswValidate[3].flag = true
        } else {
            pswValidate[3].style = "text-red"
            pswValidate[3].flag = false
        }

        if (pswValidate[0].flag && pswValidate[1].flag && pswValidate[2].flag && pswValidate[3].flag)
            return true;
        else return false;
    }

    const addToCompare = (id) => {
        if (!userCompares) {
            setLocalStorage("usrCmpr", []);
        }
        const flag = userCompares.find((item) => item == id);
        if (flag) return;
        userCompares.push(id);
        setLocalStorage("usrCmpr", userCompares);
    }

    const removeFromCompare = (id) => {
        const updatedList = userCompares.filter((item) => {
            if (item != id) return item;
        })
        userCompares = updatedList;
        setLocalStorage("usrCmpr", updatedList);
    }
    return (
        <AppContext.Provider
            value={{
                username, setUsername,
                cartCount, setCartCount,
                wishListCount, setWishListCount,
                addProductToCart,
                addProductToWishlist,
                removeProductToWishlist,
                setUserWishlist,
                setUserCart,
                initializeUser,
                resetUser,
                validatePassword,
                pswValidate,
                isMobileView,
                addToCompare,
                removeFromCompare
            }}
        >
            {props.children}
        </AppContext.Provider>
    );
}

export default AppState;