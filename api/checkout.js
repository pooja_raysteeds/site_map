import request from "../services/request";
import { API_BASE_URL } from "../services/constant";

export const encryptdata = ({ params }) => request(`${API_BASE_URL}/pay`, {
        method: "GET", params
});

export const getOrderDetails = (order_number) => request(`${API_BASE_URL}/getOrdersByOrderNumber/${order_number}`, {
        method: "GET",
});

export const changeOrderStatus = (data) => request(`${API_BASE_URL}/changeOrderStatus`, {
        method: "PUT", data
});

export const getPinCodes = (order_number) => request(`${API_BASE_URL}/getAllPincodes`, {
        method: "GET",
});