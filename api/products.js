import request from "../services/request";
import { API_BASE_URL } from "../services/constant";

export const getProducts = () => request(`${API_BASE_URL}/products`, {
    method: "GET",
});

export const getFilteredProducts = (data) => request(`${API_BASE_URL}/products`, {
    method: "POST", data
});

export const getProductsByCategoryIds = (data) => request(`${API_BASE_URL}/productsByCategoryIds`, {
    method: "POST", data
});

export const getProductsByParentIds = (data) => request(`${API_BASE_URL}/productsByParentIds`, {
    method: "POST", data
});

export const getProductByUrl = (slug_url) => request(`${API_BASE_URL}/productInfoByUrl/${slug_url}`, {
    method: "GET"
});

export const getRelatedProducts = (slug_url) => request(`${API_BASE_URL}/similarProducts/${slug_url}`, {
    method: "GET"
});

export const getProductReview = (id) => request(`${API_BASE_URL}/productReviewsById/${id}`, {
    method: "GET"
});

export const addProductReview = (data) => request(`${API_BASE_URL}/addReview`, {
    method: "POST", data
});

export const getBrands = () => request(`${API_BASE_URL}/brands`, {
    method: "GET"
});

export const getBrandsByCategory = (category_url) => request(`${API_BASE_URL}/brands/${category_url}`, {
    method: "GET",
});

export const getFilters = ({ params }) => request(`${API_BASE_URL}/filtersByCategory`, {
    method: "GET", params
});

export const getSearch = (query) => request(`${API_BASE_URL}/search/${query}`, {
    method: "GET"
});

export const getProductsByIds = (data) => request(`${API_BASE_URL}/getProductsByIds`, {
    method: "POST", data
});

export const getBestSellingProducts = (query) => request(`${API_BASE_URL}/bestSellers/${query}`, {
    method: "GET"
});

export const getExchangedProducts = () => request(`${API_BASE_URL}/productExchange`, {
    method: "GET"
});

export const getProductsWarranty = ({ params }) => request(`${API_BASE_URL}/productExtendedWarranty`, {
    method: "GET", params
});
