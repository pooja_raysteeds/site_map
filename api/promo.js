import request from "../services/request";
import { API_BASE_URL } from "../services/constant";

export const getPromoGalleries = () => request(`${API_BASE_URL}/promoGalleries`, {
    method: "GET",
});

export const getPromoByType = (type) => request(`${API_BASE_URL}/getPromosByType/${type}`, {
    method: "GET",
});

export const getDeals = () => request(`${API_BASE_URL}/deals`, {
    method: "GET",
});

export const getSpecialOffers = () => request(`${API_BASE_URL}/specialoffers`, {
    method: "GET",
});
