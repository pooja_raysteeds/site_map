import request from "../services/request";
import { API_BASE_URL } from "../services/constant";

export const signUp = (data) => request(`${API_BASE_URL}/user`, {
    method: "POST", data
});

export const login = (data) => request(`${API_BASE_URL}/login`, {
    method: "POST", data
});

export const getUser = (id) => request(`${API_BASE_URL}/user/${id}`, {
    method: "GET"
});

export const updateUser = (data) => request(`${API_BASE_URL}/updateUser`, {
    method: "PUT", data
});

export const getUserCart = (user_id) => request(`${API_BASE_URL}/userCarts/${user_id}`, {
    method: "GET"
});

export const getCartProducts = (ids) => request(`${API_BASE_URL}/getCookieCart/${JSON.stringify(ids)}`, {
    method: "GET"
});

export const addToCart = (data) => request(`${API_BASE_URL}/addUserCart`, {
    method: "POST", data
});

export const addBulkCart = (data) => request(`${API_BASE_URL}/addBulkUserCart`, {
    method: "POST", data
});

export const removeFromCart = (data) => request(`${API_BASE_URL}/deleteUserCart`, {
    method: "DELETE", data
});

export const removeAllFromCart = (user_id) => request(`${API_BASE_URL}/userCarts/${user_id}`, {
    method: "DELETE"
});

export const getUserWishList = (user_id) => request(`${API_BASE_URL}/userWishlists/${user_id}`, {
    method: "GET"
});

export const addToWishlist = (data) => request(`${API_BASE_URL}/addUserWishlist`, {
    method: "POST", data
});

export const removeFromWishlist = (data) => request(`${API_BASE_URL}/deleteWishlistByProductID`, {
    method: "DELETE", data
});

export const placeOrder = (data) => request(`${API_BASE_URL}/placeNewOrder`, {
    method: "POST", data
});

export const getUserOrder = (user_id) => request(`${API_BASE_URL}/getOrdersByUserID/${user_id}`, {
    method: "GET"
});

export const updateCartQty = (data) => request(`${API_BASE_URL}/updateProductQuantity`, {
    method: "PUT", data
});

export const updateOrderStatus = (data) => request(`${API_BASE_URL}/changeOrderStatus`, {
    method: "PUT", data
});

export const forgotPassword = (data) => request(`${API_BASE_URL}/forgotPassword`, {
    method: "POST", data
});

export const resetPassword = (data) => request(`${API_BASE_URL}/resetPassword`, {
    method: "POST", data
});

export const changePassword = (data) => request(`${API_BASE_URL}/changePassword`, {
    method: "POST", data
});

export const getOrderInfoByOrderNo = (number) => request(`${API_BASE_URL}/getOrdersByOrderNumber/${number}`, {
    method: "GET"
});

export const loginWithSocial = (data) => request(`${API_BASE_URL}/loginWithSocial`, {
    method: "POST", data
});

export const notifyMe = (data) => request(`${API_BASE_URL}/notifyUser`, {
    method: "POST", data
})

export const userAddresses = (user_id) => request(`${API_BASE_URL}/userAddresses/${user_id}`, {
    method: "GET"
});

export const markDefaultAddress = (data) => request(`${API_BASE_URL}/markDefaultAddress`, {
    method: "PUT", data
});

export const addUserAddress = (data) => request(`${API_BASE_URL}/addUserAddress`, {
    method: "POST", data
});

export const subscribeUser = (data) => request(`${API_BASE_URL}/subscribedUsers`, {
    method: "POST", data
});

export const messageToAdmin = (data) => request(`${API_BASE_URL}/messageToAdmin`, {
    method: "POST", data
});
